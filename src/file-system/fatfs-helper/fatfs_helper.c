/*
 * fatfs_helper.c
 *
 *  Created on: 04-Jul-2019
 *      Author: Rohan Dey
 */

#include "core_config.h"
#include "core_error.h"
#include "core_common.h"
#include "core_constant.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "sdmmc_host_core.h"
#include "fatfs_helper.h"
#include "thing.h"

#define TAG	LTAG_FATFS
#define ALLOCATION_UNIT_SIZE_SDMMC	(16 * 1024)

static FatFSHelper fatfs;

core_err init_and_mount_fatfs_on_sdmmc(char *partition_name, bool format_if_mount_failed, int max_open_files) {
	core_err ret = CORE_FAIL;

#if CONFIG_PLATFORM_ESP_IDF	/* For ESP, SPI mode is used to save pins */
	sdmmc_host_core_t host = SDSPI_HOST_CORE_DEFAULT();
#else
#endif
	// TODO: To test and check for other frequencies
	host.max_freq_khz = SDMMC_CORE_FREQ_PROBING;

	/* This initializes the slot without card detect (CD) and write protect (WP) signals.
	 * Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals. */
#if CONFIG_PLATFORM_ESP_IDF	/* For ESP, SPI mode is used to save pins */
	sdspi_core_slot_config_t slot_config = SDSPI_CORE_SLOT_CONFIG_DEFAULT();
    slot_config.gpio_miso = SDMMC_MISO;
    slot_config.gpio_mosi = SDMMC_MOSI;
    slot_config.gpio_sck  = SDMMC_CLK;
    slot_config.gpio_cs   = SDMMC_CS;
#else
    sdmmc_core_slot_config_t slot_config = SDMMC_CORE_SLOT_CONFIG_DEFAULT();
#endif
	/* Options for mounting the filesystem. If format_if_mount_failed is set to true, SD card will be partitioned
	 * and formatted in case when mounting fails. */
	fat_sdmmc_mount_config_core_t mount_config = { .format_if_mount_failed = format_if_mount_failed, .max_files =
			max_open_files, .allocation_unit_size = ALLOCATION_UNIT_SIZE_SDMMC };

	/* Use settings defined above to initialize SD card and mount FAT filesystem.
	 * Note: esp_vfs_fat_sdmmc_mount is an all-in-one convenience function.
	 * Please check its source code and implement error recovery when developing production applications
	 * */
	if ((partition_name == NULL) | (max_open_files == 0)) {
		CORE_LOGE(TAG, "Input params are incorrect!");
		return CORE_ERR_INVALID_ARG;
	}

	ret = fat_sdmmc_mount_pf(partition_name, &host, &slot_config, &mount_config, &fatfs.card);
	if (ret != CORE_OK) {
		if (ret == CORE_FAIL) {
			CORE_LOGE(TAG, "Failed to mount filesystem. "
					"If you want the card to be formatted, set format_if_mount_failed = true.");
		} else {
			CORE_LOGE(TAG, "Failed to initialize the card. "
					"Make sure SD card lines have pull-up resistors in place.");
		}
	}
	return ret;
}

core_err unmount_fatfs_sdmmc() {
	return fat_sdmmc_unmount_pf();
}
