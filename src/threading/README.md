# Threading Module
## Overview
The EmMate Framework uses an underlying operating system for functioning. Currently EmMate supports only FreeRTOS but in future releases it will support other operating systems also. So to provide an OS abstraction layer the Threading Module is developed.

Currently all the APIs of this module wraps FreeRTOS functions. To know more about FreeRTOS, please read the [FreeRTOS's documentation](https://www.freertos.org/FreeRTOS-Event-Groups.html) 

**Please note: The words "task" and "thread" is used interchangeably in this document**

## Features
With the help of the Threading Module we can do all kinds of thread related operations. Such as

- Create & delete threads (tasks)
- Suspend & resume threads (tasks)
- Create, delete and do other operations of Semaphores
- Create, delete and do other operations of Queues
- Halt a running task using `TaskDelay`

## How to use this module from an application

##### Header(s) to include

```
threading.h
```

#### Module Specific Configurations
---
**This module is a mandatory requirement for the framework, so it is enabled internally and cannot be disabled from the Menuconfig Application**

---

## Module Specific APIs
#### Threads (Tasks)
Thread, also called a task in RTOS, is a small piece of code which performs a specific functionality. In a multi-processor hardware environment, these threads can run concurrently, whereas, in a single-processor environment the threads are switched by the scheduler of an OS.

See the example under `examples/threading/` for understanding how to create threads and do other operations with them.

**Useful APIs:**
- Creating a Task - `TaskCreate()`
- Deleting a Task - `TaskDelete()`
- Halting a Task for some time - `TaskDelay()`

#### Queues
Queues are mostly used for inter thread communication. A queue ensures that the data is acquired and processed in a sequential manner. When 2 threads are used to synchronously perform an operation, it is better to use a queue for passing data from one thread to another.

See the example `examples/threading/data-exchange-into-threads`

**Useful APIs:**
- Creating a Queue - `QueueCreate()`
- Deleting a Queue - `QueueDelete()`
- Sending data to a Queue - `QueueSend()` 
- Receiving data from a Queue - `QueueReceive()`

#### Semaphores
Semaphores are thread synchronization tool. 

**Useful APIs:**
- Creating a Semaphore - `SemaphoreCreateMutex()`
- Deleting a Semaphore - `SemaphoreDelete()`
- To release a semaphore - `SemaphoreGive()`
- To obtain a semaphore - `SemaphoreTake()`
