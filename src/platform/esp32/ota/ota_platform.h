/*
 * File Name: ota_platform.h
 * File Path: /emmate/src/platform/esp/ota_platform/ota_platform.h
 * Description:
 *
 *  Created on: 09-May-2019
 *      Author: Noyel Seth
 */

#ifndef OTA_PLATFORM_H_
#define OTA_PLATFORM_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "http_client_platform.h"


/* Wrapper typedefs for abstracting the platform specific names */

#define OTA_PF_ERR_HTTPS_OTA_IN_PROGRESS	ESP_ERR_HTTPS_OTA_IN_PROGRESS
#define OTA_PF_ERR_OTA_VALIDATE_FAILED		ESP_ERR_OTA_VALIDATE_FAILED
#define OTA_PF_ERR_FLASH_OP_TIMEOUT			ESP_ERR_FLASH_OP_TIMEOUT
#define OTA_PF_ERR_FLASH_OP_FAIL			ESP_ERR_FLASH_OP_FAIL

//typedef esp_ota_img_states_t ota_pf_img_states_t;
typedef esp_partition_t ota_pf_partition_t;

typedef esp_https_ota_handle_t	ota_pf_https_ota_handle_t;

typedef esp_https_ota_config_t	ota_pf_https_ota_config_t;

/* Wrapper typedefs for abstracting the platform specific names */
/// OTA_DATA states for checking operability of the app.
typedef enum {
	OTA_PF_IMG_NEW = ESP_OTA_IMG_NEW, /*!< Monitor the first boot. In bootloader this state is changed to ESP_OTA_IMG_PENDING_VERIFY. */
	OTA_PF_IMG_PENDING_VERIFY = ESP_OTA_IMG_PENDING_VERIFY, /*!< First boot for this app was. If while the second boot this state is then it will be changed to ABORTED. */
	OTA_PF_IMG_VALID = ESP_OTA_IMG_VALID, /*!< App was confirmed as workable. App can boot and work without limits. */
	OTA_PF_IMG_INVALID = ESP_OTA_IMG_INVALID, /*!< App was confirmed as non-workable. This app will not selected to boot at all. */
	OTA_PF_IMG_ABORTED = ESP_OTA_IMG_ABORTED, /*!< App could not confirm the workable or non-workable. In bootloader IMG_PENDING_VERIFY state will be changed to IMG_ABORTED. This app will not selected to boot at all. */
	OTA_PF_IMG_UNDEFINED = ESP_OTA_IMG_UNDEFINED, /*!< Undefined. App can boot and work without limits. */
} ota_pf_img_states_t;

/* Wrapper typedefs for abstracting the platform specific names */
#define ota_pf_get_running_partition()							esp_ota_get_running_partition()
#define ota_pf_get_state_partition(partition, ota_state)		esp_ota_get_state_partition(/*const esp_partition_t* */ partition, /*esp_ota_img_states_t* */ ota_state)
#define ota_pf_mark_app_valid_cancel_rollback()					esp_ota_mark_app_valid_cancel_rollback()
#define ota_pf_mark_app_invalid_rollback_and_reboot() 			esp_ota_mark_app_invalid_rollback_and_reboot()
#define ota_pf_https_ota(config)								esp_https_ota(/*const esp_http_client_config_t * */config)
#define ota_pf_https_ota_begin(ota_config, handle)				esp_https_ota_begin(/*esp_https_ota_config_t* */ota_config, /*esp_https_ota_handle_t * */handle)
#define ota_pf_https_ota_perform(https_ota_handle)				esp_https_ota_perform(/*esp_https_ota_handle_t*/ https_ota_handle)
#define ota_pf_https_ota_finish(https_ota_handle)				esp_https_ota_finish(/*esp_https_ota_handle_t*/ https_ota_handle)
#define ota_pf_https_ota_get_image_len_read(https_ota_handle)	esp_https_ota_get_image_len_read(/*esp_https_ota_handle_t*/ https_ota_handle)

#endif /* OTA_PLATFORM_H_ */
