/**
 * Company:
 *  Iquester Solutions LLP
 *
 * Project:
 *   EmMate
 *
 * File Name:
 *   nvs_helper.h
 *
 * Created On:
 *   29-Mar-2019
 *
 * Author:
 *   Noyel Seth
 *
 * Summary:
 *
 *
 * Description:
 *
 *
 **/

#ifndef NVS_HELPER_H_
#define NVS_HELPER_H_

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "nvs_helper_errors.h"

/*
 * @brief initialize the ESP32 NVS
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 * */
core_err init_nvs();

/**
 * @brief This function de-initializes the underlying Non Volatile Storage/Memory (NVS)
 *
 **/
void deinit_nvs();

/*
 * @brief Erase the ESP32 NVS memory
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 * */
core_err erase_nvs();

/*
 * @brief read data from ESP32 NVS
 *
 * @param[in]	key unique Tag for collect data from NVS
 *
 * @param[out]	value	out data read from NVS
 *
 * @param[out]	val_size	out data-length of the read value
 *
 * @return
 * 		- CORE_OK							On success
 * 		- CORE_FAIL							On failure
 * 		- PLAT_NVS_ERR_NOT_FOUND 			if data not present
 * 		- PLAT_NVS_ERR_NOT_INITIALIZED		if the storage driver is not initialized
 *
 * */
core_err read_nvsdata_by_key(char *key, void* value, size_t * val_size);

/*
 * @brief write data to ESP32 NVS
 *
 * @param[in]	key unique Tag for store data from NVS
 *
 * @param[in]	value	input data to write into NVS
 *
 * @param[in]	val_size	input data length to write into NVS
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 * */
core_err write_nvsdata_by_key(char *key, void *set_data, size_t len);


#endif /* NVS_HELPER_H_ */
