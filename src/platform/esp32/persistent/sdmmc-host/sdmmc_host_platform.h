/*
 * sdmmc_host_platform.h
 *
 *  Created on: 03-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef SDMMC_HOST_PLATFORM_H_
#define SDMMC_HOST_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_error.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "driver/sdmmc_types.h"

#define SDMMC_PF_FREQ_DEFAULT		SDMMC_FREQ_DEFAULT      /*!< SD/MMC Default speed (limited by clock divider) */
#define SDMMC_PF_FREQ_HIGHSPEED		SDMMC_FREQ_HIGHSPEED    /*!< SD High speed (limited by clock divider) */
#define SDMMC_PF_FREQ_PROBING		SDMMC_FREQ_PROBING      /*!< SD/MMC probing speed */
#define SDMMC_PF_FREQ_52M			SDMMC_FREQ_52M          /*!< MMC 52MHz speed */
#define SDMMC_PF_FREQ_26M			SDMMC_FREQ_26M          /*!< MMC 26MHz speed */

/**
 * @brief Default sdmmc_host_pf_t structure initializer for SPI peripheral
 *
 */
#define SDSPI_HOST_PF_DEFAULT()			SDSPI_HOST_DEFAULT()

/**
 * @brief Macro defining default sdmmc_pf_slot_config_t configuration of SPI host slot
 */
#define SDSPI_PF_SLOT_CONFIG_DEFAULT()	SDSPI_SLOT_CONFIG_DEFAULT()

/**
 * @brief Default sdmmc_host_pf_t structure initializer for SDMMC peripheral
 *
 * Uses SDMMC peripheral, with 4-bit mode enabled, and max frequency set to 20MHz
 */
#define SDMMC_HOST_PF_DEFAULT()			SDMMC_HOST_DEFAULT()

/**
 * @brief Macro defining default sdmmc_pf_slot_config_t configuration of SDMMC host slot
 */
#define SDMMC_PF_SLOT_CONFIG_DEFAULT()	SDMMC_SLOT_CONFIG_DEFAULT()

/**
 * SD/MMC Host description
 *
 * This structure defines properties of SD/MMC host and functions
 * of SD/MMC host which can be used by upper layers.
 */
typedef sdmmc_host_t	sdmmc_host_pf_t;

/**
 * SD/MMC card information structure
 */
typedef sdmmc_card_t	sdmmc_pf_card_t;

/**
 * Extra configuration for SDMMC peripheral slot
 */
typedef sdmmc_slot_config_t	sdmmc_pf_slot_config_t;

/**
 * Extra configuration for SPI host
 */
typedef sdspi_slot_config_t	sdspi_pf_slot_config_t;

/**
 * @brief Initialize SDMMC host peripheral
 *
 * @note This function is not thread safe
 *
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_INVALID_STATE if sdmmc_host_pf_init was already called
 *      - CORE_ERR_NO_MEM if memory can not be allocated
 */
#define sdmmc_host_pf_init()	sdmmc_host_init()

/**
 * @brief Initialize given slot of SDMMC peripheral
 *
 * On the ESP32, SDMMC peripheral has two slots:
 *  - Slot 0: 8-bit wide, maps to HS1_* signals in PIN MUX
 *  - Slot 1: 4-bit wide, maps to HS2_* signals in PIN MUX
 * But the core framework supports only 1 slot, i.e. Slot 1: 4-bit wide
 *
 * Card detect and write protect signals can be routed to
 * arbitrary GPIOs using GPIO matrix.
 *
 * @note This function is not thread safe
 *
 * @param slot_config  additional configuration for the slot
 *
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_INVALID_STATE if host has not been initialized using sdmmc_host_pf_init
 */
#define sdmmc_host_pf_init_slot(slot_config)	sdmmc_host_init_slot(SDMMC_HOST_SLOT_1, /*const sdmmc_slot_config_t* */slot_config)

/**
 * @brief Select bus width to be used for data transfer
 *
 * SD/MMC card must be initialized prior to this command, and a command to set
 * bus width has to be sent to the card (e.g. SD_APP_SET_BUS_WIDTH)
 *
 * @note This function is not thread safe
 *
 * @param width  bus width (1 or 4)
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_INVALID_ARG if slot number or width is not valid
 */
#define sdmmc_host_pf_set_bus_width(slot, width)	sdmmc_host_set_bus_width(SDMMC_HOST_SLOT_1, /*size_t*/ width)

/**
 * @brief Get bus width configured in ``sdmmc_host_pf_init_slot`` to be used for data transfer
 *
 * @return configured bus width of the specified slot.
 */
#define sdmmc_host_pf_get_slot_width()	sdmmc_host_get_slot_width(SDMMC_HOST_SLOT_1)

/**
 * @brief Set card clock frequency
 *
 * Supported values are:
 *	SDMMC_PF_FREQ_DEFAULT	(20000 KHz)
 *	SDMMC_PF_FREQ_HIGHSPEED	(40000 KHz)
 *	SDMMC_PF_FREQ_PROBING	(400 KHz)
 *	SDMMC_PF_FREQ_52M		(52000 KHz)
 *	SDMMC_PF_FREQ_26M		(26000 KHz)
 *
 * @note This function is not thread safe
 *
 * @param freq_khz  card clock frequency, in kHz
 * @return
 *      - CORE_OK on success
 *      - other error codes may be returned in the future
 */
#define sdmmc_host_pf_set_card_clk(freq_khz)			sdmmc_host_set_card_clk(SDMMC_HOST_SLOT_1, /*uint32_t*/ freq_khz)

/**
 * @brief Enable or disable DDR mode of SD interface
 *
 * @param ddr_enabled  enable or disable DDR mode
 *
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_NOT_SUPPORTED if DDR mode is not supported on this slot
 */
#define sdmmc_host_pf_set_bus_ddr_mode(ddr_enabled)		sdmmc_host_set_bus_ddr_mode(SDMMC_HOST_SLOT_1, /*bool*/ ddr_enabled);

/**
 * @brief Send command to the card and get response
 *
 * This function returns when command is sent and response is received,
 * or data is transferred, or timeout occurs.
 *
 * @note This function is not thread safe w.r.t. init/deinit functions,
 *       and bus width/clock speed configuration functions. Multiple tasks
 *       can call sdmmc_host_do_transaction as long as other sdmmc_host_*
 *       functions are not called.
 *
 * @attention Data buffer passed in cmdinfo->data must be in DMA capable memory
 *
 * @param cmdinfo   pointer to structure describing command and data to transfer
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_TIMEOUT if response or data transfer has timed out
 *      - CORE_ERR_INVALID_CRC if response or data transfer CRC check has failed
 *      - CORE_ERR_INVALID_RESPONSE if the card has sent an invalid response
 *      - CORE_ERR_INVALID_SIZE if the size of data transfer is not valid in SD protocol
 *      - CORE_ERR_INVALID_ARG if the data buffer is not in DMA capable memory
 */
#define sdmmc_host_pf_do_transaction(cmdinfo)		sdmmc_host_do_transaction(SDMMC_HOST_SLOT_1, /*sdmmc_command_t* */ cmdinfo);

/**
 * @brief Enable IO interrupts
 *
 * This function configures the host to accept SDIO interrupts.
 *
 * @return returns CORE_OK, other errors possible in the future
 */
#define sdmmc_host_pf_io_int_enable()	sdmmc_host_io_int_enable(SDMMC_HOST_SLOT_1);

/**
 * @brief Block until an SDIO interrupt is received, or timeout occurs
 *
 * @param timeout_ticks  number of RTOS ticks to wait for the interrupt
 *
 * @return
 *  - CORE_OK on success (interrupt received)
 *  - CORE_ERR_TIMEOUT if the interrupt did not occur within timeout_ticks
 */
#define sdmmc_host_pf_io_int_wait(timeout_ticks)	sdmmc_host_io_int_wait(SDMMC_HOST_SLOT_1, /*TickType_t*/ timeout_ticks);

/**
 * @brief Disable SDMMC host and release allocated resources
 *
 * @note This function is not thread safe
 *
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_INVALID_STATE if sdmmc_host_init function has not been called
 */
#define sdmmc_host_pf_deinit()	sdmmc_host_deinit();

/**
 * @brief Enable the pull-ups of sd pins.
 *
 * @note You should always place actual pullups on the lines instead of using
 * this function. Internal pullup resistance are high and not sufficient, may
 * cause instability in products. This is for debug or examples only.
 *
 * @param width Bit width of your configuration, 1 or 4.
 *
 * @return
 *      - CORE_OK: if success
 *      - CORE_ERR_INVALID_ARG: if configured width larger than maximum the slot can support
 */
#define sdmmc_host_pf_pullup_en(width)	sdmmc_host_pullup_en(SDMMC_HOST_SLOT_1, /*int*/ width);


#ifdef __cplusplus
}
#endif

#endif /* SDMMC_HOST_PLATFORM_H_ */
