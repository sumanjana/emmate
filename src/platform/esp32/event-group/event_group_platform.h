/*
 * File Name: event_group_platform.h
 * File Path: /emmate/src/platform/esp/event-group/event_group_platform.h
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef EVENT_GROUP_PLATFORM_H_
#define EVENT_GROUP_PLATFORM_H_

#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

/* Wrapper typedefs for abstracting the platform specific names */
typedef EventGroupHandle_t	EventGroupPfHandle;
typedef EventBits_t 		EventPfBits;


#define EventPfMaxDelay			portMAX_DELAY
#define CONFIG_USE_16_BIT_TICKS	configUSE_16_BIT_TICKS

/*
 * The number of bits (or flags) stored within an event group is 8 if configUSE_16_BIT_TICKS is set to 1,
 * or 24 if configUSE_16_BIT_TICKS is set to 0. The dependency on configUSE_16_BIT_TICKS results from the
 * data type used for thread local storage in the internal implementation of tasks.
 * "URL: https://www.freertos.org/FreeRTOS-Event-Groups.html
 */

//Event Platform Bits
#if( configUSE_16_BIT_TICKS == 0 )

#define EVT_PTF_BIT31 	BIT31
#define EVT_PTF_BIT30	BIT30
#define EVT_PTF_BIT29	BIT29
#define EVT_PTF_BIT28	BIT28
#define EVT_PTF_BIT27	BIT27
#define EVT_PTF_BIT26	BIT26
#define EVT_PTF_BIT25	BIT25
#define EVT_PTF_BIT24	BIT24
#define EVT_PTF_BIT23	BIT23
#define EVT_PTF_BIT22	BIT22
#define EVT_PTF_BIT21	BIT21
#define EVT_PTF_BIT20	BIT20
#define EVT_PTF_BIT19	BIT19
#define EVT_PTF_BIT18	BIT18
#define EVT_PTF_BIT17	BIT17
#define EVT_PTF_BIT16	BIT16
#define EVT_PTF_BIT15	BIT15
#define EVT_PTF_BIT14	BIT14
#define EVT_PTF_BIT13	BIT13
#define EVT_PTF_BIT12	BIT12
#define EVT_PTF_BIT11	BIT11
#define EVT_PTF_BIT10	BIT10
#define EVT_PTF_BIT9	BIT9

#endif

#define EVT_PTF_BIT8	BIT8
#define EVT_PTF_BIT7	BIT7
#define EVT_PTF_BIT6	BIT6
#define EVT_PTF_BIT5	BIT5
#define EVT_PTF_BIT4	BIT4
#define EVT_PTF_BIT3 	BIT3
#define EVT_PTF_BIT2 	BIT2
#define EVT_PTF_BIT1 	BIT1
#define EVT_PTF_BIT0 	BIT0

/* Wrapper functions for abstracting the platform specific names */
#define event_group_ptf_create()									xEventGroupCreate()

#define event_group_ptf_wait_bits(eventgroup, \
		bitstowaitfor, clearonexit, waitforallbits, tickstowait)	xEventGroupWaitBits( /*const EventGroupHandle_t*/ eventgroup, /*const EventBits_t*/ bitstowaitfor, /*const BaseType_t*/ clearonexit, /*const BaseType_t*/ waitforallbits, /*TickType_t*/ tickstowait )
#define event_group_ptf_set_bits( eventgroup,bitstoset )			xEventGroupSetBits( /*EventGrupHandle_t*/ eventgroup, /*const EventBits_t*/ bitstoset )
#define event_group_ptf_set_bits_from_isr(eventgroup, bitstoset, \
		higherprioritytaskwoken )									xEventGroupSetBitsFromISR(/*EventGroupHandle_t*/ eventgroup, /*const EventBits_t*/ bitstoset, /*BaseType_t * */ higherprioritytaskwoken )
#define event_group_ptf_clear_bits(eventgroup, bitstoclear)			xEventGroupClearBits( /*EventGroupHandle_t*/ eventgroup, /*const EventBits_t*/ bitstoclear)
#define event_group_ptf_clear_bits_from_isr(eventgroup, bitstoclear)		xEventGroupClearBitsFromISR( /*EventGroupHandle_t*/ eventgroup, /*const EventBits_t*/ bitstoclear )
#define event_group_ptf_get_bits(eventgroup)						xEventGroupGetBits( /*EventGroupHandle_t*/ eventgroup )
#define event_group_ptf_get_bits_from_isr(eventgroup)				xEventGroupGetBitsFromISR( /*EventGroupHandle_t*/ eventgroup )
#define event_group_ptf_sync( eventgroup, bitstoset, \
		bitstowaitfor, tickstowait  ) 								xEventGroupSync( /*EventGroupHandle_t*/ eventgroup, /*const EventBits_t*/ bitstoset, /*const EventBits_t*/ bitstowaitfor, /*TickType_t*/ tickstowait )
#define event_group_ptf_delete(eventgroup)							vEventGroupDelete(/*EventGroupHandle_t*/ eventgroup )

#endif /* EVENT_GROUP_PLATFORM_H_ */
