/*
 * File Name: led_pwm_driver_platform.h
 *
 * Description:
 *
 *  Created on: 23-Sep-2019
 *      Author: Noyel Seth
 */

#ifndef LED_PWM_DRIVER_PLATFORM_H_
#define LED_PWM_DRIVER_PLATFORM_H_

#include "driver/ledc.h"
#include "core_error.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	LED_PWM_PF_HIGH_SPEED_MODE = LEDC_HIGH_SPEED_MODE, /*!< LEDC high speed speed_mode */
	LED_PWM_PF_LOW_SPEED_MODE = LEDC_LOW_SPEED_MODE, /*!< LEDC low speed speed_mode */
	LED_PWM_PF_SPEED_MODE_MAX = LEDC_SPEED_MODE_MAX, /*!< LEDC speed limit */
} led_pwm_pf_mode_t;

typedef enum {
	LED_PWM_PF_INTR_DISABLE = LEDC_INTR_DISABLE, /*!< Disable LEDC interrupt */
	LED_PWM_PF_INTR_FADE_END = LEDC_INTR_FADE_END, /*!< Enable LEDC interrupt */
} led_pwm_pf_intr_type_t;

typedef enum {
	LED_PWM_PF_DUTY_DIR_DECREASE = LEDC_DUTY_DIR_DECREASE, /*!< LEDC duty decrease direction */
	LED_PWM_PF_DUTY_DIR_INCREASE = LEDC_DUTY_DIR_INCREASE, /*!< LEDC duty increase direction */
	LED_PWM_PF_DUTY_DIR_MAX = LEDC_DUTY_DIR_MAX,
} led_pwm_pf_duty_direction_t;

typedef enum {
	LED_PWM_PF_REF_TICK = LEDC_REF_TICK, /*!< LEDC timer clock divided from reference tick (1Mhz) */
	LED_PWM_PF_APB_CLK = LEDC_APB_CLK, /*!< LEDC timer clock divided from APB clock (80Mhz) */
} led_pwm_pf_clk_src_t;

typedef enum {
	LED_PWM_PF_AUTO_CLK = LEDC_AUTO_CLK, /*!< The driver will automatically select the source clock(REF_TICK or APB) based on the giving resolution and duty parameter when init the timer*/
	LED_PWM_PF_USE_REF_TICK = LEDC_USE_REF_TICK, /*!< LEDC timer select REF_TICK clock as source clock*/
	LED_PWM_PF_USE_APB_CLK = LEDC_USE_APB_CLK, /*!< LEDC timer select APB clock as source clock*/
	LED_PWM_PF_USE_RTC8M_CLK = LEDC_USE_RTC8M_CLK, /*!< LEDC timer select RTC8M_CLK as source clock. Only for low speed channels and this parameter must be the same for all low speed channels*/
} led_pwm_pf_clk_cfg_t;

typedef enum {
	LED_PWM_PF_TIMER_0 = LEDC_TIMER_0,/*!< LEDC timer 0 */
	LED_PWM_PF_TIMER_1 = LEDC_TIMER_1, /*!< LEDC timer 1 */
	LED_PWM_PF_TIMER_2 = LEDC_TIMER_2, /*!< LEDC timer 2 */
	LED_PWM_PF_TIMER_3 = LEDC_TIMER_3, /*!< LEDC timer 3 */
	LED_PWM_PF_TIMER_MAX = LEDC_TIMER_MAX
} led_pwm_pf_timer_t;

typedef enum {
	LED_PWM_PF_CHANNEL_0 = LEDC_CHANNEL_0, /*!< LEDC channel 0 */
	LED_PWM_PF_CHANNEL_1 = LEDC_CHANNEL_1, /*!< LEDC channel 1 */
	LED_PWM_PF_CHANNEL_2 = LEDC_CHANNEL_2, /*!< LEDC channel 2 */
	LED_PWM_PF_CHANNEL_3 = LEDC_CHANNEL_3, /*!< LEDC channel 3 */
	LED_PWM_PF_CHANNEL_4 = LEDC_CHANNEL_4, /*!< LEDC channel 4 */
	LED_PWM_PF_CHANNEL_5 = LEDC_CHANNEL_5, /*!< LEDC channel 5 */
	LED_PWM_PF_CHANNEL_6 = LEDC_CHANNEL_6, /*!< LEDC channel 6 */
	LED_PWM_PF_CHANNEL_7 = LEDC_CHANNEL_7, /*!< LEDC channel 7 */
	LED_PWM_PF_CHANNEL_MAX = LEDC_CHANNEL_MAX
} led_pwm_pf_channel_t;

typedef enum {
	LED_PWM_PF_TIMER_1_BIT = LEDC_TIMER_1_BIT, /*!< LEDC PWM duty resolution of  1 bits */
	LED_PWM_PF_TIMER_2_BIT = LEDC_TIMER_2_BIT, /*!< LEDC PWM duty resolution of  2 bits */
	LED_PWM_PF_TIMER_3_BIT = LEDC_TIMER_3_BIT, /*!< LEDC PWM duty resolution of  3 bits */
	LED_PWM_PF_TIMER_4_BIT = LEDC_TIMER_4_BIT, /*!< LEDC PWM duty resolution of  4 bits */
	LED_PWM_PF_TIMER_5_BIT = LEDC_TIMER_5_BIT, /*!< LEDC PWM duty resolution of  5 bits */
	LED_PWM_PF_TIMER_6_BIT = LEDC_TIMER_6_BIT, /*!< LEDC PWM duty resolution of  6 bits */
	LED_PWM_PF_TIMER_7_BIT = LEDC_TIMER_7_BIT, /*!< LEDC PWM duty resolution of  7 bits */
	LED_PWM_PF_TIMER_8_BIT = LEDC_TIMER_8_BIT, /*!< LEDC PWM duty resolution of  8 bits */
	LED_PWM_PF_TIMER_9_BIT = LEDC_TIMER_9_BIT, /*!< LEDC PWM duty resolution of  9 bits */
	LED_PWM_PF_TIMER_10_BIT = LEDC_TIMER_10_BIT, /*!< LEDC PWM duty resolution of 10 bits */
	LED_PWM_PF_TIMER_11_BIT = LEDC_TIMER_11_BIT, /*!< LEDC PWM duty resolution of 11 bits */
	LED_PWM_PF_TIMER_12_BIT = LEDC_TIMER_12_BIT, /*!< LEDC PWM duty resolution of 12 bits */
	LED_PWM_PF_TIMER_13_BIT = LEDC_TIMER_13_BIT, /*!< LEDC PWM duty resolution of 13 bits */
	LED_PWM_PF_TIMER_14_BIT = LEDC_TIMER_14_BIT, /*!< LEDC PWM duty resolution of 14 bits */
	LED_PWM_PF_TIMER_15_BIT = LEDC_TIMER_15_BIT, /*!< LEDC PWM duty resolution of 15 bits */
	LED_PWM_PF_TIMER_16_BIT = LEDC_TIMER_16_BIT, /*!< LEDC PWM duty resolution of 16 bits */
	LED_PWM_PF_TIMER_17_BIT = LEDC_TIMER_17_BIT, /*!< LEDC PWM duty resolution of 17 bits */
	LED_PWM_PF_TIMER_18_BIT = LEDC_TIMER_18_BIT, /*!< LEDC PWM duty resolution of 18 bits */
	LED_PWM_PF_TIMER_19_BIT = LEDC_TIMER_19_BIT, /*!< LEDC PWM duty resolution of 19 bits */
	LED_PWM_PF_TIMER_20_BIT = LEDC_TIMER_20_BIT, /*!< LEDC PWM duty resolution of 20 bits */
	LED_PWM_PF_TIMER_BIT_MAX = LEDC_TIMER_BIT_MAX
} led_pwm_pf_timer_bit_t;

typedef enum {
	LED_PWM_PF_FADE_NO_WAIT = LEDC_FADE_NO_WAIT, /*!< LEDC fade function will return immediately */
	LED_PWM_PF_FADE_WAIT_DONE = LEDC_FADE_WAIT_DONE, /*!< LEDC fade function will block untLED_PWMfading to the target duty */
	LED_PWM_PF_FADE_MAX = LEDC_FADE_MAX,
} led_pwm_pf_fade_mode_t;

///**
// * @brief Configuration parameters of LEDC channel for ledc_channel_config function
// */
//typedef struct {
//	int gpio_num; /*!< the LEDC output gpio_num, if you want to use gpio16, gpio_num = 16 */
//	led_pwm_pf_mode_t speed_mode; /*!< LEDC speed speed_mode, high-speed mode or low-speed mode */
//	led_pwm_pf_channel_t channel; /*!< LEDC channel (0 - 7) */
//	led_pwm_pf_intr_type_t intr_type; /*!< configure interrupt, Fade interrupt enable  or Fade interrupt disable */
//	led_pwm_pf_timer_t timer_sel; /*!< Select the timer source of channel (0 - 3) */
//	uint32_t duty; /*!< LEDC channel duty, the range of duty setting is [0, (2**duty_resolution)] */
//	int hpoint; /*!< LEDC channel hpoint value, the max value is 0xfffff */
//} led_pwm_pf_channel_config_t;

///**
// * @brief Configuration parameters of LEDC Timer timer for ledc_timer_config function
// */
//typedef struct {
//	led_pwm_pf_mode_t speed_mode; /*!< LEDC speed speed_mode, high-speed mode or low-speed mode */
//	union {
//		led_pwm_pf_timer_bit_t duty_resolution; /*!< LEDC channel duty resolution */
//		led_pwm_pf_timer_bit_t bit_num __attribute__((deprecated)); /*!< Deprecated in ESP-IDF 3.0. This is an alias to 'duty_resolution' for backward compatibility with ESP-IDF 2.1 */
//	};
//	led_pwm_pf_timer_t timer_num; /*!< The timer source of channel (0 - 3) */
//	uint32_t freq_hz; /*!< LEDC timer frequency (Hz) */
//	led_pwm_pf_clk_cfg_t clk_cfg; /*!< Configure LEDC source clock.
//	 For low speed channels and high speed channels, you can specify the source clock using LEDC_USE_REF_TICK, LEDC_USE_APB_CLK or LEDC_AUTO_CLK.
//	 For low speed channels, you can also specify the source clock using LEDC_USE_RTC8M_CLK, in this case, all low speed channel's source clock must be RTC8M_CLK*/
//} led_pwm_pf_timer_config_t;

typedef ledc_isr_handle_t led_pwm_pf_isr_handle_t;

/**
 * @brief LEDC channel configuration
 *        Configure LEDC channel with the given channel/output gpio_num/interrupt/source timer/frequency(Hz)/LEDC duty resolution
 *
 * @param ledc_conf Pointer of LEDC channel configure struct
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define  led_pwm_pf_channel_config(ledc_conf)  ledc_channel_config(/* const ledc_channel_config_t* */ ledc_conf)

/**
 * @brief LEDC timer configuration
 *        Configure LEDC timer with the given source timer/frequency(Hz)/duty_resolution
 *
 * @param  timer_conf Pointer of LEDC timer configure struct
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Can not find a proper pre-divider number base on the given frequency and the current duty_resolution.
 */
#define led_pwm_pf_timer_config(timer_conf)		ledc_timer_config(/* const ledc_timer_config_t* */ timer_conf)

/**
 * @brief LEDC update channel parameters
 * @note  Call this function to activate the LEDC updated parameters.
 *        After ledc_set_duty, we need to call this function to update the settings.
 * @note  ledc_set_duty, ledc_set_duty_with_hpoint and ledc_update_duty are not thread-safe, do not call these functions to
 *        control one LEDC channel in different tasks at the same time.
 *        A thread-safe version of API is ledc_set_duty_and_update
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode,
 * @param channel LEDC channel (0-7), select from ledc_channel_t
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *
 */
#define led_pwm_pf_update_duty(speed_mode,  channel)		ledc_update_duty(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel)

/**
 * @brief LEDC stop.
 *        Disable LEDC output, and set idle level
 *
 * @param  speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param  channel LEDC channel (0-7), select from ledc_channel_t
 * @param  idle_level Set output idle level after LEDC stops.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define led_pwm_pf_stop(speed_mode, channel, idle_level)	ledc_stop(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ idle_level)

/**
 * @brief LEDC set channel frequency (Hz)
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param  timer_num LEDC timer index (0-3), select from ledc_timer_t
 * @param  freq_hz Set the LEDC frequency
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Can not find a proper pre-divider number base on the given frequency and the current duty_resolution.
 */
#define led_pwm_pf_set_freq(speed_mode,  timer_num, freq_hz)		ledc_set_freq(/*ledc_mode_t*/ speed_mode, /*ledc_timer_t*/ timer_num, /*uint32_t*/ freq_hz)

/**
 * @brief      LEDC get channel frequency (Hz)
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param timer_num LEDC timer index (0-3), select from ledc_timer_t
 *
 * @return
 *     - 0  error
 *     - Others Current LEDC frequency
 */
#define led_pwm_pf_get_freq(speed_mode, timer_num)	ledc_get_freq(/*ledc_mode_t*/ speed_mode, /*ledc_timer_t*/ timer_num)

/**
 * @brief LEDC set duty and hpoint value
 *        Only after calling ledc_update_duty will the duty update.
 * @note  ledc_set_duty, ledc_set_duty_with_hpoint and ledc_update_duty are not thread-safe, do not call these functions to
 *        control one LEDC channel in different tasks at the same time.
 *        A thread-safe version of API is ledc_set_duty_and_update
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel (0-7), select from ledc_channel_t
 * @param duty Set the LEDC duty, the range of duty setting is [0, (2**duty_resolution)]
 * @param hpoint Set the LEDC hpoint value(max: 0xfffff)
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define led_pwm_pf_set_duty_with_hpoint(speed_mode, channel, duty, hpoint)	ledc_set_duty_with_hpoint(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ duty, /*uint32_t*/ hpoint)

/**
 * @brief LEDC get hpoint value, the counter value when the output is set high level.
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel (0-7), select from ledc_channel_t
 * @return
 *     - LEDC_ERR_VAL if parameter error
 *     - Others Current hpoint value of LEDC channel
 */
#define led_pwm_pf_get_hpoint( speed_mode, channel)		ledc_get_hpoint(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel)

/**
 * @brief LEDC set duty
 *        This function do not change the hpoint value of this channel. if needed, please call ledc_set_duty_with_hpoint.
 *        only after calling ledc_update_duty will the duty update.
 * @note  ledc_set_duty, ledc_set_duty_with_hpoint and ledc_update_duty are not thread-safe, do not call these functions to
 *        control one LEDC channel in different tasks at the same time.
 *        A thread-safe version of API is ledc_set_duty_and_update.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel (0-7), select from ledc_channel_t
 * @param duty Set the LEDC duty, the range of duty setting is [0, (2**duty_resolution)]
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define led_pwm_pf_set_duty( speed_mode, channel, duty)		ledc_set_duty(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ duty)

/**
 * @brief LEDC get duty
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel (0-7), select from ledc_channel_t
 *
 * @return
 *     - LEDC_ERR_DUTY if parameter error
 *     - Others Current LEDC duty
 */
#define led_pwm_pf_get_duty(speed_mode, channel)		ledc_get_duty(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel)

/**
 * @brief LEDC set gradient
 *        Set LEDC gradient, After the function calls the ledc_update_duty function, the function can take effect.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel (0-7), select from ledc_channel_t
 * @param duty Set the start of the gradient duty, the range of duty setting is [0, (2**duty_resolution)]
 * @param fade_direction Set the direction of the gradient
 * @param step_num Set the number of the gradient
 * @param duty_cyle_num Set how many LEDC tick each time the gradient lasts
 * @param duty_scale Set gradient change amplitude
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define led_pwm_pf_set_fade(speed_mode, channel, duty, fade_direction, \
                        step_num, duty_cyle_num, duty_scale)				ledc_set_fade(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ duty, /*ledc_duty_direction_t*/ fade_direction, \
                                																/*uint32_t*/ step_num, /*uint32_t*/ duty_cyle_num, /*uint32_t*/ duty_scale)

/**
 * @brief Register LEDC interrupt handler, the handler is an ISR.
 *        The handler will be attached to the same CPU core that this function is running on.
 *
 * @param fn Interrupt handler function.
 * @param arg User-supplied argument passed to the handler function.
 * @param intr_alloc_flags Flags used to allocate the interrupt. One or multiple (ORred)
 *        ESP_INTR_FLAG_* values. See esp_intr_alloc.h for more info.
 * @param arg Parameter for handler function
 * @param handle Pointer to return handle. If non-NULL, a handle for the interrupt will
 *        be returned here.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Function pointer error.
 */
#define led_pwm_pf_isr_register(fn, arg, intr_alloc_flags, handle)		ledc_isr_register(/* void (* */fn /*)(void*)*/, /*void * */ arg, /*int*/ intr_alloc_flags, /*ledc_isr_handle_t * */handle)

/**
 * @brief Configure LEDC settings
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param timer_sel  Timer index (0-3), there are 4 timers in LEDC module
 * @param clock_divider Timer clock divide value, the timer clock is divided from the selected clock source
 * @param duty_resolution Resolution of duty setting in number of bits. The range of duty values is [0, (2**duty_resolution)]
 * @param clk_src Select LEDC source clock.
 *
 * @return
 *     - (-1) Parameter error
 *     - Other Current LEDC duty
 */
#define led_pwm_pf_timer_set(speed_mode, timer_sel, clock_divider, duty_resolution, clk_src)		ledc_timer_set(/*ledc_mode_t*/ speed_mode, /*ledc_timer_t*/ timer_sel, /*uint32_t*/ clock_divider, /*uint32_t*/ duty_resolution, /*ledc_clk_src_t*/ clk_src)

/**
 * @brief Reset LEDC timer
 *
 * @param  speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param  timer_sel LEDC timer index (0-3), select from ledc_timer_t
 *
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 */
#define led_pwm_pf_timer_rst(speed_mode, timer_sel)		ledc_timer_rst(/*ledc_mode_t*/ speed_mode, uint32_t*/ timer_sel)

/**
 * @brief Pause LEDC timer counter
 *
 * @param  speed_mode  Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param  timer_sel LEDC timer index (0-3), select from ledc_timer_t
 *
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 *
 */
#define led_pwm_pf_timer_pause(speed_mode, timer_sel)		ledc_timer_pause(/*ledc_mode_t*/ speed_mode, /*uint32_t*/ timer_sel)

/**
 * @brief Resume LEDC timer
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param timer_sel LEDC timer index (0-3), select from ledc_timer_t
 *
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 */
#define led_pwm_pf_timer_resume(speed_mode, timer_sel)		ledc_timer_resume(/*ledc_mode_t speed_mode*/, /*uint32_t*/ timer_sel)

/**
 * @brief Bind LEDC channel with the selected timer
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel index (0-7), select from ledc_channel_t
 * @param timer_idx LEDC timer index (0-3), select from ledc_timer_t
 *
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 */
#define led_pwm_pf_bind_channel_timer(speed_mode, channel, timer_idx)		ledc_bind_channel_timer(/*ledc_mode_t*/ speed_mode, /*uint32_t*/ channel, /*uint32_t*/ timer_idx)

/**
 * @brief Set LEDC fade function.
 * @note  Call ledc_fade_func_install() once before calling this function.
 *        Call ledc_fade_start() after this to start fading.
 * @note  ledc_set_fade_with_step, ledc_set_fade_with_time and ledc_fade_start are not thread-safe, do not call these functions to
 *        control one LEDC channel in different tasks at the same time.
 *        A thread-safe version of API is ledc_set_fade_step_and_start
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode,
 * @param channel LEDC channel index (0-7), select from ledc_channel_t
 * @param target_duty Target duty of fading [0, (2**duty_resolution) - 1]
 * @param scale Controls the increase or decrease step scale.
 * @param cycle_num increase or decrease the duty every cycle_num cycles
 *
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function not installed.
 *     - ESP_FAIL Fade function init error
 */
#define led_pwm_pf_set_fade_with_step(speed_mode, channel, target_duty, scale, cycle_num)		ledc_set_fade_with_step(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ target_duty, /*uint32_t*/ scale, /*uint32_t*/ cycle_num)

/**
 * @brief Set LEDC fade function, with a limited time.
 * @note  Call ledc_fade_func_install() once before calling this function.
 *        Call ledc_fade_start() after this to start fading.
 * @note  ledc_set_fade_with_step, ledc_set_fade_with_time and ledc_fade_start are not thread-safe, do not call these functions to
 *        control one LEDC channel in different tasks at the same time.
 *        A thread-safe version of API is ledc_set_fade_step_and_start
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode,
 * @param channel LEDC channel index (0-7), select from ledc_channel_t
 * @param target_duty Target duty of fading.( 0 - (2 ** duty_resolution - 1)))
 * @param max_fade_time_ms The maximum time of the fading ( ms ).
 *
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function not installed.
 *     - ESP_FAIL Fade function init error
 */
#define led_pwm_pf_set_fade_with_time(speed_mode, channel, target_duty, max_fade_time_ms)		ledc_set_fade_with_time(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ target_duty, /*int*/ max_fade_time_ms)

/**
 * @brief Install LEDC fade function. This function will occupy interrupt of LEDC module.
 * @param intr_alloc_flags Flags used to allocate the interrupt. One or multiple (ORred)
 *        ESP_INTR_FLAG_* values. See esp_intr_alloc.h for more info.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
#define led_pwm_pf_fade_func_install(intr_alloc_flags)	ledc_fade_func_install(/*int*/ intr_alloc_flags)

/**
 * @brief Uninstall LEDC fade function.
 *
 */
#define led_pwm_pf_fade_func_uninstall(void)	ledc_fade_func_uninstall(void)

/**
 * @brief Start LEDC fading.
 * @note  Call ledc_fade_func_install() once before calling this function.
 *        Call this API right after ledc_set_fade_with_time or ledc_set_fade_with_step before to start fading.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel number
 * @param fade_mode Whether to block until fading done.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function not installed.
 *     - ESP_ERR_INVALID_ARG Parameter error.
 */
#define led_pwm_pf_fade_start(speed_mode, channel, fade_mode)		ledc_fade_start(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*ledc_fade_mode_t*/ fade_mode)

/**
 * @brief A thread-safe API to set duty for LEDC channel and return when duty updated.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode
 * @param channel LEDC channel (0-7), select from ledc_channel_t
 * @param duty Set the LEDC duty, the range of duty setting is [0, (2**duty_resolution)]
 * @param hpoint Set the LEDC hpoint value(max: 0xfffff)
 *
 */
#define led_pwm_pf_set_duty_and_update(speed_mode, channel, duty, hpoint)		ledc_set_duty_and_update(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ duty, /*uint32_t*/ hpoint)

/**
 * @brief A thread-safe API to set and start LEDC fade function, with a limited time.
 * @note  Call ledc_fade_func_install() once, before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode,
 * @param channel LEDC channel index (0-7), select from ledc_channel_t
 * @param target_duty Target duty of fading.( 0 - (2 ** duty_resolution - 1)))
 * @param max_fade_time_ms The maximum time of the fading ( ms ).
 * @param fade_mode choose blocking or non-blocking mode
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function not installed.
 *     - ESP_FAIL Fade function init error
 */
#define led_pwm_pf_set_fade_time_and_start(speed_mode, channel, target_duty, max_fade_time_ms, fade_mode)		ledc_set_fade_time_and_start(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ target_duty, /*uint32_t*/ max_fade_time_ms, /*ledc_fade_mode_t*/ fade_mode)

/**
 * @brief A thread-safe API to set and start LEDC fade function.
 * @note  Call ledc_fade_func_install() once before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 * @param speed_mode Select the LEDC speed_mode, high-speed mode and low-speed mode,
 * @param channel LEDC channel index (0-7), select from ledc_channel_t
 * @param target_duty Target duty of fading [0, (2**duty_resolution) - 1]
 * @param scale Controls the increase or decrease step scale.
 * @param cycle_num increase or decrease the duty every cycle_num cycles
 * @param fade_mode choose blocking or non-blocking mode
 * @return
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function not installed.
 *     - ESP_FAIL Fade function init error
 */
#define led_pwm_pf_set_fade_step_and_start(speed_mode, channel, target_duty, scale, cycle_num, fade_mode)		ledc_set_fade_step_and_start(/*ledc_mode_t*/ speed_mode, /*ledc_channel_t*/ channel, /*uint32_t*/ target_duty, /*uint32_t*/ scale, /*uint32_t*/ cycle_num, /*ledc_fade_mode_t*/ fade_mode)

/********************************************************* **********************************************************/

/**
 * @brief LED PWM timer configuration
 *        Configure LED PWM timer with the given source timer, frequency(Hz), duty_resolution
 *
 * @param  duty_res 	LED PWM channel duty resolution
 *
 * @param  freq_hz		LED PWM frequency (Hz)
 *
 * @param  timer		LED PWM timer source of timer (0 - 3)
 *
 * @return
 *     - CORE_OK Success
 *     - CORE_ERR_INVALID_ARG Parameter error
 *     - CORE_FAIL Can not find a proper pre-divider number base on the given frequency and the current duty_resolution.
 */
core_err led_pwm_pf_driver_configure_timer(led_pwm_pf_timer_bit_t duty_res, uint32_t freq_hz, led_pwm_pf_timer_t timer);

 /**
  * @brief LED PWM channel configuration
  *        Configure LED PWM channel with the given channel/ LED PWM duty resolution / output gpio_num / source timer
  *
  * @param channel 	LED PWM channel led_pwm_pf_channel_t (0 - 7)
  *
  * @param duty		LED PWM channel duty, the range of duty setting is [0, (2^duty_resolution)]
  * @note			If Timer's Duty resolution configure as  13bit then Max Duty resolution will be 2^13=8192
  * 				and lowest Duty resolution always 0
  *
  * @param gpio_num	LED PWM output gpio_num
  *
  * @param timer	Select the LED PWM timer source of channel (0 - 3)
  *
  * @return
  *     - CORE_OK Success
  *     - CORE_ERR_INVALID_ARG Parameter error
  */
 core_err led_pwm_pf_driver_configure_channel(led_pwm_pf_channel_t channel, uint32_t duty, int gpio_num,
 		led_pwm_pf_timer_t timer) ;

 /**
  * @brief LED PWM stop.
  *        Disable LED PWM output, and set idle level
  *
  * @param  channel LED PWM channel (0-7), select from led_pwm_pf_channel_t
  *
  * @return
  *     - CORE_OK Success
  *     - CORE_ERR_INVALID_ARG Parameter error
  */
core_err led_pwm_pf_driver_stop(led_pwm_pf_channel_t channel);

/**
 * @brief A thread-safe API to set duty for LED PWM channel and return when duty updated.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param  channel 	LED PWM channel (0-7), select from led_pwm_pf_channel_t
 *
 * @param duty		LED PWM channel duty, the range of duty setting is [0, (2^duty_resolution)]
 * @note			If Timer's Duty resolution configure as  13bit then Max Duty resolution will be 2^13=8192
 * 					and lowest Duty resolution always 0
 *
 * @return
 *     - CORE_ERR_INVALID_ARG Parameter error
 *     - CORE_OK Success
 */
core_err led_pwm_pf_driver_set_duty_cycle(led_pwm_pf_channel_t channel, uint32_t duty);

/**
 * @brief A thread-safe API to set and start LED PWM fade function.
 * @note  Call led_pwm_pf_driver_fade_func_install() once before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel LED PWM channel index (0-7), select from led_pwm_pf_channel_t
 *
 * @param target_duty Target duty of fading [0, (2^duty_resolution) - 1]
 *
 * @param scale Controls the increase or decrease step scale.
 *
 * @param cycle_num increase or decrease the duty every cycle_num cycles

 * @return
 *     - CORE_ERR_INVALID_ARG Parameter error
 *     - CORE_OK Success
 *     - CORE_ERR_INVALID_STATE Fade function not installed.
 *     - CORE_FAIL Fade function init error
 */
core_err led_pwm_pf_driver_set_fade_step_and_start(led_pwm_pf_channel_t channel, uint32_t target_duty, uint32_t scale,
		uint32_t cycle_num);

/**
 * @brief A thread-safe API to set and start LED PWM fade function, with a limited time.
 * @note  Call led_pwm_pf_driver_fade_func_install() once, before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel LED PWM channel index (0-7), select from led_pwm_pf_channel_t
 *
 * @param target_duty Target duty of fading.( 0 - (2^duty_resolution - 1)))
 *
 * @param max_fade_time_ms The maximum time of the fading ( ms ).
 *
 * @return
 *     - CORE_ERR_INVALID_ARG Parameter error
 *     - CORE_OK Success
 *     - CORE_ERR_INVALID_STATE Fade function not installed.
 *     - CORE_FAIL Fade function init error
 */
core_err led_pwm_pf_driver_set_fade_time_and_start(led_pwm_pf_channel_t channel, uint32_t target_duty,
		uint32_t max_fade_time_ms);

/**
 * @brief Install LED PWM fade function. This function will help to execute LED PWM Fade Operation.
 *
 * @return
 *     - CORE_OK Success
 *     - CORE_ERR_INVALID_STATE Fade function already installed.
 */
core_err led_pwm_pf_driver_fade_func_install();

/**
 * @brief Uninstall LED PWM fade function. This function will help to stop execution of LED PWM Fade Operation.
 *
 *  @return
 *     - CORE_OK Success
 */
core_err led_pwm_pf_driver_fade_func_uninstall();

/**
 * @brief This function return the Max Duty Resolution value depend to LED PWM Configured Source Timer (0-3)
 *
 *  @return
 *     - Max Duty Resolution value
 */
uint32_t led_pwm_pf_driver_get_max_duty_by_timer(led_pwm_pf_timer_t timer);

/**
 * @brief This function return the Max Duty Resolution value depend to LED PWM Configured Channel (0-7)
 *
 *  @return
 *     - Max Duty Resolution value
 */
uint32_t led_pwm_pf_driver_get_max_duty_by_channel(led_pwm_pf_channel_t channel);

#ifdef __cplusplus
}
#endif

#endif /* LED_PWM_DRIVER_PLATFORM_H_ */
