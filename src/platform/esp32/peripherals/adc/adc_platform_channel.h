/*
 * File Name: adc_platform_channel.h
 * File Path: /emmate/src/platform/esp32/peripherals/adc/adc_platform_channel.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Noyel Seth
 */

#ifndef ADC_PLATFORM_CHANNEL_H_
#define ADC_PLATFORM_CHANNEL_H_

#include "thing.h"
#include "adc_platform.h"

/* Wrapper for abstracting the platform specific names */

/**************************** ADC Channel 1 ******************************************/

//#define ADC1_PF_GPIO36_CHANNEL     ADC1_PF_CHANNEL_0
#define ADC_1_PF_CHANNEL_0_GPIO 		SOM_PIN_101

/* ESP32 modules does not exposes GPIO 37 hence ESP32 SOM does not support this channel */
////#define ADC1_PF_GPIO37_CHANNEL     ADC1_PF_CHANNEL_1
//#define ADC_1_PF_CHANNEL_1_GPIO
/* ESP32 modules does not exposes GPIO 38 hence ESP32 SOM does not support this channel */
////#define ADC1_PF_GPIO38_CHANNEL     ADC1_PF_CHANNEL_2
//#define ADC_1_PF_CHANNEL_2_GPIO

//#define ADC1_PF_GPIO39_CHANNEL     ADC1_PF_CHANNEL_3
#define ADC_1_PF_CHANNEL_3_GPIO 		SOM_PIN_2

//#define ADC1_PF_GPIO32_CHANNEL     ADC1_PF_CHANNEL_4
#define ADC_1_PF_CHANNEL_4_GPIO 		SOM_PIN_88

//#define ADC1_PF_GPIO33_CHANNEL     ADC1_PF_CHANNEL_5
#define ADC_1_PF_CHANNEL_5_GPIO 		SOM_PIN_86

//#define ADC1_PF_GPIO34_CHANNEL     ADC1_PF_CHANNEL_6
#define ADC_1_PF_CHANNEL_6_GPIO 		SOM_PIN_36

//#define ADC1_PF_GPIO35_CHANNEL     ADC1_PF_CHANNEL_7
#define ADC_1_PF_CHANNEL_7_GPIO 		SOM_PIN_90

/**************************** ADC Channel 2 ******************************************/
//#define ADC2_PF_GPIO4_CHANNEL      ADC2_PF_CHANNEL_0
#define ADC_2_PF_CHANNEL_0_GPIO 		SOM_PIN_49

//#define ADC2_PF_GPIO0_CHANNEL      ADC2_PF_CHANNEL_1
#define ADC_2_PF_CHANNEL_1_GPIO 		SOM_PIN_98

//#define ADC2_PF_GPIO2_CHANNEL      ADC2_PF_CHANNEL_2
#define ADC_2_PF_CHANNEL_2_GPIO 		SOM_PIN_97

//#define ADC2_PF_GPIO15_CHANNEL     ADC2_PF_CHANNEL_3
#define ADC_2_PF_CHANNEL_3_GPIO 		SOM_PIN_190

//#define ADC2_PF_GPIO13_CHANNEL     ADC2_PF_CHANNEL_4
#define ADC_2_PF_CHANNEL_4_GPIO 		SOM_PIN_53

//#define ADC2_PF_GPIO12_CHANNEL     ADC2_PF_CHANNEL_5
#define ADC_2_PF_CHANNEL_5_GPIO 		SOM_PIN_92

//#define ADC2_PF_GPIO14_CHANNEL     ADC2_PF_CHANNEL_6
#define ADC_2_PF_CHANNEL_6_GPIO 		SOM_PIN_38


/* ESP32 modules does not exposes GPIO 27 hence ESP32 SOM does not support this channel */
//#define ADC2_PF_GPIO27_CHANNEL     ADC2_PF_CHANNEL_7
//#define ADC_2_PF_CHANNEL_7_GPIO

/* ESP32 modules does not exposes GPIO 25 hence ESP32 SOM does not support this channel */
//#define ADC2_PF_GPIO25_CHANNEL     ADC2_PF_CHANNEL_8
//#define ADC_2_PF_CHANNEL_8_GPIO

/* ESP32 modules does not exposes GPIO 26 hence ESP32 SOM does not support this channel */
//#define ADC2_PF_GPIO26_CHANNEL     ADC2_PF_CHANNEL_9
//#define ADC_2_PF_CHANNEL_9_GPIO


#endif /* ADC_PLATFORM_CHANNEL_H_ */
