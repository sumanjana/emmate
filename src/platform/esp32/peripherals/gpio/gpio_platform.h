/*
 * File Name: gpio_platform.h
 * File Path: /emmate/src/platform/esp/gpio/gpio_platform.h
 * Description:
 *
 *  Created on: 10-May-2019
 *      Author: Noyel Seth
 */

#ifndef GPIO_PLATFORM_H_
#define GPIO_PLATFORM_H_

#include "../peripheral-common/pf_peripheral_common.h"
#include "driver/gpio.h"

/* Wrapper typedefs for abstracting the platform specific names */

//Keep the LEVELx values as they are here; they match up with (1<<level)
#define GPIO_PF_IO_INTR_FLAG_LEVEL1 		PF_INTR_FLAG_LEVEL1	///< Accept a Level 1 interrupt vector (lowest priority)
#define GPIO_PF_IO_INTR_FLAG_LEVEL2 		PF_INTR_FLAG_LEVEL2	///< Accept a Level 2 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL3 		PF_INTR_FLAG_LEVEL3	///< Accept a Level 3 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL4 		PF_INTR_FLAG_LEVEL4	///< Accept a Level 4 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL5 		PF_INTR_FLAG_LEVEL5	///< Accept a Level 5 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL6 		PF_INTR_FLAG_LEVEL6	///< Accept a Level 6 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_NMI 			PF_INTR_FLAG_NMI			///< Accept a Level 7 interrupt vector (highest priority)
#define GPIO_PF_IO_INTR_FLAG_SHARED 		PF_INTR_FLAG_SHARED	///< Interrupt can be shared between ISRs
#define GPIO_PF_IO_INTR_FLAG_EDGE 			PF_INTR_FLAG_EDGE		///< Edge-triggered interrupt
#define GPIO_PF_IO_INTR_FLAG_IRAM 			PF_INTR_FLAG_IRAM			///< ISR can be called if cache is disabled
#define GPIO_PF_IO_INTR_FLAG_INTRDISABLED 	PF_INTR_FLAG_INTRDISABLED		///< Return with this interrupt disabled

#define GPIO_PF_IO_INTR_FLAG_LOWMED 		PF_INTR_FLAG_LOWMED ///< Low and medium prio interrupts. These can be handled in C.
#define GPIO_PF_IO_INTR_FLAG_HIGH 			PF_INTR_FLAG_HIGH ///< High level interrupts. Need to be handled in assembly.

#define GPIO_PF_IO_INTR_FLAG_LEVELMASK 		PF_INTR_FLAG_LEVELMASK	///< Mask for all level flags

#define	GPIO_PF_IO_INTERRUPT		IRAM_ATTR			// interrupt handler's declared type

#define GPIO_PLATFORM_IS_VALID_IO(gpio_num) 			GPIO_IS_VALID_GPIO(gpio_num) /*!< Check whether it is a valid GPIO number */
#define GPIO_PLATFORM_IS_VALID_OUTPUT_IO(gpio_num)   	GPIO_IS_VALID_OUTPUT_GPIO(gpio_num)    /*!< Check whether it can be a valid GPIO number of output mode */

typedef enum {
	GPIO_PF_IO_NUM_NC = GPIO_NUM_NC, /*!< Use to signal not connected to S/W */
	GPIO_PF_IO_NUM_0 = GPIO_NUM_0, /*!< GPIO0, input and output */
	GPIO_PF_IO_NUM_1 = GPIO_NUM_1, /*!< GPIO1, input and output */
	GPIO_PF_IO_NUM_2 = GPIO_NUM_2, /*!< GPIO2, input and output
	 @note There are more enumerations like that
	 up to GPIO39, excluding GPIO20, GPIO24 and GPIO28..31.
	 They are not shown here to reduce redundant information.
	 @note GPIO34..39 are input mode only. */
	/** @cond */
	GPIO_PF_IO_NUM_3 = GPIO_NUM_3, /*!< GPIO3, input and output */
	GPIO_PF_IO_NUM_4 = GPIO_NUM_4, /*!< GPIO4, input and output */
	GPIO_PF_IO_NUM_5 = GPIO_NUM_5, /*!< GPIO5, input and output */
	GPIO_PF_IO_NUM_6 = GPIO_NUM_6, /*!< GPIO6, input and output */
	GPIO_PF_IO_NUM_7 = GPIO_NUM_7, /*!< GPIO7, input and output */
	GPIO_PF_IO_NUM_8 = GPIO_NUM_8, /*!< GPIO8, input and output */
	GPIO_PF_IO_NUM_9 = GPIO_NUM_9, /*!< GPIO9, input and output */
	GPIO_PF_IO_NUM_10 = GPIO_NUM_10, /*!< GPIO10, input and output */
	GPIO_PF_IO_NUM_11 = GPIO_NUM_11, /*!< GPIO11, input and output */
	GPIO_PF_IO_NUM_12 = GPIO_NUM_12, /*!< GPIO12, input and output */
	GPIO_PF_IO_NUM_13 = GPIO_NUM_13, /*!< GPIO13, input and output */
	GPIO_PF_IO_NUM_14 = GPIO_NUM_14, /*!< GPIO14, input and output */
	GPIO_PF_IO_NUM_15 = GPIO_NUM_15, /*!< GPIO15, input and output */
	GPIO_PF_IO_NUM_16 = GPIO_NUM_16, /*!< GPIO16, input and output */
	GPIO_PF_IO_NUM_17 = GPIO_NUM_17, /*!< GPIO17, input and output */
	GPIO_PF_IO_NUM_18 = GPIO_NUM_18, /*!< GPIO18, input and output */
	GPIO_PF_IO_NUM_19 = GPIO_NUM_19, /*!< GPIO19, input and output */

	GPIO_PF_IO_NUM_21 = GPIO_NUM_21, /*!< GPIO21, input and output */
	GPIO_PF_IO_NUM_22 = GPIO_NUM_22, /*!< GPIO22, input and output */
	GPIO_PF_IO_NUM_23 = GPIO_NUM_23, /*!< GPIO23, input and output */

	GPIO_PF_IO_NUM_25 = GPIO_NUM_25, /*!< GPIO25, input and output */
	GPIO_PF_IO_NUM_26 = GPIO_NUM_26, /*!< GPIO26, input and output */
	GPIO_PF_IO_NUM_27 = GPIO_NUM_27, /*!< GPIO27, input and output */

	GPIO_PF_IO_NUM_32 = GPIO_NUM_32, /*!< GPIO32, input and output */
	GPIO_PF_IO_NUM_33 = GPIO_NUM_33, /*!< GPIO33, input and output */
	GPIO_PF_IO_NUM_34 = GPIO_NUM_34, /*!< GPIO34, input mode only */
	GPIO_PF_IO_NUM_35 = GPIO_NUM_35, /*!< GPIO35, input mode only */
	GPIO_PF_IO_NUM_36 = GPIO_NUM_36, /*!< GPIO36, input mode only */
	GPIO_PF_IO_NUM_37 = GPIO_NUM_37, /*!< GPIO37, input mode only */
	GPIO_PF_IO_NUM_38 = GPIO_NUM_38, /*!< GPIO38, input mode only */
	GPIO_PF_IO_NUM_39 = GPIO_NUM_39, /*!< GPIO39, input mode only */
	GPIO_PF_IO_NUM_MAX = GPIO_NUM_MAX,
/** @endcond */
} gpio_pf_io_num;

typedef enum {
	GPIO_PF_INTERRUPT_DISABLE = GPIO_INTR_DISABLE, /*!< Disable GPIO interrupt                             */
	GPIO_PF_INTERRUPT_POSEDGE = GPIO_INTR_POSEDGE, /*!< GPIO interrupt type : rising edge                  */
	GPIO_PF_INTERRUPT_NEGEDGE = GPIO_INTR_NEGEDGE, /*!< GPIO interrupt type : falling edge                 */
	GPIO_PF_INTERRUPT_ANYEDGE = GPIO_INTR_ANYEDGE, /*!< GPIO interrupt type : both rising and falling edge */
	GPIO_PF_INTERRUPT_LOW_LEVEL = GPIO_INTR_LOW_LEVEL, /*!< GPIO interrupt type : input low level trigger      */
	GPIO_PF_INTERRUPT_HIGH_LEVEL = GPIO_INTR_HIGH_LEVEL, /*!< GPIO interrupt type : input high level trigger     */
	GPIO_PF_INTERRUPT_MAX = GPIO_INTR_MAX,
} gpio_pf_interrupt_type;

typedef enum {
	GPIO_PF_IO_MODE_DISABLE = GPIO_MODE_DISABLE, /*!< GPIO mode : disable input and output             */
	GPIO_PF_IO_MODE_INPUT = GPIO_MODE_INPUT, /*!< GPIO mode : input only                           */
	GPIO_PF_IO_MODE_OUTPUT = GPIO_MODE_OUTPUT, /*!< GPIO mode : output only mode                     */
	GPIO_PF_IO_MODE_OUTPUT_OD = GPIO_MODE_OUTPUT_OD, /*!< GPIO mode : output only with open-drain mode     */
	GPIO_PF_IO_MODE_INPUT_OUTPUT_OD = GPIO_MODE_INPUT_OUTPUT_OD, /*!< GPIO mode : output and input with open-drain mode*/
	GPIO_PF_IO_MODE_INPUT_OUTPUT = GPIO_MODE_INPUT_OUTPUT, /*!< GPIO mode : output and input mode                */
} gpio_pf_io_mode;

typedef enum {
	GPIO_PF_IO_PULLUP_DISABLE = GPIO_PULLUP_DISABLE, /*!< Disable GPIO pull-up resistor */
	GPIO_PF_IO_PULLUP_ENABLE = GPIO_PULLUP_ENABLE, /*!< Enable GPIO pull-up resistor */
} gpio_pf_io_pullup;

typedef enum {
	GPIO_PF_IO_PULLDOWN_DISABLE = GPIO_PULLDOWN_DISABLE, /*!< Disable GPIO pull-down resistor */
	GPIO_PF_IO_PULLDOWN_ENABLE = GPIO_PULLDOWN_ENABLE, /*!< Enable GPIO pull-down resistor  */
} gpio_pf_io_pulldown;

typedef enum {
	GPIO_PF_IO_PULLUP_ONLY = GPIO_PULLUP_ONLY, /*!< Pad pull up            */
	GPIO_PF_IO_PULLDOWN_ONLY = GPIO_PULLDOWN_ONLY, /*!< Pad pull down          */
	GPIO_PF_IO_PULLUP_PULLDOWN = GPIO_PULLUP_PULLDOWN, /*!< Pad pull up + pull down*/
	GPIO_PF_IO_FLOATING = GPIO_FLOATING, /*!< Pad floating           */
} gpio_pf_io_pull_mode;

typedef enum {
	GPIO_PF_IO_DRIVE_CAP_0 = GPIO_DRIVE_CAP_0, /*!< Pad drive capability: weak          */
	GPIO_PF_IO_DRIVE_CAP_1 = GPIO_DRIVE_CAP_1, /*!< Pad drive capability: stronger      */
	GPIO_PF_IO_DRIVE_CAP_2 = GPIO_DRIVE_CAP_2, /*!< Pad drive capability: default value */
	GPIO_PF_IO_DRIVE_CAP_DEFAULT = GPIO_DRIVE_CAP_DEFAULT, /*!< Pad drive capability: default value */
	GPIO_PF_IO_DRIVE_CAP_3 = GPIO_DRIVE_CAP_3, /*!< Pad drive capability: strongest     */
	GPIO_PF_IO_DRIVE_CAP_MAX = GPIO_DRIVE_CAP_MAX,
} gpio_pf_io_drive_cap;

/* Wrapper typedefs for abstracting the platform specific names */
/**
 * @brief Configuration parameters of GPIO pad for gpio_config function
 */
typedef gpio_config_t gpio_pf_io_config;
typedef gpio_isr_t gpio_pf_io_isr;
typedef gpio_isr_handle_t gpio_pf_io_isr_handle;

/* Wrapper typedefs for abstracting the platform specific names */
/**
 * @brief GPIO common configuration
 *
 *        Configure GPIO's Mode,pull-up,PullDown,IntrType
 *
 * @param  pGPIOConfig Pointer to GPIO configure struct
 *
 * @return
 *     - ESP_OK success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *
 */
#define gpio_pf_io_config(pGPIOConfig)	gpio_config(/*const gpio_config_t* */ pGPIOConfig)

/**
 * @brief Reset an gpio to default state (select gpio function, enable pullup and disable input and output).
 *
 * @param gpio_num GPIO number.
 *
 * @note This function also configures the IOMUX for this pin to the GPIO
 *       function, and disconnects any other peripheral output configured via GPIO
 *       Matrix.
 *
 * @return Always return ESP_OK.
 */
#define gpio_pf_io_reset_pin(gpio_num) 					gpio_reset_pin(/*gpio_num_t*/ gpio_num)

/**
 * @brief  GPIO set interrupt trigger type
 *
 * @param  gpio_num GPIO number. If you want to set the trigger type of e.g. of GPIO16, gpio_num should be GPIO_NUM_16 (16);
 * @param  intr_type Interrupt type, select from gpio_int_type_t
 *
 * @return
 *     - ESP_OK  Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *
 */
#define gpio_pf_io_set_intr_type(gpio_num, intr_type) 	gpio_set_intr_type(/*gpio_num_t*/ gpio_num, /*gpio_int_type_t*/ intr_type)

/**
 * @brief  Enable GPIO module interrupt signal
 *
 * @note Please do not use the interrupt of GPIO36 and GPIO39 when using ADC.
 *       Please refer to the comments of `adc1_get_raw`.
 *       Please refer to section 3.11 of 'ECO_and_Workarounds_for_Bugs_in_ESP32' for the description of this issue.
 *
 * @param  gpio_num GPIO number. If you want to enable an interrupt on e.g. GPIO16, gpio_num should be GPIO_NUM_16 (16);
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *
 */
#define gpio_pf_io_intr_enable(gpio_num) 					gpio_intr_enable(/*gpio_num_t*/ gpio_num)

/**
 * @brief  Disable GPIO module interrupt signal
 *
 * @param  gpio_num GPIO number. If you want to disable the interrupt of e.g. GPIO16, gpio_num should be GPIO_NUM_16 (16);
 *
 * @return
 *     - ESP_OK success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *
 */
#define gpio_pf_io_intr_disable(gpio_num)					gpio_intr_disable(/*gpio_num_t*/ gpio_num)

/**
 * @brief  GPIO set output level
 *
 * @param  gpio_num GPIO number. If you want to set the output level of e.g. GPIO16, gpio_num should be GPIO_NUM_16 (16);
 * @param  level Output level. 0: low ; 1: high
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG GPIO number error
 *
 */
#define gpio_pf_io_set_level(gpio_num, level)			 	gpio_set_level(/*gpio_num_t*/ gpio_num, /*uint32_t*/ level)

/**
 * @brief  GPIO get input level
 *
 * @warning If the pad is not configured for input (or input and output) the returned value is always 0.
 *
 * @param  gpio_num GPIO number. If you want to get the logic level of e.g. pin GPIO16, gpio_num should be GPIO_NUM_16 (16);
 *
 * @return
 *     - 0 the GPIO input level is 0
 *     - 1 the GPIO input level is 1
 *
 */
#define gpio_pf_io_get_level(gpio_num)					gpio_get_level(/*gpio_num_t*/ gpio_num)

/**
 * @brief	 GPIO set direction
 *
 * Configure GPIO direction,such as output_only,input_only,output_and_input
 *
 * @param  gpio_num  Configure GPIO pins number, it should be GPIO number. If you want to set direction of e.g. GPIO16, gpio_num should be GPIO_NUM_16 (16);
 * @param  mode GPIO direction
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG GPIO error
 *
 */
#define gpio_pf_io_set_direction(gpio_num, mode)	 		gpio_set_direction(/*gpio_num_t*/ gpio_num, /*gpio_mode_t*/ mode)

/**
 * @brief  Configure GPIO pull-up/pull-down resistors
 *
 * Only pins that support both input & output have integrated pull-up and pull-down resistors. Input-only GPIOs 34-39 do not.
 *
 * @param  gpio_num GPIO number. If you want to set pull up or down mode for e.g. GPIO16, gpio_num should be GPIO_NUM_16 (16);
 * @param  pull GPIO pull up/down mode.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG : Parameter error
 *
 */
#define gpio_pf_io_set_pull_mode(gpio_num, pull)	 		gpio_set_pull_mode(/*gpio_num_t*/ gpio_num, /*gpio_pull_mode_t*/ pull)

/**
 * @brief Enable GPIO wake-up function.
 *
 * @param gpio_num GPIO number.
 *
 * @param intr_type GPIO wake-up type. Only GPIO_INTR_LOW_LEVEL or GPIO_INTR_HIGH_LEVEL can be used.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_wakeup_enable(gpio_num, intr_type)	 	gpio_wakeup_enable(/*gpio_num_t*/ gpio_num, /*gpio_int_type_t*/ intr_type)

/**
 * @brief Disable GPIO wake-up function.
 *
 * @param gpio_num GPIO number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_wakeup_disable(gpio_num)	 			gpio_wakeup_disable(/*gpio_num_t*/ gpio_num)

/**
 * @brief   Register GPIO interrupt handler, the handler is an ISR.
 *          The handler will be attached to the same CPU core that this function is running on.
 *
 * This ISR function is called whenever any GPIO interrupt occurs. See
 * the alternative gpio_install_isr_service() and
 * gpio_isr_handler_add() API in order to have the driver support
 * per-GPIO ISRs.
 *
 * @param  fn  Interrupt handler function.
 * @param  intr_alloc_flags Flags used to allocate the interrupt. One or multiple (ORred)
 *            ESP_INTR_FLAG_* values. See esp_intr_alloc.h for more info.
 * @param  arg  Parameter for handler function
 * @param  handle Pointer to return handle. If non-NULL, a handle for the interrupt will be returned here.
 *
 * \verbatim embed:rst:leading-asterisk
 * To disable or remove the ISR, pass the returned handle to the :doc:`interrupt allocation functions </api-reference/system/intr_alloc>`.
 * \endverbatim
 *
 * @return
 *     - ESP_OK Success ;
 *     - ESP_ERR_INVALID_ARG GPIO error
 *     - ESP_ERR_NOT_FOUND No free interrupt found with the specified flags
 */
#define gpio_pf_io_isr_register(fn, arg, intr_alloc_flags, handle) gpio_isr_register(/*void (*fn)(void*)*/ fn, /*void * */ arg, /*int*/ intr_alloc_flags, /*gpio_isr_handle_t* */handle)

/**
 * @brief Enable pull-up on GPIO.
 *
 * @param gpio_num GPIO number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_pullup_en(gpio_num) gpio_pullup_en(/*gpio_num_t*/ gpio_num)

/**
 * @brief Disable pull-up on GPIO.
 *
 * @param gpio_num GPIO number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_pullup_dis(gpio_num) gpio_pullup_dis(/*gpio_num_t*/ gpio_num)

/**
 * @brief Enable pull-down on GPIO.
 *
 * @param gpio_num GPIO number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_pulldown_en(gpio_num) gpio_pulldown_en(/*gpio_num_t*/ gpio_num)

/**
 * @brief Disable pull-down on GPIO.
 *
 * @param gpio_num GPIO number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_pulldown_dis(gpio_num) gpio_pulldown_dis(/*gpio_num_t*/ gpio_num)

/**
 * @brief Install the driver's GPIO ISR handler service, which allows per-pin GPIO interrupt handlers.
 *
 * This function is incompatible with gpio_isr_register() - if that function is used, a single global ISR is registered for all GPIO interrupts. If this function is used, the ISR service provides a global GPIO ISR and individual pin handlers are registered via the gpio_isr_handler_add() function.
 *
 * @param intr_alloc_flags Flags used to allocate the interrupt. One or multiple (ORred)
 *            ESP_INTR_FLAG_* values. See esp_intr_alloc.h for more info.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_NO_MEM No memory to install this service
 *     - ESP_ERR_INVALID_STATE ISR service already installed.
 *     - ESP_ERR_NOT_FOUND No free interrupt found with the specified flags
 *     - ESP_ERR_INVALID_ARG GPIO error
 */
#define gpio_pf_io_install_isr_service(intr_alloc_flags) gpio_install_isr_service(/*int*/ intr_alloc_flags)

/**
 * @brief Uninstall the driver's GPIO ISR service, freeing related resources.
 */
#define gpio_pf_io_uninstall_isr_service()	gpio_uninstall_isr_service()

/**
 * @brief Add ISR handler for the corresponding GPIO pin.
 *
 * Call this function after using gpio_install_isr_service() to
 * install the driver's GPIO ISR handler service.
 *
 * The pin ISR handlers no longer need to be declared with IRAM_ATTR,
 * unless you pass the ESP_INTR_FLAG_IRAM flag when allocating the
 * ISR in gpio_install_isr_service().
 *
 * This ISR handler will be called from an ISR. So there is a stack
 * size limit (configurable as "ISR stack size" in menuconfig). This
 * limit is smaller compared to a global GPIO interrupt handler due
 * to the additional level of indirection.
 *
 * @param gpio_num GPIO number
 * @param isr_handler ISR handler function for the corresponding GPIO number.
 * @param args parameter for ISR handler.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Wrong state, the ISR service has not been initialized.
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_isr_handler_add(gpio_num, isr_handler, args) gpio_isr_handler_add(/*gpio_num_t*/ gpio_num, /*gpio_isr_t*/ isr_handler, /*void* */args)

/**
 * @brief Remove ISR handler for the corresponding GPIO pin.
 *
 * @param gpio_num GPIO number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Wrong state, the ISR service has not been initialized.
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_isr_handler_remove(gpio_num) gpio_isr_handler_remove(/*gpio_num_t*/ gpio_num)

/**
 * @brief Set GPIO pad drive capability
 *
 * @param gpio_num GPIO number, only support output GPIOs
 * @param strength Drive capability of the pad
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_set_drive_capability( gpio_num,  strength) gpio_set_drive_capability(/*gpio_num_t*/ gpio_num, /*gpio_drive_cap_t*/ strength)

/**
 * @brief Get GPIO pad drive capability
 *
 * @param gpio_num GPIO number, only support output GPIOs
 * @param strength Pointer to accept drive capability of the pad
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define gpio_pf_io_get_drive_capability(gpio_num, strength) gpio_get_drive_capability(/*gpio_num_t*/ gpio_num, /*gpio_drive_cap_t* */ strength)

/**
 * @brief Enable gpio pad hold function.
 *
 * The gpio pad hold function works in both input and output modes, but must be output-capable gpios.
 * If pad hold enabled:
 *   in output mode: the output level of the pad will be force locked and can not be changed.
 *   in input mode: the input value read will not change, regardless the changes of input signal.
 *
 * The state of digital gpio cannot be held during Deep-sleep, and it will resume the hold function
 * when the chip wakes up from Deep-sleep. If the digital gpio also needs to be held during Deep-sleep,
 * `gpio_deep_sleep_hold_en` should also be called.
 *
 * Power down or call gpio_hold_dis will disable this function.
 *
 * @param gpio_num GPIO number, only support output-capable GPIOs
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_NOT_SUPPORTED Not support pad hold function
 */
#define gpio_pf_io_hold_en( gpio_num) gpio_hold_en(/*gpio_num_t*/ gpio_num)

/**
 * @brief Disable gpio pad hold function.
 *
 * When the chip is woken up from Deep-sleep, the gpio will be set to the default mode, so, the gpio will output
 * the default level if this function is called. If you dont't want the level changes, the gpio should be configured to
 * a known state before this function is called.
 *  e.g.
 *     If you hold gpio18 high during Deep-sleep, after the chip is woken up and `gpio_hold_dis` is called,
 *     gpio18 will output low level(because gpio18 is input mode by default). If you don't want this behavior,
 *     you should configure gpio18 as output mode and set it to hight level before calling `gpio_hold_dis`.
 *
 * @param gpio_num GPIO number, only support output-capable GPIOs
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_NOT_SUPPORTED Not support pad hold function
 */
#define gpio_pf_io_hold_dis( gpio_num) gpio_hold_dis(/*gpio_num_t*/ gpio_num)

/**
 * @brief Enable all digital gpio pad hold function during Deep-sleep.
 *
 * When the chip is in Deep-sleep mode, all digital gpio will hold the state before sleep, and when the chip is woken up,
 * the status of digital gpio will not be held. Note that the pad hold feature only works when the chip is in Deep-sleep mode,
 * when not in sleep mode, the digital gpio state can be changed even you have called this function.
 *
 * Power down or call gpio_hold_dis will disable this function, otherwise, the digital gpio hold feature works as long as the chip enter Deep-sleep.
 */
#define gpio_pf_io_deep_sleep_hold_en() gpio_deep_sleep_hold_en()

/**
 * @brief Disable all digital gpio pad hold function during Deep-sleep.
 *
 */
#define gpio_pf_io_deep_sleep_hold_dis() gpio_deep_sleep_hold_dis()

/**
 * @brief Set pad input to a peripheral signal through the IOMUX.
 * @param gpio_num GPIO number of the pad.
 * @param signal_idx Peripheral signal id to input. One of the ``*_IN_IDX`` signals in ``soc/gpio_sig_map.h``.
 */
#define gpio_pf_io_iomux_in( gpio_num,  signal_idx) gpio_iomux_in(/*uint32_t*/ gpio_num, /*uint32_t*/ signal_idx)

/**
 * @brief Set peripheral output to an GPIO pad through the IOMUX.
 * @param gpio_num gpio_num GPIO number of the pad.
 * @param func The function number of the peripheral pin to output pin.
 *        One of the ``FUNC_X_*`` of specified pin (X) in ``soc/io_mux_reg.h``.
 * @param oen_inv True if the output enable needs to be inversed, otherwise False.
 */
#define gpio_pf_io_iomux_out( gpio_num,  func,  oen_inv) gpio_iomux_out(/*uint8_t*/ gpio_num, /*int*/ func,/*bool*/ oen_inv)



/**
 * @brief Select pad as a GPIO function from IOMUX.
 *
 * @param gpio_num GPIO number, 0~0x27
 *
 */
#define gpio_pf_io_pad_select_gpio(gpio_num)			gpio_pad_select_gpio(/*uint8_t*/ gpio_num)


#endif /* GPIO_PLATFORM_H_ */
