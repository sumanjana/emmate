/*
 * File Name: udp_log_streamer_platform.h
 * Description:
 *
 *  Created on: 03-May-2019
 *      Author: Rohan Dey
 */

#ifndef UDP_LOG_STREAMER_PLATFORM_H
#define UDP_LOG_STREAMER_PLATFORM_H

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#ifdef __cplusplus
}
#endif

#endif /* UDP_LOG_STREAMER_PLATFORM_H */
