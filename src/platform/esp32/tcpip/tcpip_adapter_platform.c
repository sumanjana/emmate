/*
 * File Name: tcpip_adapter_platform.c
 * File Path: /emmate/src/platform/esp/tcpip/tcpip_adapter_platform.c
 * Description:
 *
 *  Created on: 06-May-2019
 *      Author: Rohan Dey
 */

#include "tcpip_adapter_platform.h"

core_err get_tcpip_pf_ip_info(TCPIP_ADAPTER_IF_PF tcpip_if, TcpipAdapterIpInfo_Pf *ip_info) {
	core_err ret = tcpip_adapter_get_ip_info(tcpip_if, ip_info);
	if (ret != CORE_OK) {
		CORE_LOGE(LTAG_TCPIP, "get_tcpip_adapter_ip_info failed: err = %d", ret);
	}
	return ret;
}
