/*
 * File Name: heap_debug.h
 * File Path: /emmate/src/common/pic32/heap_debug.h
 * Description:
 *
 *  Created on: 19-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef HEAP_DEBUG_H_
#define HEAP_DEBUG_H_

#include "esp_heap_caps.h"

void check_heap_integrity_all();

#endif /* HEAP_DEBUG_H_ */
