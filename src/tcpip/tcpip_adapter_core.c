/*
 * File Name: tcpip_adapter_core.c
 * File Path: /emmate/src/tcpip/tcpip_adapter_core.c
 * Description:
 *
 *  Created on: 06-May-2019
 *      Author: Rohan Dey
 */

#include "tcpip_adapter_core.h"

core_err get_tcpip_ip_info(TCPIP_ADAPTER_IF_PF tcpip_if, TcpipAdapterIpInfo_Core *ip_info) {
	core_err ret = tcpip_adapter_get_ip_info(tcpip_if, ip_info);
	if (ret != CORE_OK) {
		CORE_LOGE(LTAG_TCPIP, "get_tcpip_ip_info failed: err = %d", ret);
		return CORE_FAIL;
	}
	return ret;
}
