/*
 * File Name: dht22.h
 * File Path: /emmate/src/external-ics/sensor/temperature/dht22/dht22.h
 * Description:
 *
 *  Created on: 20-Jun-2019
 *      Author: Noyel Seth
 */

#ifndef DHT22_H_
#define DHT22_H_

#include <stdbool.h>
#include "core_error.h"

#ifdef __cplusplus
extern "C" {
#endif


// == function prototypes =======================================

/**
 * @brief	Configure DHT22 Data-out GPIO
 *
 * @param[in]	dout_gpio	DHT22 Data-out GPIO
 *
 * @return
 * 		- CORE_OK on Success
 * 		- CORE_ERR_INVALID_ARG DHT22 Data-out GPIO Parameter error
 */
core_err dht22_set_dout_gpio(int dout_gpio);

/**
 * @brief	Print DHT22 read error log
 *
 * @param	response	return of DHT22 Data read process
 *
 */
void dht22_error_handler(core_err response);

/**
 * @brief	Execute DHT22 Data read process
 *
 * @return
 * 		- CORE_OK on Success
 * 		- CORE_ERR_TIMEOUT	Read Operation timed out
 * 		- CORE_ERR_INVALID_CRC	CRC or checksum was invalid
 * 		- CORE_ERR_INVALID_ARG DHT22 Data-out GPIO Parameter isn't configured
 */
core_err dht22_read();

/**
 * @brief	Get DHT22 Humidity value
 */
float dht22_get_humidity();

/**
 * @brief	Get DHT22 Temperature value
 */
float dht22_get_temperature();

/**
 *
 */
int dht22_get_signal_level( int usTimeOut, bool state );


#ifdef __cplusplus
}
#endif


#endif /* DHT22_H_ */
