/*
 * File Name: lm35.c
 * File Path: /emmate/src/external-ics/sensor/temperature/lm35/lm35.c
 * Description:
 *
 *  Created on: 19-Jun-2019
 *      Author: Noyel Seth
 */


#include "lm35.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

/**
 * The EmMate Framework has integrated external ICs support Library
 * Application developer can use this Library directly by using EmMate Framework.
 * For using this Library we need to defined 'CONFIG_USE_LM35' before include the lm35.h
 */
#ifdef CONFIG_USE_LM35

/**
 * The integrated external ICs support Library lm35 use ADC GPIO for complete the lm35 process
 * For lm35 library, ADC control temperature detect sensor
 * ADC GPIO need to define into Application's thing.h as:
 * #define THING_LM35_GPIO 	SOM_PIN_x
 */
#if THING_LM35_GPIO
#define LM35_GPIO		THING_LM35_GPIO
#else
#error "THING_LM35_GPIO is not defined. Please set this MACRO in thing.h"

#endif

#define TAG LTAG_LM35_LIB

/*
 *
 */
core_err lm35_init() {
	core_err ret = CORE_FAIL;

	/*
	 * Initialize the ADC with the configured data and start processing.
	 * ADC Channel: LM35_GPIO
	 * ADC read interval in seconds: 10secs = 10000millisecs
	 * ADC read data width in bit: ADC_CORE_WIDTH_BIT_10
	 * ADC voltage attenuation level: ADC_CORE_ATTEN_DB_11 (ADC read upto max Volt 3.3v)
	 */
#if LM35_GPIO
	ret = init_adc_peripheral(LM35_GPIO, DELAY_10_SEC, ADC_CORE_WIDTH_BIT_12, ADC_CORE_ATTEN_DB_11);
	if(ret == CORE_OK) {
		ret = lm35_stop_process();
	} else {
		CORE_LOGE(TAG, "Failed to initialize lm35");
	}
#endif
	return ret;
}

/*
 *
 */
core_err lm35_start_process() {
	core_err ret = CORE_FAIL;
#if LM35_GPIO
	ret = resume_adc_peripheral_read(LM35_GPIO);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to start lm35");
		return ret;
	}
#endif
	return ret;
}

/*
 *
 */
core_err lm35_stop_process() {
	core_err ret = CORE_FAIL;
#if LM35_GPIO
	ret = pause_adc_peripheral_read(LM35_GPIO);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to stop lm35");
		return ret;
	}
#endif
	return ret;
}

/*
 *
 */
core_err lm35_get_temperature(float *temperature) {
	core_err ret = CORE_FAIL;

#if LM35_GPIO
	// Declared integer variable to capture ADC raw data
	int raw_data = 0;

	// Declared uint32 variable to capture ADC mV data (mVolt convertion from raw data)
	uint32_t mV_data = 0;

	// Collect the ADC raw data & mV data
	ret = get_adc_peripheral_data(LM35_GPIO, &raw_data, &mV_data);
	if (ret == CORE_OK) {
		*temperature = mV_data / 10;   // convert analog output into temperature
		/*
		 * if last read read mV different from the previous read mV, the this line will print. otherwise continue
		 */
		CORE_LOGD(TAG, "Ambient temperature :%f C\n", *temperature);
	} else {
		CORE_LOGE(TAG, "Failed to read from lm35");
		return ret;
	}
#endif
	return ret;
}

/*
 *
 */
core_err lm35_deinit() {
	core_err ret = CORE_FAIL;

#if LM35_GPIO
	ret = deinit_adc_peripheral(LM35_GPIO);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to de-initialize lm35");
	}
#endif
	return ret;
}

#endif /* CONFIG_USE_LM35 */
