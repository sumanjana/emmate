/*
 * File Name: lm35.h
 * File Path: /emmate/src/external-ics/sensor/temperature/lm35/lm35.h
 * Description:
 *
 *  Created on: 19-Jun-2019
 *      Author: Noyel Seth
 */

#ifndef LM35_H_
#define LM35_H_

#include "external_ics.h"

//#ifdef CONFIG_USE_LM35

//#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "thing.h"
#include "adc_core_channel.h"
#include "adc_core.h"

/**
 * @brief	Initialize the LM35 sensor process
 *
 * @return
 *		- CORE_OK				if success
 *		- CORE_FAIL				if failed to start process
 *		- CORE_ERR_NO_MEM		if failed to allocate memory for ADC configuration
 *		- CORE_ERR_INVALID_ARG	if THING_LM35_GPIO not valid
 */
core_err lm35_init();


/**
 * @brief	Start the LM35 sensor read processing
 *
 * @return
 *		- CORE_OK				if success
 *		- CORE_ERR_INVALID_ARG	if THING_LM35_GPIO not valid
 */
core_err lm35_start_process() ;


/**
 * @brief	Stop the LM35 sensor read processing
 *
 *
 * @return
 *		- CORE_OK				if success
 *		- CORE_ERR_INVALID_ARG	if THING_LM35_GPIO not valid
 */
core_err lm35_stop_process();


/**
 * @brief	Get the LM35 Temperature data
 *
 * @param[out]	temperature	get the temperature reading data.
 *
 * @return
 *		- CORE_OK				if success
 *		- CORE_ERR_INVALID_ARG	if THING_LM35_GPIO not valid
 */
core_err lm35_get_temperature(float *temperature);


/**
 * @brief	De-Initialize the LM35 sensor lib process
 *
 * @return
 *		- CORE_OK				if success
 *		- CORE_ERR_INVALID_ARG	if THING_LM35_GPIO not valid
 */
core_err lm35_deinit();

//#endif /* CONFIG_USE_LM35 */

#endif /* LM35_H_ */
