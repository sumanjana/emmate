/*
 * File Name: ds1307.h
 * File Path: /emmate/src/external-ics/rtc/ds1307/ds1307.h
 * Description:
 *
 *  Created on: 20-Jun-2019
 *      Author: Noyel Seth
 */

#ifndef DS1307_H_
#define DS1307_H_

#include "external_ics.h"

/**
 * The EmMate Framework has integrated external ICs support Library
 * Application developer can use this Library directly by using EmMate Framework.
 * For using this Library we need to defined 'CONFIG_USE_DS1307' before include the ds1307.h
 */
//#ifdef CONFIG_USE_DS1307

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "thing.h"
#include "i2c_core.h"


/**
 * @brief	Initialize the ds1307 process
 *
 * @return
 *     - CORE_OK 						Success
 *     - CORE_FAIL 						Driver install error
 */
core_err ds1307_init();

/**
 * @brief de-initialize the ds1307 process
 *
 * @return
 *     - CORE_OK 						Success
 *     - CORE_FAIL 						Driver delete error
 */
core_err ds1307_deinit();

/******************************** Write & Read SECOND ********************************/
/**
 * @brief write second to ds1307 peripheral
 *
 * @param data_wr[in]				write seconds data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_sec(uint8_t* data_wr);

/**
 * @brief read seconds from ds1307 peripheral
 *
 * @param data_rd[out]				read seconds data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_sec(uint8_t* data_rd);


/******************************** Write & Read MINUTE ********************************/
/**
 * @brief write MINUTE to ds1307 peripheral
 *
 * @param data_wr[in]				write minute data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_min(uint8_t* data_wr);

/**
 * @brief read MINUTE from ds1307 peripheral
 *
 * @param data_rd[out]				read minute data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_min(uint8_t* data_rd);


/******************************** Write & Read HOUR ********************************/
/**
 * @brief write HOUR to ds1307 peripheral
 *
 * @param data_wr[in]				write hour data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_hour(uint8_t* data_wr);

/**
 * @brief read HOUR from ds1307 peripheral
 *
 * @param data_rd[out]				read hour data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_hour(uint8_t* data_rd);


/******************************** Write & Read DAY ********************************/
/**
 * @brief write DAY to ds1307 peripheral
 *
 * @param data_wr[in]				write day data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_day(uint8_t* data_wr);

/**
 * @brief read DAY from ds1307 peripheral
 *
 * @param data_rd[out]				read day data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_day(uint8_t* data_rd);


/******************************** Write & Read DATE ********************************/
/**
 * @brief write DATE to ds1307 peripheral
 *
 * @param data_wr[in]				write date data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_date(uint8_t* data_wr);

/**
 * @brief read DATE from ds1307 peripheral
 *
 * @param data_rd[out]				read date data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_date(uint8_t* data_rd);



/******************************** Write & Read MONTH ********************************/
/**
 * @brief write MONTH to ds1307 peripheral
 *
 * @param data_wr[in]				write month data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_month(uint8_t* data_wr);


/**
 * @brief read MONTH from ds1307 peripheral
 *
 * @param data_rd[out]				read month data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_month(uint8_t* data_rd);



/******************************** Write & Read YEAR ********************************/
/**
 * @brief write YEAR to ds1307 peripheral
 *
 * @param data_wr[in]				write year data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_year(uint8_t* data_wr);

/**
 * @brief read YEAR from ds1307 peripheral
 *
 * @param data_rd[out]				read year data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_year(uint8_t* data_rd);



/******************************** Write & Read SQWE ********************************/
/**
 * @brief write SQWE to ds1307 peripheral
 *
 * @param data_wr[in]				write SQWE data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_sqwe(uint8_t* data_wr);

/**
 * @brief read SQWE from ds1307 peripheral
 *
 * @param data_rd[out]				read SQWE data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_sqwe(uint8_t* data_rd);



/******************************** Write & Read Time ********************************/
/**
 * @brief write Time to ds1307 peripheral
 *
 * @param data_sec[in]				write Seconds data
 * @param data_min[in]				write Minute data
 * @param data_hr[in]				write Hour data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_time(uint8_t* data_sec, uint8_t* data_min, uint8_t* data_hr);


/**
 * @brief read TIME from ds1307 peripheral
 *
 * @param data_sec[out]				read Seconds data
 * @param data_min[out]				read Minute data
 * @param data_hr[out]				read Hour data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_time(uint8_t* data_sec, uint8_t* data_min, uint8_t* data_hr);


/******************************** Write & Read Date ********************************/
/**
 * @brief write DATE to ds1307 peripheral
 *
 * @param data_day[in]				write Day data
 * @param data_date[in]				write Date data
 * @param data_month[in]			write Month data
 * @param date_yr[in]				write Year data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_write_date_and_year(uint8_t* data_day, uint8_t* data_date, uint8_t* data_month, uint8_t* date_yr);


/**
 * @brief read DATE from ds1307 peripheral
 *
 * @param data_day[out]				read Day data
 * @param data_date[out]			read Date data
 * @param data_month[out]			read Month data
 * @param date_yr[out]				read Year data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			ds1307 not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		ds1307 driver not initialize
 */
core_err ds1307_read_date_and_year(uint8_t* data_day, uint8_t* data_date, uint8_t* data_month, uint8_t* date_yr);

//#endif /* CONFIG_USE_DS1307 */

#endif /* DS1307_H_ */
