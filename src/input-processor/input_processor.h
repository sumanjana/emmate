/*
 * File Name: input_processor.h
 * File Path: /emmate/src/input-processor/input_processor.h
 * Description:
 *
 *  Created on: 09-May-2019
 *      Author: Rohan Dey
 */

#ifndef INPUT_PROCESSOR_H_
#define INPUT_PROCESSOR_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#if CONFIG_USE_SYSTIME
#include "systime.h"
#endif
#if CONFIG_USE_PARSON
#include "parson.h"
#endif

/*
 * @brief	This is a helper function to parse the following json keys
 * 			1. stat
 * 			2. error object
 * 				- err_code
 * 				- err_msg
 *
 * @param[in]	json_buff	Input JSON string
 * @param[out]	stat		Parsed value
 * @param[out]	error		Parsed value
 *
 * @return
 * 		- CORE_OK	if success
 * 		- CORE_FAIL	if failed
 *
 * */
core_err inproc_parse_json_common_info(char *json_buff, int *stat, CoreError *error);
#endif /* INPUT_PROCESSOR_H_ */
