/*
 * File Name: inpproc_utils.c
 * File Path: /emmate/src/input-processor/inpproc_utils.c
 * Description:
 *
 *  Created on: 30-Apr-2019
 *      Author: Noyel Seth
 */

#include "inpproc_utils.h"

#include "core_logger.h"

#if CONFIG_USE_LOGGING
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>

#define TAG LTAG_PARSER_UTILS

core_err cpy_json_str_obj(JSON_Object * rootObj, char* key, char* dest) {
	core_err ret = CORE_FAIL;
#if CONFIG_USE_PARSON
	//if (json_object_get_string(rootObj, key) != NULL) {
		const char* data = json_object_get_string(rootObj, key);
		//printf("data = %s\r\ndata_len=%d\r\n", data, strlen(data));
		if ( (data != NULL) && (dest != NULL)) {
			memcpy(dest, data, strlen(data));
			//printf("dest = %s\r\n", dest);
			ret = CORE_OK;
		}
	//}
#endif
	return ret;
}
