/*
 * File Name: inpproc_utils.c
 * File Path: /emmate/src/input-processor/input_processor.c
 * Description:
 *
 *  Created on: 09-May-2019
 *      Author: Rohan Dey
 */

#include "input_processor.h"
#include "inpproc_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "core_utils.h"
#include <string.h>

#define TAG LTAG_INPROC

//#define RESP_JSON "{\"stat\": true,\"error\": {\"err_code\": 10,\"err_msg\": null},\"fota\": {\"stat\": true,\"url_len\": 142,\"url\": \"https://dev.iquesters.com/emb_api/test_pis/upload/firmware.hex\",\"cert_len\": 0,\"cert\": null,\"ver\": \"0.0.0.1\",\"fname\": \"firmware.hex\",\"fsize\": 18410,\"sch\": \"1559735400\"}}"

core_err inproc_parse_json_common_info(char *json_buff, int *status, CoreError *error) {
	core_err ret = CORE_FAIL;

	JSON_Value* root_value = NULL;
	JSON_Object * rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "JSON Value type not matched");
			ret = CORE_FAIL;
			goto free_memory;
		} else {
			rootObj = json_value_get_object(root_value);

			/* Get the response status */
			*status = json_object_get_boolean(rootObj, GET_VAR_NAME(status, NULL));
			if (*status == -1) {
				CORE_LOGE(TAG, "Could not parse JSON key [ %s], not proceeding further!", GET_VAR_NAME(status, NULL));
				ret = CORE_FAIL;
				goto free_memory;
			}

			/* If only status is false, then we will have an error object */
			if (*status == false) {
				/* Get Error Code and Error Message */
				JSON_Value* error_value = json_object_get_value(rootObj, GET_VAR_NAME(error, NULL));
				if (error_value != NULL) {
					if (json_value_get_type(error_value) != JSONObject) {
						CORE_LOGE(TAG, "The key error_value is not a JSON Object!");
						error->err_code = -1;
						ret = CORE_FAIL;
						goto free_memory;
					} else {
						JSON_Object *err_obj = json_value_get_object(error_value);
						if (err_obj != NULL) {
							CORE_LOGD(TAG, "errObj is not NULL!");
							error->err_code = (int) json_object_get_number(err_obj,
									GET_VAR_NAME(error->err_code, "->"));
							if (error->err_code != CORE_OK) {
								const char *err_msg = json_object_get_string(err_obj,
										GET_VAR_NAME(error->err_msg, "->"));
								if (err_msg != NULL) {
									int len = strlen(err_msg);
									if (len <= CORE_ERROR_MSG_MAX_LEN)
										strcpy(error->err_msg, err_msg);
									else {
										strncpy(error->err_msg, err_msg, CORE_ERROR_MSG_MAX_LEN);
										strcat(error->err_msg, CORE_ERROR_MSG_TERMINATOR);
									}
								} else {
									CORE_LOGD(TAG, "err_msg is null");
								}
							}
						} else {
							CORE_LOGD(TAG, "errObj is null");
						}
					}
				} else {
					CORE_LOGE(TAG, "error_value is null");
					error->err_code = -1;
					ret = CORE_FAIL;
					goto free_memory;
				}
			} /* if (*status == false) */
		}
		ret = CORE_OK;

		free_memory:
		/* clear root_value */
		json_value_free(root_value);

	} else {
		CORE_LOGE(TAG, "Could not create JSON root object");
		ret = CORE_FAIL;
	}

	return ret;
}
