/*
 * File Name: button_helper.c
 * File Path: /emmate/src/hmi/button/button_helper.c
 * Description:
 *
 *  Created on: 10-May-2019
 *      Author: Noyel Seth
 */

#include <stdarg.h>
#include <string.h>
#include "button_helper.h"
#include "button_platform.h"
#include "threading.h"
#include "heap_debug.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "gll.h"
#include "gpio_helper_api.h"

#if CONFIG_USE_BUTTON

#define TAG		LTAG_BTN_HELPER

#define SHORT_PRESSED_BTN_INTERACTION 		0x01
#define LONG_PRESSED_BTN_INTERACTION		0x02
#define CUSTOM_PRESSED_BTN_INTERACTION		0x04
#define DOUBLE_PRESSED_BTN_INTERACTION		0x08
#define ALL_PRESSED_BTN_INTERACTION			0x0F

typedef struct {
	QueueHandle btn_queue;
	bool lib_init_stat;
} BtnHelperData;

static ButtonData* btn_dtlist = NULL;
static BtnHelperData btnhelper_dt = { .btn_queue = NULL, .lib_init_stat = false };

static void INTERRUPT_ATTRIBUTES btn_isr_handler(void* arg) {
	 uint32_t gpio_num = (uint32_t) arg;
	 //printf("GPIO_IO_INTERRUPT= %d\r\n", gpio_num);
	 QueueSendFromISR(btnhelper_dt.btn_queue, &gpio_num, NULL);
}


static uint8_t double_intercation_detection(uint32_t press_duration, uint8_t idx, uint32_t io_num){
	if (press_duration <= (DEFAULT_PRESS_DURATION / TICK_RATE_TO_MS)) {
		btn_dtlist[idx].bnt_hlp_dt.pressed_count++;
		if (btn_dtlist[idx].bnt_hlp_dt.pressed_count == 2) {
//			printf("press_tick %d, release_tick %d\r\n", btn_dtlist[idx].bnt_hlp_dt.press_tick,
//					btn_dtlist[idx].bnt_hlp_dt.release_tick);

			uint32_t interval_time = btn_dtlist[idx].bnt_hlp_dt.press_tick - btn_dtlist[idx].bnt_hlp_dt.release_tick;
			CORE_LOGD(TAG,"Double Press interval time %d\r\n", interval_time);
			if (interval_time <= (DEFAULT_DOUBLE_PRESS_INTERVAL_DURATION / TICK_RATE_TO_MS)) {
				CORE_LOGD(TAG, "Button's Double Press Detection completed. Calling the handler ...");
				btn_dtlist[idx].notify_btn_stateCb(io_num, DOUBLE_PRESSED);
				btn_dtlist[idx].bnt_hlp_dt.release_tick = 0;
				btn_dtlist[idx].bnt_hlp_dt.pressed_count = 0;
				return 0;
			} else {
				btn_dtlist[idx].bnt_hlp_dt.release_tick = TaskGetTickCount();
				btn_dtlist[idx].bnt_hlp_dt.pressed_count = 1;
			}
		} else {
			btn_dtlist[idx].bnt_hlp_dt.release_tick = TaskGetTickCount();
		}
	}
	return 1;
}


static void long_press_detection_task(void* pvParameters) {
	uint32_t io_num = (uint32_t) pvParameters;
	uint32_t idx = pin_to_btn_idx_bsp_board(io_num);
	uint32_t press_count_for_wait = 0;
	uint32_t total_wait_count=((btn_dtlist[idx].press_duration/ TICK_RATE_TO_MS) / (DELAY_500_MSEC/ TICK_RATE_TO_MS));
	while(1){
		if (btn_dtlist[idx].btn_interaction_type == LONG_PRESSED_BTN_INTERACTION || btn_dtlist[idx].btn_interaction_type ==
		ALL_PRESSED_BTN_INTERACTION) {
			if (get_bsp_board_btn_state(idx) == true) {
				press_count_for_wait += 1;
				if (press_count_for_wait > total_wait_count) {
					CORE_LOGD(TAG, "Button's Long Press Detection completed. Calling the handler ...");
					if (btn_dtlist[idx].notify_btn_stateCb != NULL) {
						btn_dtlist[idx].notify_btn_stateCb(io_num, LONG_PRESSED);
					} else {
						printf("btn_dtlist[idx].notify_btn_stateCb == NULL\r\n");
					}
					break;
				}
			} else {
				break;
			}
		} else {
			break;
		}
		TaskDelay(DELAY_500_MSEC/ TICK_RATE_TO_MS);
	}
	CORE_LOGD(TAG,"%s for Btn %d killed\r\n", __func__, io_num);
	TaskDelete(NULL);
}

static uint8_t long_intercation_detection(uint32_t io_num){

	BaseType xReturned = TaskCreate(long_press_detection_task, "long_press_detect", TASK_STACK_SIZE_2K, (void*)io_num, THREAD_PRIORITY_4, NULL);
		if (true != xReturned) {
			CORE_LOGE(TAG, "Failed start btn-interaction task");
			return 1;
		}
	return 0;
}

static uint8_t short_intercation_detection(uint32_t press_duration, uint8_t idx, uint32_t io_num){
	if ((press_duration >= DEFAULT_SHORT_PRESS_DURATION / TICK_RATE_TO_MS)
							&& (press_duration < DEFAULT_LONG_PRESS_DURATION / TICK_RATE_TO_MS)) {
		CORE_LOGD(TAG, "Button's Short Press Detection completed. Calling the handler ...");
		btn_dtlist[idx].notify_btn_stateCb(io_num, SHORT_PRESSED);
		return 0;
	}
	return 1;
}

static uint8_t single_intercation_detection(uint32_t press_duration, uint8_t idx, uint32_t io_num){
	if (press_duration >= (btn_dtlist[idx].press_duration / TICK_RATE_TO_MS)) {
		CORE_LOGD(TAG, "Button's Custom Single Press Detection completed. Calling the handler ...");
		btn_dtlist[idx].notify_btn_stateCb(io_num, CUSTOM_PRESSED);
		return 0;
	}
	return 1;
}

/*
 * Button Interaction task
 */
static void btn_interaction_task(void* pvParameters) {
	uint32_t io_num;
	while (1) {
		if (QueueReceive(btnhelper_dt.btn_queue, &io_num, THREADING_MAX_DELAY)) {
			CORE_LOGD(TAG, "GPIO[%d] intr, val: %d\n", io_num, gpio_io_get_level(io_num));

			uint32_t idx = pin_to_btn_idx_bsp_board(io_num);

#if BUTTONS_NUMBER  > 0
			if(get_bsp_board_btn_state(idx) == true) {
#else
#error "BUTTONS_ACTIVE_STATE is not defined. Please set this MACRO in thing.h"
			if (get_bsp_board_btn_state(idx) == 0) {
#endif
				if (btn_dtlist[idx].bnt_hlp_dt.press_tick == 0) {
					btn_dtlist[idx].bnt_hlp_dt.press_tick = TaskGetTickCount();
					CORE_LOGD(TAG, "BUTTONS_PRESSED : %d \r\n", btn_dtlist[idx].bnt_hlp_dt.press_tick);
					btn_dtlist[idx].bnt_hlp_dt.press_stat= true;
					if(long_intercation_detection(io_num)!=0){

					}
				}
			} else {
				if(btn_dtlist[idx].bnt_hlp_dt.press_stat == true){
					uint32_t press_duration = TaskGetTickCount() - btn_dtlist[idx].bnt_hlp_dt.press_tick;
					CORE_LOGD(TAG, "BUTTONS_RELEASED: %d = %d-%d \r\n", press_duration, TaskGetTickCount(), btn_dtlist[idx].bnt_hlp_dt.press_tick);

					if (btn_dtlist[idx].btn_interaction_type == DOUBLE_PRESSED_BTN_INTERACTION) {
						if(double_intercation_detection(press_duration, idx, io_num) !=0 ){

						}
					}
					else if (btn_dtlist[idx].btn_interaction_type == SHORT_PRESSED_BTN_INTERACTION) {
						if(short_intercation_detection(press_duration, idx, io_num)!=0){

						}
					} else if (btn_dtlist[idx].btn_interaction_type == CUSTOM_PRESSED_BTN_INTERACTION) {
						if(single_intercation_detection(press_duration, idx, io_num)!=0){

						}
					} else if (btn_dtlist[idx].btn_interaction_type == ALL_PRESSED_BTN_INTERACTION) {
						if(double_intercation_detection(press_duration, idx, io_num) !=0 ){
							if(short_intercation_detection(press_duration, idx, io_num)!=0){

							}
						}
					}

					btn_dtlist[idx].bnt_hlp_dt.press_tick = 0;
					btn_dtlist[idx].bnt_hlp_dt.press_stat=false;

				}
			}
		}
//		CORE_LOGD(TAG, "Stack remaining for task '%s' is %d bytes\r\n", TaskGetTaskName(NULL), TaskGetStackHighWaterMark(NULL));
		TaskDelay(DELAY_20_MSEC / TICK_RATE_TO_MS);
	}
}

/******************************************************************************************************/

/*
 *
 */
core_err init_btn_interface(ButtonData *btn_dt) {
	core_err ret = CORE_FAIL;
#if BUTTONS_NUMBER  > 0
	uint32_t idx = pin_to_btn_idx_bsp_board(btn_dt->io_pin);
#else
#error "BUTTONS_NUMBER is either not defined or set to 0. Please set this MACRO in thing.h"
	uint32_t idx = 0;
#endif
	if (0xFFFFFFFF != idx) {
		/* Reset the tick counts initially */
		btn_dt->bnt_hlp_dt.press_tick = 0;
		btn_dt->bnt_hlp_dt.release_tick = 0;

		memset(&btn_dtlist[idx], 0x00, sizeof(ButtonData));
		memcpy(&btn_dtlist[idx], btn_dt, sizeof(ButtonData));

		ret = configure_gpio(btn_dt->io_pin, GPIO_IO_MODE_INPUT, btn_dt->io_pull_mode);
		if(ret == CORE_OK){
			ret = add_gpio_isr(btn_dt->io_pin, btn_dt->intr_type, btn_isr_handler);
			if(ret != CORE_OK){
				CORE_LOGE(TAG, "Failed to add ISR for Button GPIO: %d", btn_dt->io_pin);
			}
		}else{
			CORE_LOGE(TAG, "Failed to Configure GPIO: %d", btn_dt->io_pin);
		}
	}

	return ret;
}

/*
 *
 */
core_err init_long_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode,  notify_btn_state cb_handler){
	core_err ret = CORE_FAIL;

	ButtonData btn_dt;
	// configure ButtonData for reset button (depend on connectivity)
	btn_dt.io_pin = btn_gpio;
//	btn_dt.io_pulldown_mode = GPIO_IO_PULLDOWN_ENABLE;
//	btn_dt.io_pullup_mode = GPIO_IO_PULLUP_DISABLE;
	btn_dt.io_pull_mode=pull_mode;
	btn_dt.intr_type = GPIO_INTERRUPT_ANYEDGE;
	btn_dt.press_duration = DEFAULT_LONG_PRESS_DURATION;
	btn_dt.btn_interaction_type=LONG_PRESSED_BTN_INTERACTION;
	btn_dt.notify_btn_stateCb = cb_handler;
	btn_dt.bnt_hlp_dt.press_stat=false;
	btn_dt.bnt_hlp_dt.release_stat=false;
	btn_dt.bnt_hlp_dt.pressed_count=0;
	ret = init_btn_interface(&btn_dt);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Button: %d Failed to %s",btn_gpio, __func__);
	}
	return ret;
}

/*
 *
 */
core_err init_custom_long_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode, uint32_t press_duration, notify_btn_state cb_handler){
	core_err ret = CORE_FAIL;

	ButtonData btn_dt;
	// configure ButtonData for reset button (depend on connectivity)
	btn_dt.io_pin = btn_gpio;
//	btn_dt.io_pulldown_mode = GPIO_IO_PULLDOWN_ENABLE;
//	btn_dt.io_pullup_mode = GPIO_IO_PULLUP_DISABLE;
	btn_dt.io_pull_mode=pull_mode;
	btn_dt.intr_type = GPIO_INTERRUPT_ANYEDGE;
	btn_dt.press_duration = press_duration;
	btn_dt.btn_interaction_type=LONG_PRESSED_BTN_INTERACTION;
	btn_dt.notify_btn_stateCb = cb_handler;
	btn_dt.bnt_hlp_dt.press_stat=false;
	btn_dt.bnt_hlp_dt.release_stat=false;
	btn_dt.bnt_hlp_dt.pressed_count=0;
	ret = init_btn_interface(&btn_dt);
	if (ret != CORE_OK) {
		CORE_LOGE(LTAG_HMI, "Button: %d Failed to %s",btn_gpio, __func__);
	}
	return ret;
}

/*
 *
 */
core_err init_short_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode,  notify_btn_state cb_handler){
	core_err ret = CORE_FAIL;

	ButtonData btn_dt;
	// configure ButtonData for reset button (depend on connectivity)
	btn_dt.io_pin = btn_gpio;
	btn_dt.io_pull_mode=pull_mode;
	btn_dt.intr_type = GPIO_INTERRUPT_ANYEDGE;
	btn_dt.press_duration = DEFAULT_SHORT_PRESS_DURATION;
	btn_dt.btn_interaction_type=SHORT_PRESSED_BTN_INTERACTION;
	btn_dt.notify_btn_stateCb = cb_handler;
	btn_dt.bnt_hlp_dt.press_stat=false;
	btn_dt.bnt_hlp_dt.release_stat=false;
	btn_dt.bnt_hlp_dt.pressed_count=0;
	ret = init_btn_interface(&btn_dt);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Button: %d Failed to %s",btn_gpio, __func__);
	}
	return ret;
}

/*
 *
 */
core_err init_custom_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode, uint32_t press_duration, notify_btn_state cb_handler){
	core_err ret = CORE_FAIL;

	ButtonData btn_dt;
	// configure ButtonData for reset button (depend on connectivity)
	btn_dt.io_pin = btn_gpio;
	btn_dt.io_pull_mode=pull_mode;
	btn_dt.intr_type = GPIO_INTERRUPT_ANYEDGE;
	btn_dt.press_duration = press_duration;
	btn_dt.btn_interaction_type=CUSTOM_PRESSED_BTN_INTERACTION;
	btn_dt.notify_btn_stateCb = cb_handler;
	btn_dt.bnt_hlp_dt.press_stat=false;
	btn_dt.bnt_hlp_dt.release_stat=false;
	btn_dt.bnt_hlp_dt.pressed_count=0;
	ret = init_btn_interface(&btn_dt);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Button: %d Failed to %s",btn_gpio, __func__);
	}
	return ret;
}

/*
 *
 */
core_err init_double_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode,  notify_btn_state cb_handler){
	core_err ret = CORE_FAIL;

	ButtonData btn_dt;
	// configure ButtonData for reset button (depend on connectivity)
	btn_dt.io_pin = btn_gpio;
	btn_dt.io_pull_mode=pull_mode;
	btn_dt.intr_type = GPIO_INTERRUPT_ANYEDGE;
	btn_dt.press_duration = DEFAULT_PRESS_DURATION;
	btn_dt.btn_interaction_type=DOUBLE_PRESSED_BTN_INTERACTION;
	btn_dt.notify_btn_stateCb = cb_handler;
	btn_dt.bnt_hlp_dt.press_stat=false;
	btn_dt.bnt_hlp_dt.release_stat=false;
	btn_dt.bnt_hlp_dt.pressed_count=0;
	ret = init_btn_interface(&btn_dt);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Button: %d Failed to %s",btn_gpio, __func__);
	}
	return ret;
}

/*
 *
 */
core_err init_all_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode,  notify_btn_state cb_handler){
	core_err ret = CORE_FAIL;

	ButtonData btn_dt;
	// configure ButtonData for reset button (depend on connectivity)
	btn_dt.io_pin = btn_gpio;
	btn_dt.io_pull_mode=pull_mode;
	btn_dt.intr_type = GPIO_INTERRUPT_ANYEDGE;
	btn_dt.press_duration = DEFAULT_LONG_PRESS_DURATION;
	btn_dt.btn_interaction_type=ALL_PRESSED_BTN_INTERACTION;
	btn_dt.notify_btn_stateCb = cb_handler;
	btn_dt.bnt_hlp_dt.press_stat=false;
	btn_dt.bnt_hlp_dt.release_stat=false;
	btn_dt.bnt_hlp_dt.pressed_count=0;
	ret = init_btn_interface(&btn_dt);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Button: %d Failed to %s",btn_gpio, __func__);
	}
	return ret;
}

/*
 *
 */
core_err init_buttons() {
	/* If the Button Helper library is initialized once then no need to reinit again */
	if (btnhelper_dt.lib_init_stat) {
		return CORE_OK;
	}

	init_platform_buttons(1);

	// Create queue for hold BSP Buttons's input status
	btnhelper_dt.btn_queue = QueueCreate(10, sizeof(uint32_t));
	if (btnhelper_dt.btn_queue == NULL) {
		CORE_LOGE(TAG, "Failed to create queue: %s, %d", (char*) __FILE__, __LINE__);
		return CORE_FAIL;
	}

	// memory allocate for BSP Buttons
	if (btn_dtlist == NULL) {
		btn_dtlist = calloc(BUTTONS_NUMBER, sizeof(ButtonData));
		if (btn_dtlist == NULL) {
			CORE_LOGE(TAG, "Failed allocate memory for btn-list");
			return CORE_FAIL;
		}
	}

	// create BSP Button's task
	BaseType xReturned = TaskCreate(btn_interaction_task, "btn-task", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_4, NULL);
	if (true != xReturned) {
		CORE_LOGE(TAG, "Failed start btn-interaction task");
		return CORE_FAIL;
	}

	/* Button Helper library initialized successfully */
	btnhelper_dt.lib_init_stat = true;

	return CORE_OK;
}

#endif /* CONFIG_USE_BUTTON */
