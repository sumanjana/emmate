# Character LCD Module

## Overview

The CLCD Module provides APIs to display string on a Character LCD. Here, the application can initialize a CLCD with row & column and display string into that CLCD with the scrolling & blinking transitions.

## Features
- String will displayed until the CLCD gets de-initialized
- Scroll transision (Left-to-Right & Right-to-Left)
- Blink transision.
- String can be set in between specific coloumns
- String can be set in between multiple rows *(not yet implemented)*
- CLCD Library support upto 20x4 LCD

## How to use this module from an application

The below code snippet shows to an example to displaying strings into 20x4 CLCD:

- Initialization

```c

void clcd_display_init() {

	/*
	 * Initialize clcd with column & row, here we using 20x4 lcd display
	 * EmMate CLCD library can support upto 20x4 CLCD Display.
	 */
	uint8_t clcd_row = 4; 
	uint8_t clcd_column = 20;
	core_err ret = init_clcd(clcd_row, clcd_column);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize %dX%d CLCD", clcd_column, clcd_row);
		return;
	}
}
```
- De-Initialization

```c
void clcd_display_deinit() {

	/*
	 * De-Initialize CLCD.
	 */
	core_err ret = deinit_clcd();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "failed to de-initialize CLCD");
		return;
	}
}
```

- Set Display string into CLCD

```c
void clcd_display_company_name() {

	/*
	 * Set string for displaying 1st row
	 * Company Name: "Iquester SolutionLLP"
	 * Start Row 0	 (because string displayed on 1st row & 1st rows's starting coodinate is 0)
	 * End Row 0 (because string displayed on 1st row & 1st row's ending coodinate is 0)
	 * Start Column 0 (because string displayed from 1st column. So, column's starting coodinate is 0)
	 * End Column 19 (because string displayed upto last column of the 1st row. So, column's ending coodinate 19. [Initialize CLCD's COLUMN_RANGE-1])
	 * Scroll option [SCROLL_OFF, SCROLL_LEFT_TO_RIGHT, SCROLL_RIGHT_TO_LEFT]
	 * Scroll frequency(in ms) -> 0ms 
	 * Blink frequency(in ms) -> 0ms
	 * Overwrite option [NOT_OVERWRITE, OVERWRITE]
	 *
	 */
	 
	char *company_name = "Iquester Sol. LLP";
	core_err res = set_string_to_clcd(company_name, 0, 0, 0, 19, SCROLL_OFF, 0, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, "In Func: '%s' set_string_to_clcd failed!", __func__);
		return;
	}
}
```

- Set Display string with Scroll (Left to Right) into CLCD

```c
void clcd_display_platform_name() {

	/*
	 * Set string for displaying 2nd row
	 * Platform Name: "EmMate Framework"
	 * Start Row 1
	 * End Row 1
	 * Start Column 2 
	 * End Column 17
	 * Scroll option -> SCROLL_LEFT_TO_RIGHT
	 * Scroll frequency(in ms) -> 500ms
	 * Blink frequency(in ms),
	 * Overwrite option -> NOT_OVERWRITE
	 *
	 */

	char *platform_name = "EmMate Framework";
	core_err res = set_string_to_clcd(platform_name, 1, 1, 2, 17, SCROLL_LEFT_TO_RIGHT, 500, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, "In Func: '%s' set_string_to_clcd failed!", __func__);
		return;
	}
}
```

- Set Display string with Blink into CLCD

```c
void clcd_display_greeting_msg() {

	/*
	 * Set string for displaying 3rd row
	 * Message: "Hello! User."
	 * Start Row 2
	 * End Row 2
	 * Start Column 4 
	 * End Column 15
	 * Scroll option -> SCROLL_OFF
	 * Scroll frequency(in ms) -> 0ms
	 * Blink frequency(in ms) -> 250ms,
	 * Overwrite option -> NOT_OVERWRITE
	 *
	 */

	char *greeting_msg = "Hello! User.";
	core_err res = set_string_to_clcd(greeting_msg, 2, 2, 4, 15, SCROLL_OFF, 0, 250, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, "In Func: '%s' set_string_to_clcd failed!", __func__);
		return;
	}
}
```

- Set Display string with overwrite into CLCD

```c
void clcd_display_overwrite_msg(uint8_t msg_num) {

	/*
	 * Set string for displaying 4th row
	 * Message-0: "Welcome! User."
	 * Message-1: "Bye Bye! User."
	 * Start Row 3
	 * End Row 3
	 * Start Column 3 
	 * End Column 16
	 * Scroll option -> SCROLL_OFF
	 * Scroll frequency(in ms) -> 0ms
	 * Blink frequency(in ms) -> 0ms,
	 * Overwrite option -> OVERWRITE
	 *
	 */

	core_err res;
	char *msg_0 = "Welcome! User."
	char *msg_1 = "Bye Bye! User."
	
	if (msg_num == 0 ){
		res = set_string_to_clcd(msg_0, 3, 3, 3, 16, SCROLL_OFF, 0, 0, OVERWRITE);
	}else{
		res = set_string_to_clcd(msg_1, 3, 3, 3, 16, SCROLL_OFF, 0, 0, OVERWRITE);
	}
	
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, "In Func: '%s' set_string_to_clcd failed!", __func__);
		return;
	}
}
```



- Complete CLCD Application with Upper mentioned codes

```c
#include "clcd.h"
#include "core_logger.h"
#include "thing.h"

void core_app_main(void * param) {
	
	/* Initialize clcd */
	clcd_display_init();
	
	/* Set Company String to clcd */
	clcd_display_company_name();
	
	/* Set PlatForm String to clcd */
	clcd_display_platform_name();
	
	/* Set GreetingMessge String to clcd */
	clcd_display_greeting_msg();
	
	uint8_t msg_num=0;
	
	while(1){
	
		clcd_display_overwrite_msg(uint8_t msg_num);
		
		msg_num ^= 1; // Toggle the Message selection number
		
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}

```

**Please see the CLCD Examples for more info**


##### Header(s) to include

```c
	clcd.h
```

#### Module Specific Configurations

The following configurations are needed for the CLCD module to work.

![](img/clcd-config.png)
