# Human-Machine Interface (HMI)
## Overview
The Human-Machine Interface module provides easy to use features that can be used by an application to interact with the external world.

This module provides generic features of HMI that can be used in any application. Some examples would be:
	- Blinking LEDs in any pattern
	- Taking input from 1 or more push buttons
	- Displaying text on a LCD and similar HMI devices

Most of the application code is already written in this module thus making it easier for the app developer. Only specific APIs calls of this module needs to be made to generate the required output.

## Submodules
The HMI module has the following submodules:
1. LED - For more details see [Details of LED Module](led/README.md)
2. Button - For more details see [Details of Button Module](button/README.md)
3. Character LCD - For more details see [Details of Button Module](clcd/README.md)
4. Graphic LCD *(not yet implemented)*
5. OLED Display *(not yet implemented)*
6. Touchscreen Display *(not yet implemented)*

## How to use the HMI Module from an Application

##### Header(s) to include

```
hmi.h
```

#### Module Specific Configurations

The following configurations are needed for the HMI module to work.

![](img/hmi-config.png)

#### Module Specific APIs
1. For initializing the LED submodule

```c
init_hmi_led();
```

2. For initializing the Button submodule

```c
init_hmi_button();
```