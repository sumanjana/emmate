/*
 * system_hmi_led_notification.h
 *
 *  Created on: 20-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_HMI_LED_NOTIFICATION_H_
#define SYSTEM_HMI_LED_NOTIFICATION_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"


///////////////////////////////////////// LEDS /////////////////////////////////////////

void show_booting_notification();
void show_ble_connected_notification();
void show_ble_advertising_notification();
void show_network_connected_notification();
void show_network_connecting_notification();
void show_system_running_notification();
void show_system_fault_notification();
void show_system_resetting_notification();


#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_HMI_LED_NOTIFICATION_H_ */
