/*
 * File Name: led_patterns.h
 * File Path: /emmate/src/hmi/led/led_patterns.h
 * Description:
 *
 *  Created on: 17-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef LED_PATTERNS_H_
#define LED_PATTERNS_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "led_helper.h"

/*
 * LED Notification Pattern Macros
 * Rule:
 * LED_<MONO/COLOR>_<ON-MS>_<OFF-MS>_<REPEAT>
 */

//////////////////////////////////////////////////////// MONO LED ////////////////////////////////////////////////////////

/** MONO - 100 to 1000 - 100 to 1000 - single time*/
#define LED_MONO_100_100_1			make_led_pattern(COLOR_MONO,100,100,1)
#define LED_MONO_250_250_1			make_led_pattern(COLOR_MONO,250,250,1)
#define LED_MONO_500_500_1			make_led_pattern(COLOR_MONO,500,500,1)
#define LED_MONO_1000_1000_1		make_led_pattern(COLOR_MONO,1000,1000,1)

/** MONO - 100-250 - single time*/
#define LED_MONO_100_250_1			make_led_pattern(COLOR_MONO,100,250,1)

/** MONO - always on */
#define LED_MONO_1000_1_1			make_led_pattern(COLOR_MONO,1000,1,1)

/** MONO - always off */
#define LED_MONO_1_1000_1			make_led_pattern(COLOR_MONO,1,1000,1)

/** MONO Special */
#define LED_MONO_100_100_2			make_led_pattern(COLOR_MONO,100,100,2)

//////////////////////////////////////////////////////// RGB LED ////////////////////////////////////////////////////////

/** RED/GREEN/BLUE - 100-100 - single time */
#define LED_RED_100_100_1			make_led_pattern(COLOR_RED,100,100,1)
#define LED_GREEN_100_100_1			make_led_pattern(COLOR_GREEN,100,100,1)
#define LED_BLUE_100_100_1			make_led_pattern(COLOR_BLUE,100,100,1)

/** RED/GREEN/BLUE - 250-250 - single time */
#define LED_RED_250_250_1			make_led_pattern(COLOR_RED,250,250,1)
#define LED_GREEN_250_250_1			make_led_pattern(COLOR_GREEN,250,250,1)
#define LED_BLUE_250_250_1			make_led_pattern(COLOR_BLUE,250,250,1)

/** RED/GREEN/BLUE - 500-500 - single time */
#define LED_RED_500_500_1			make_led_pattern(COLOR_RED,500,500,1)
#define LED_GREEN_500_500_1			make_led_pattern(COLOR_GREEN,500,500,1)
#define LED_BLUE_500_500_1			make_led_pattern(COLOR_BLUE,500,500,1)

/** RED/GREEN/BLUE - 1000-1000 - single time */
#define LED_RED_1000_1000_1			make_led_pattern(COLOR_RED,1000,1000,1)
#define LED_GREEN_1000_1000_1			make_led_pattern(COLOR_GREEN,1000,1000,1)
#define LED_BLUE_1000_1000_1			make_led_pattern(COLOR_BLUE,1000,1000,1)

/** RED/GREEN/BLUE always on */
#define LED_RED_1000_1_1			make_led_pattern(COLOR_RED,1000,1,1)
#define LED_GREEN_1000_1_1			make_led_pattern(COLOR_GREEN,1000,1,1)
#define LED_BLUE_1000_1_1			make_led_pattern(COLOR_BLUE,1000,1,1)

/** Special */
#define LED_RED_100_250_4			make_led_pattern(COLOR_RED,100,250,4)

#define LED_BLUE_500_1000_2			make_led_pattern(COLOR_BLUE,500,1000,2)

#define LED_RED_100_250_2			make_led_pattern(COLOR_RED,100,250,2)

#define LED_GREEN_100_100_2			make_led_pattern(COLOR_GREEN,100,100,2)

#define LED_GREEN_100_250_2			make_led_pattern(COLOR_GREEN,100,250,2)

#define LED_BLUE_100_250_2			make_led_pattern(COLOR_BLUE,100,250,2)

#define LED_WHITE_100_250_1			make_led_pattern(COLOR_WHITE,100,250,1)

#define LED_WHITE_1000_1_1			make_led_pattern(COLOR_WHITE,1000,1,1)


/**
 * @brief  Makes a LED pattern based on input parameters
 *
 * @param[in]	color_code RGB color code
 * @param[in]	on_ms ON time in ms
 * @param[in]	off_ms 'OFF time in ms
 * @param[in]	repeat Number of times a pattern should repeat
 *
 * @return
 *         - LedPattern* if success
 *         - NULL if failed
 */
LedPattern* make_led_pattern(uint32_t color_code, uint16_t on_ms, uint16_t off_ms, uint16_t repeat);

#endif /* LED_PATTERNS_H_ */
