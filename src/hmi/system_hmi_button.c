/*
 * system_hmi_button.c
 *
 *  Created on: 20-Jul-2019
 *      Author: Rohan Dey
 */
#include "system_hmi_button.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_BUTTON
#include "button_helper.h"
#endif
#include "hmi.h"

#define TAG	LTAG_SYSTEM_HMI

#if (CONFIG_HAVE_SYSTEM_HMI && CONFIG_ENABLE_SYSTEM_BUTTON_HMI && CONFIG_USE_BUTTON)
///////////////////////////////////////// BUTTONS /////////////////////////////////////////
static reset_interaction_cb rst_intact_handler;
static void hmi_reset_btn_event_handler(uint8_t io_pin, uint32_t rst_intact_type) {
	CORE_LOGD(TAG, "Reset Btn ( %d ) Event Called, InterAction Type %d", io_pin, rst_intact_type);
	if (rst_intact_handler != NULL) {
		rst_intact_handler(rst_intact_type);
	}
}

void init_reset_btn() {
//	ButtonData btn_dt;
//	// configure ButtonData for reset button (depend on connectivity)
//	btn_dt.io_pin = SYSTEM_RESET_BUTTON;
//	btn_dt.io_pulldown_mode = GPIO_IO_PULLDOWN_ENABLE;
//	btn_dt.io_pullup_mode = GPIO_IO_PULLUP_DISABLE;
//	btn_dt.int_type = GPIO_INTERRUPT_ANYEDGE;
//	btn_dt.press_duration = 5000;
//	btn_dt.notify_btn_stateCb = hmi_reset_btn_event_handler;
//	core_err ret = init_btn_interface(&btn_dt);
//	if (ret != CORE_OK) {
//		CORE_LOGE(LTAG_HMI, "Failed to %s", __func__);
//	}

	init_long_press_btn_interface(SYSTEM_RESET_BUTTON, GPIO_IO_FLOATING, hmi_reset_btn_event_handler);
//	init_double_press_btn_interface(SYSTEM_RESET_BUTTON, GPIO_IO_PULLDOWN_ONLY, hmi_reset_btn_event_handler);
//	init_custom_press_btn_interface(SYSTEM_RESET_BUTTON, GPIO_IO_PULLDOWN_ONLY, 6000, hmi_reset_btn_event_handler);
//	init_all_press_btn_interface(SYSTEM_RESET_BUTTON, GPIO_IO_PULLDOWN_ONLY, hmi_reset_btn_event_handler);
}

void register_hmi_reset_button_cb(reset_interaction_cb cb_handler) {
	if (cb_handler != NULL) {
		rst_intact_handler = cb_handler;
	}
}
#else

void init_reset_btn(){}
void register_hmi_reset_button_cb(reset_interaction_cb cb_handler){}

#endif	/* (CONFIG_HAVE_SYSTEM_HMI && CONFIG_ENABLE_SYSTEM_BUTTON_HMI && CONFIG_USE_BUTTON) */
