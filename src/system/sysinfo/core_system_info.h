/*
 * File Name: core_system_info.h
 * File Path: /emmate/src/system/sysinfo/core_system_info.h
 * Description:
 *
 *  Created on: 21-May-2019
 *      Author: Rohan Dey
 */

#ifndef CORE_SYSTEM_INFO_H_
#define CORE_SYSTEM_INFO_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "version.h"
#include "system_info.h"

typedef struct {
	char core_version[CORE_VERSION_NUMBER_LEN+1];	/**< Core Framework's version number */
	char app_version[APP_VERSION_NUMBER_LEN+1];		/**< Core Application's version number */
} FrameworkInfo;

typedef struct {
	PlatformInfo platform_info;
	FrameworkInfo framework_info;
} SystemInfo;

/**
 * @brief	Get hardware and framework information
 * */
void read_save_system_info();

/**
 * @brief	Print the system's info
 * */
void print_system_info();

/**
 * @brief	Returns an object of SystemInfo. read_save_system_info() must be called before calling
 * 			this function
 *
 * @return
 * 			SystemInfo *	pointer to a SystemInfo object populated with all the info
 * 			NULL			if read_save_system_info() has not been called before
 * */
SystemInfo * get_system_info();

/**
 * @brief	Returns an object of PlatformInfo. read_save_system_info() must be called before calling
 * 			this function
 *
 * @return
 * 			PlatformInfo *	pointer to a PlatformInfo object populated with all the info
 * 			NULL			if read_save_system_info() has not been called before
 * */
PlatformInfo * get_platform_info();

/**
 * @brief	Returns an object of HardwareInfo. read_save_system_info() must be called before calling
 * 			this function
 *
 * @return
 * 			HardwareInfo *	pointer to a HardwareInfo object populated with all the info
 * 			NULL			if read_save_system_info() has not been called before
 * */
HardwareInfo * get_hardware_info();

/**
 * @brief	Returns an object of SDKInfo. read_save_system_info() must be called before calling
 * 			this function
 *
 * @return
 * 			SDKInfo *	pointer to a SDKInfo object populated with all the info
 * 			NULL			if read_save_system_info() has not been called before
 * */
SDKInfo * get_sdk_info();

/**
 * @brief	Returns an object of FrameworkInfo. read_save_system_info() must be called before calling
 * 			this function
 *
 * @return
 * 			FrameworkInfo *	pointer to a FrameworkInfo object populated with all the info
 * 			NULL			if read_save_system_info() has not been called before
 * */
FrameworkInfo * get_framework_info();

#endif /* CORE_SYSTEM_INFO_H_ */
