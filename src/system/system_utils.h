/*
 * File Name: system_utils.h
 * File Path: /emmate/src/system/system_utils.h
 * Description:
 *
 *  Created on: 28-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_UTILS_H_
#define SYSTEM_UTILS_H_

#include "core_logger.h"
#include "core_config.h"
#include "core_error.h"
#include "system.h"
#include "system_init.h"
#include "event_group_core.h"
#include "system_platform_utils.h"

//#define CONFIG_SHOW_THREAD_DEBUG	1

/**
 * @brief Return System's Group Event handle, for system event base process
 *
 * @return	System's Group Event handle
 *
 */
EventGrpHandle get_system_evtgrp_hdl();

/**
 * @brief	Function pointer type to point to a system reboot event notification callback
 *
 * @note	The function signature should be as follows:
 *
 * 			void app_systemreboot(void)
 */
typedef void (*systemreboot_function)(void);

/**
 * @brief		This function registers a function of type systemreboot_function, which will be called when an system reboot
 * 				event is generated within the system.
 *
 * @note		The system reboot event can be generated in the following cases:
 * 				- After completion of a FOTA (Firmware Over The Air) update operation
 * 				- System initialization failure
 * 				- After a factory reset event
 * 				- After a device configuration reset event
 *
 * @param[in]	systemreboot_func This is the pointer to the function which will be registered
 *
 * @return
 * 			- CORE_OK					If the function was registered successfully
 * 			- CORE_ERR_INVALID_ARG		If the input param is NULL
 * */
core_err register_system_reboot_event_function(systemreboot_function systemreboot_func);

/**
 * @brief Function used for Restart the core system.
 *
 */
void core_system_restart();

/**
 * @brief		This function calls the registered system reboot callback. The system reboot function should be registered
 * 				by calling register_system_reboot_event_function()
 * */
void notify_system_reboot_event();

/**
 * @brief Register the factory reset button and its callback function
 */
void register_factory_reset_button();

/**
 * @brief	Function pointer type to point to a factory reset event notification callback
 *
 * @note	The function signature should be as follows:
 *
 * 			void app_factoryreset(void)
 */
typedef void (*factoryreset_function)(void);

/**
 * @brief		This function registers a function of type factoryreset_function, which will be called when an factory reset
 * 				event is generated within the system. The factory reset event can be generated when a user presses the
 * 				factory reset button for a specified time period
 *
 * @param[in]	factoryreset_function This is the pointer to the function which will be registered
 *
 * @return
 * 			- CORE_OK					If the function was registered successfully
 * 			- CORE_ERR_INVALID_ARG		If the input param is NULL
 * */
core_err register_factory_reset_event_function(factoryreset_function factoryreset_func);

/**
 * @brief		This function call the registered factory reset callback. The factory reset function should be registered
 * 				by calling register_factory_reset_info_function()
 *
 * */
void notify_factory_reset_event();

#if CONFIG_SHOW_THREAD_DEBUG

/**
 * @brief A task for debugging purpose. this function start a task to logging all running threads in RTOS.
 *
 */
void start_sysTaskList_task();
#endif

/**
 * @brief Creating a Microseconds dalay
 *
 */
#define core_us_delay(delay)		core_platform_us_delay(delay)

#endif /* SYSTEM_UTILS_H_ */
