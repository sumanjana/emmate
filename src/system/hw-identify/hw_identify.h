/*
 * hw_identify.h
 *
 *  Created on: 29-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef HW_IDENTIFY_H_
#define HW_IDENTIFY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#if CONFIG_USE_WIFI
#include "wifi_constants.h"
#endif
#if CONFIG_USE_ETH
#include "eth_constants.h"
#endif
#if CONFIG_USE_GSM
#include "gsm_constants.h"
#endif
#if CONFIG_USE_BLE
#include "ble_constant.h"
#endif

/* Request */
/*
 * {
 * 	"wifi_mac": "24:0a:c4:8f:6c:0c",
 * 	"eth_mac": "",
 * 	"gsm_imei": "",
 * 	"bt_mac": ""
 * }
 */
typedef struct {
#if CONFIG_USE_WIFI
	uint8_t wifi_mac[WIFI_MAC_LEN+1];				/**< Wi-Fi module's MAC */
#endif

#if CONFIG_USE_ETH
	uint8_t eth_mac[ETH_MAC_LEN+1];					/**< Ethernet module's MAC */
#endif

#if CONFIG_USE_GSM
	uint8_t gsm_imei[GSM_IMEI_LEN+1];				/**< GSM module's IMEI number */
#endif

#if CONFIG_USE_BLE
	uint8_t bt_mac[BLE_MAC_LEN+1];					/**< BLE module's MAC */
#endif
} HWIdentificationRequest;

/* Response */
/*
 * {
 * 	"status": true,
 * 	"error": {
 * 		"err_code": 123,
 * 		"err_msg": "This is an error!"
 * 	},
 * 	"somthing_id": "",
 * }
 */
typedef struct {
	int status;						/**< Registration Status. Success or Failure */
	CoreError error;				/**< Error object if generated during the SOM registration process */
	char somthing_id[SOMTHING_ID_LEN+1];		/**< A System On Module (SOM) unique identifier. To be fetched from a hardwired silicon UID chip or from server */
//	char custom_hw_id[SOMTHING_ID_LEN+1];	/**< A custom board ID to be fetched from the server */
} HWIdentificationResponse;

/**
 *
 */
core_err start_hardware_identification();

#ifdef __cplusplus
}
#endif

#endif /* HW_IDENTIFY_H_ */
