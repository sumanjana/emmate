/*
 * hw_identify_parser.c
 *
 *  Created on: 29-Jul-2019
 *      Author: Rohan Dey
 */

#include "hw_identify_parser.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "input_processor.h"
#include "inpproc_utils.h"
#include "core_utils.h"
#include <string.h>

#define TAG		LTAG_HWIDENTIFY

core_err make_hwid_json(char **ppbuf, int *plen, HWIdentificationRequest *hwid_req) {
	/* Make a JSON with network interface's physical address only */
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

#define MAC_BUFSZ	50
	char buf[MAC_BUFSZ] = { 0 };
	int mret = -1;

#if CONFIG_USE_WIFI
	/* Making Wi-Fi MAC */
	memset(buf, 0, MAC_BUFSZ);
	mret = format_mac(hwid_req->wifi_mac, buf);
	if (mret == 0) {
		CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(hwid_req->wifi_mac, "->"), buf);
		json_object_set_string(root_object, GET_VAR_NAME(hwid_req->wifi_mac, "->"), (const char*) buf);
	} else if (mret == 1) {
		CORE_LOGD(TAG, "[%s]: NULL", GET_VAR_NAME(hwid_req->wifi_mac, "->"));
		json_object_set_string(root_object, GET_VAR_NAME(hwid_req->wifi_mac, "->"), "");
	} else {
		CORE_LOGE(TAG, "Could not make json key: %s", GET_VAR_NAME(hwid_req->wifi_mac, "->"));
	}
#endif
#if CONFIG_USE_ETH
	/* Making Ethernet MAC */
	memset(buf, 0, MAC_BUFSZ);
	mret = format_mac(hwid_req->eth_mac, buf);
	if (mret == 0) {
		CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(hwid_req->eth_mac, "->"), buf);
		json_object_set_string(root_object, GET_VAR_NAME(hwid_req->eth_mac, "->"), (const char*) buf);
	} else if (mret == 1) {
		CORE_LOGD(TAG, "[%s]: NULL", GET_VAR_NAME(hwid_req->eth_mac, "->"));
		json_object_set_string(root_object, GET_VAR_NAME(hwid_req->eth_mac, "->"), "");
	} else {
		CORE_LOGE(TAG, "Could not make json key: %s", GET_VAR_NAME(hwid_req->eth_mac, "->"));
	}
#endif
#if CONFIG_USE_GSM
	/* Making GSM IMEI */
	// TODO: check formatting for GSM IMEI
	json_object_set_string(root_object, GET_VAR_NAME(hwid_req->gsm_imei, "->"), (const char*) hwid_req->gsm_imei);
#endif
#if CONFIG_USE_BLE
	/* Making Bluetooth MAC */
	memset(buf, 0, MAC_BUFSZ);
	mret = format_mac(hwid_req->bt_mac, buf);
	if (mret == 0) {
		CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(hwid_req->bt_mac, "->"), buf);
		json_object_set_string(root_object, GET_VAR_NAME(hwid_req->bt_mac, "->"), (const char*) buf);
	} else if (mret == 1) {
		CORE_LOGD(TAG, "[%s]: NULL", GET_VAR_NAME(hwid_req->bt_mac, "->"));
		json_object_set_string(root_object, GET_VAR_NAME(hwid_req->bt_mac, "->"), "");
	} else {
		CORE_LOGE(TAG, "Could not make json key: %s", GET_VAR_NAME(hwid_req->bt_mac, "->"));
	}
#endif
	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	CORE_LOGD(TAG, "HW Identify JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		CORE_LOGE(TAG, "make_hwid_json malloc failed!");
		return CORE_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return CORE_OK;
}

core_err parse_hwid_response_json(char *json_buff, HWIdentificationResponse *hwid_resp) {
	core_err ret = CORE_FAIL;

	JSON_Value* root_value = NULL;
	JSON_Object * rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "JSON Value type not matched");
			ret = CORE_FAIL;
//			goto free_memory;
		} else {
			rootObj = json_value_get_object(root_value);

			/* Parse the common info: stat and error */
			ret = inproc_parse_json_common_info(json_buff, &hwid_resp->status, &hwid_resp->error);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
				ret = CORE_FAIL;
				goto free_memory;
			}

			/* Check if response as successful */
			if (hwid_resp->status) {

				/* parse som id */
				ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(hwid_resp->somthing_id, "->"), hwid_resp->somthing_id);
				if (ret == CORE_FAIL) {
					CORE_LOGE(TAG, "Could not parse JSON key %s!", GET_VAR_NAME(hwid_resp->somthing_id, "->"));
					ret = CORE_FAIL;
				}

//				/* parse custom hardware id */
//				ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(hwid_resp->custom_hw_id, "->"), hwid_resp->custom_hw_id);
//				if (ret == CORE_FAIL) {
//					CORE_LOGE(TAG, "Could not parse JSON key %s!", GET_VAR_NAME(hwid_resp->custom_hw_id, "->"));
//					return CORE_FAIL;
//				}

			} else {
				CORE_LOGE(TAG, "The server returned status as not successful! stat = %d", hwid_resp->status);
				ret = CORE_FAIL;
			}
		}
		free_memory:
		/* clear root_value */
		json_value_free(root_value);
	} else {
		CORE_LOGE(TAG, "Could not create JSON root object");
		ret = CORE_FAIL;
	}
	return ret;
}

void print_hwid_response(HWIdentificationResponse *hwid_resp) {
	CORE_LOGD(TAG, "");
	CORE_LOGD(TAG, "======================================================================================");
	CORE_LOGD(TAG, "Parsing Hardware Identification response completed... The following data was received:");
	CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hwid_resp->status, "->"), hwid_resp->status);
	CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hwid_resp->error.err_code, "->"), hwid_resp->error.err_code);
	CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hwid_resp->somthing_id, "->"), hwid_resp->somthing_id ? hwid_resp->somthing_id : "no id");
//	CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hwid_resp->custom_hw_id, "->"), hwid_resp->custom_hw_id ? hwid_resp->custom_hw_id : "no id");
	CORE_LOGD(TAG, "======================================================================================");
	CORE_LOGD(TAG, "");
}

