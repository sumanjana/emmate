/*
 * device_config_reset.h
 *
 *  Created on: 23-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef DEVICE_CONFIG_RESET_H_
#define DEVICE_CONFIG_RESET_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

/**
 * @brief	Function pointer type to point to a device configuration reset event notification callback
 *
 * @note	The function signature should be as follows:
 *
 * 			void app_device_config_reset(void)
 */
typedef void (*deviceconfigreset_function)(void);

/**
 * @brief		This function registers a function of type deviceconfigreset_function, which will be called when a device configuration
 * 				reset request is received from the server. This request can be received when a authentic user presses the device configuration
 * 				reset button in the migCloud portal
 *
 * @param[in]	factoryreset_function This is the pointer to the function which will be registered
 *
 * @return
 * 			- CORE_OK					If the function was registered successfully
 * 			- CORE_ERR_INVALID_ARG		If the input param is NULL
 * */
core_err register_device_config_reset_event_function(deviceconfigreset_function deviceconfigreset_func);

#ifdef __cplusplus
}
#endif

#endif /* DEVICE_CONFIG_RESET_H_ */
