/*
 * File Name: device_config.h
 * File Path: /emmate/src/system/device_config.h
 * Description:
 *
 *  Created on: 13-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef DEVICE_CONFIG_H_
#define DEVICE_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#include "core_constant.h"
#include "core_error.h"
#include "core_config.h"
//#include "system.h"

#include "event_group_core.h"

#if CONFIG_USE_WIFI
#include "wifi.h"
#endif

// Device ID METADATA Key
#define DEVID_METADATA_KEY "id"

/***************** Macros of AWS Credential **************************/
// AWS Credential METADATA Key
#define AWS_CERT_METADATA_KEY			"cert"
#define AWS_ROOTCA_METADATA_KEY			"rootca"
#define AWS_PRIV_METADATA_KEY			"key"
#define AWS_URL_METADATA_KEY			"url"
#define AWS_THINGNAME_METADATA_KEY		"thing"

#if CONFIG_USE_WIFI
/***************** Macros of WiFi Credential **************************/
// Wi-Fi related METADATA Key
#define WIFI_SSID_METADATA_KEY			"ssid"
#define WIFI_PWD_METADATA_KEY			"pwd"
#endif

typedef enum {
	SSID_BYTE_STREAM = 0,
	PWD_BYTE_STREAM,
	CERT_BYTE_STREAM,
	KEY_BYTE_STREAM,
	ROOTCA_BYTE_STREAM,
	URL_BYTE_STREAM,
	THING_BYTE_STREAM,
	DEFAULT_BYTE_STREAM,
} BYTE_STREAM_DATA;


#define NUM_DEVICE_CONFIGS		8

/**< Validation mask value. Depends on the number of Device Configurations */
#if (NUM_DEVICE_CONFIGS == 8)
#define DEVCFG_VALIDATION_MASK				0b00000011
#elif (NUM_DEVICE_CONFIGS == 16)
#define VALIDATION_MASK			0xFFFF
#elif (NUM_DEVICE_CONFIGS == 32)
#define VALIDATION_MASK			0xFFFFFFFF
#endif

/**
 * @brief  	This macro adds a bit starting from the LSB every time it is called.
 * 			It is used to prepare the device configuration checklist.
 *
 * 			Usage:
 * 			------
 * 			Prepare the device configuration checklist for later validation
 *
 * 			SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
 *
 * 	@param[in]	data The input data for adding a bit
 * 	@param[in]	mask The mask value. Default value = 0x01
 */
#define SET_DEVCONFIG_CHECKLIST(data, mask) {\
											data |= mask; \
											mask = mask << 1; }
/**
 * @brief  	This macro validates that all required configurations are set.
 * 			It does this by ANDing the data and comparing it with the mask
 *
 * 			Usage:
 * 			------
 * 			Validate that all configurations required in Level-1 are set
 *
 * 			bool is_config_ok = false;
 *
 * 			VALIDATE_DEVICE_CONFIG(sys_data->dev_cfg.dev_conf_checklist, DEVCFG_VALIDATION_MASK, is_config_ok);
 *
 * 	@param[in]	data The input data to be validated. Generally the device config checklist
 * 	@param[in]	mask The mask value to be ANDed
 * 	@param[out]	result Boolean to be set true or false depending on the validation
 */
#define VALIDATE_DEVICE_CONFIG(data, mask, result) {\
					if ((data & mask) == mask) \
						result = true; \
					else \
						result = false;}

/**
 * @brief  	This is the data structure to contain all device level configurations.
 * 			These configurations are set by the user via a peripheral such as BLE, USB, UART etc. and then they are
 * 			stored into a persistent memory. During initialization the configurations are read from the persistent
 * 			memory and stored in this data structure for further system's usage
 */
typedef struct {
#if (NUM_DEVICE_CONFIGS == 8)
/** Each bit of these variable corresponds to a device configuration.
 * Size of this variable depends on the macro NUM_DEVICE_CONFIGS
 *
 * conf_checklist bit expansion:
 * bit 0 : device id
 * bit 1 : interface config 1 (e.g. wifi ssid)
 * bit 2 : interface config 2 (e.g. wifi pwd)
 */
	uint8_t dev_conf_checklist;
	uint8_t checklist_mask;
#elif (NUM_DEVICE_CONFIGS == 16)
	uint16_t dev_conf_checklist;
	uint16_t checklist_mask;
#elif (NUM_DEVICE_CONFIGS == 32)
	uint32_t dev_conf_checklist;
	uint32_t checklist_mask;
#endif
	char devid[DEV_ID_LEN];			/**< Unique Device ID. Could be MAC ID*/
	uint16_t devid_len;				/**< Length of Device ID*/
#if CONFIG_USE_BLE
#endif
#if CONFIG_USE_WIFI
	WifiConfig wifi_cfg;			/**< Structure to contain Wi-Fi configurations */
#endif
#if IFACE_NBIOT
#endif
#if IFACE_GSM
#endif

#if SERVER_AWSIOT
	char aws_cert[AWS_CERT_AND_KEY_LEN];	/* AWS Certificate */
	char aws_key[AWS_CERT_AND_KEY_LEN];		/* AWS Priv Key */
	char aws_rootca[AWS_CERT_AND_KEY_LEN];	/* AWS RootCA */
	char aws_url[AWS_URL_LEN];				/* AWS Endpoint URL */
	char aws_thingname[AWS_THING_NAME_LEN];	/* AWS Thingname */

	uint16_t aws_cert_len;					/* AWS Certificate Length */
	uint16_t aws_key_len;					/* AWS Priv Key Length */
	uint16_t aws_rootca_len;				/* AWS RootCA Length */
	uint16_t aws_url_len;					/* AWS Endpoint URL Length */
	uint16_t aws_thingname_len;				/* AWS Thingname Length */
#endif
	uint8_t opermode;						/**< Operation Mode of the System */
	uint8_t opermode_len;					/**< Length of Operation Mode variable */
} DeviceConfig;

/* Function Declarations */
/**
 * @brief  	Sets a DeviceConfig variable to defaults values
 *
 * @param[in]	devcfg Pointer to DeviceConfig
 */
void set_devcfg_to_default(DeviceConfig *devcfg);

/**
 * @brief  	Initializes the Device Configuration module and starts internal threads to do the configuration process
 *
 * @param[in]	devcfg Pointer to DeviceConfig
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL 	on failure
 */
core_err init_and_start_device_cfg(DeviceConfig *devcfg);

#ifdef __cplusplus
}
#endif

#endif /* DEVICE_CONFIG_H_ */
