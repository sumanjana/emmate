/*
 * File Name: som_registration_parser.c
 * File Path: /emmate/src/system/som-registration/som_registration_parser.c
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#include "som_registration_parser.h"
#include "input_processor.h"
#include "inpproc_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "core_utils.h"
#include <string.h>

#define TAG	LTAG_SOM_REG

static JSON_Value * make_hardware_info_json(HardwareInfo *chip_info) {
	JSON_Value *ci_value = json_value_init_object();
	JSON_Object *ci_object = json_value_get_object(ci_value);

	json_object_set_string(ci_object, GET_VAR_NAME(chip_info->chip_name, "->"), chip_info->chip_name);
	json_object_set_string(ci_object, GET_VAR_NAME(chip_info->chip_mfgr, "->"), chip_info->chip_mfgr);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_wifi, "->"), chip_info->has_wifi);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_bt, "->"), chip_info->has_bt);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_ble, "->"), chip_info->has_ble);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_gsm_2g, "->"), chip_info->has_gsm_2g);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_gsm_3g, "->"), chip_info->has_gsm_3g);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_gsm_4g, "->"), chip_info->has_gsm_4g);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_lora, "->"), chip_info->has_lora);
	json_object_set_boolean(ci_object, GET_VAR_NAME(chip_info->has_nbiot, "->"), chip_info->has_nbiot);
	json_object_set_number(ci_object, GET_VAR_NAME(chip_info->flash_size, "->"), chip_info->flash_size);
	json_object_set_number(ci_object, GET_VAR_NAME(chip_info->ram_size, "->"), chip_info->ram_size);

	return ci_value;
}

core_err make_som_regn_request_json(char **ppbuf, int *plen, SomRegistrationRequest *regn_req) {
//#define JSON_KEY_SOMID				"som_id"
//#define JSON_KEY_CORE_VERSION		"core_version"
//#define JSON_KEY_APP_VERSION		"app_version"
//#define JSON_KEY_WIFI_MAC			"wifi_mac"
//#define JSON_KEY_ETH_MAC			"eth_mac"
//#define JSON_KEY_GSM_IMEI			"gsm_imei"
//#define JSON_KEY_BT_MAC				"bt_mac"

	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	CORE_LOGD(TAG, "Going to make SOM Registration JSON with the following values:");

	/* Set JSON key value */
	CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(regn_req->somthing_id, "->"), regn_req->som_id);
	json_object_set_string(root_object, GET_VAR_NAME(regn_req->somthing_id, "->"), regn_req->som_id);

	json_object_set_value(root_object, GET_VAR_NAME(regn_req->chip_info, "->"), make_hardware_info_json(regn_req->chip_info));

	CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(regn_req->core_version, "->"), regn_req->core_version);
	json_object_set_string(root_object, GET_VAR_NAME(regn_req->core_version, "->"), regn_req->core_version);

	CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(regn_req->app_version, "->"), regn_req->app_version);
	json_object_set_string(root_object, GET_VAR_NAME(regn_req->app_version, "->"), regn_req->app_version);

#define MAC_BUFSZ	50
	char buf[MAC_BUFSZ] = { 0 };
	int mret = -1;

#if CONFIG_USE_WIFI
	/* Making Wi-Fi MAC */
	memset(buf, 0, MAC_BUFSZ);
	mret = format_mac(regn_req->wifi_mac, buf);
	if(mret == 0) {
		CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(regn_req->wifi_mac, "->"), buf);
		json_object_set_string(root_object, GET_VAR_NAME(regn_req->wifi_mac, "->"), (const char*) buf);
	} else if(mret == 1) {
		CORE_LOGD(TAG, "[%s]: NULL", GET_VAR_NAME(regn_req->wifi_mac, "->"));
		json_object_set_string(root_object, GET_VAR_NAME(regn_req->wifi_mac, "->"), "");
	} else {
		CORE_LOGE(TAG, "Could not make json key: %s", GET_VAR_NAME(regn_req->wifi_mac, "->"));
	}
#endif
#if CONFIG_USE_ETH
	/* Making Ethernet MAC */
	memset(buf, 0, MAC_BUFSZ);
	mret = format_mac(regn_req->eth_mac, buf);
	if (mret == 0) {
		CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(regn_req->eth_mac, "->"), buf);
		json_object_set_string(root_object, GET_VAR_NAME(regn_req->eth_mac, "->"), (const char*) buf);
	} else if (mret == 1) {
		CORE_LOGD(TAG, "[%s]: NULL", GET_VAR_NAME(regn_req->eth_mac, "->"));
		json_object_set_string(root_object, GET_VAR_NAME(regn_req->eth_mac, "->"), "");
	} else {
		CORE_LOGE(TAG, "Could not make json key: %s", GET_VAR_NAME(regn_req->eth_mac, "->"));
	}
#endif
#if CONFIG_USE_GSM
	/* Making GSM IMEI */
	// TODO: check formatting for GSM IMEI
	json_object_set_string(root_object, GET_VAR_NAME(regn_req->gsm_imei, "->"), (const char*) regn_req->gsm_imei);
#endif
#if CONFIG_USE_BLE
	/* Making Bluetooth MAC */
	memset(buf, 0, MAC_BUFSZ);
	mret = format_mac(regn_req->bt_mac, buf);
	if (mret == 0) {
		CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(regn_req->bt_mac, "->"), buf);
		json_object_set_string(root_object, GET_VAR_NAME(regn_req->bt_mac, "->"), (const char*) buf);
	} else if (mret == 1) {
		CORE_LOGD(TAG, "[%s]: NULL", GET_VAR_NAME(regn_req->bt_mac, "->"));
		json_object_set_string(root_object, GET_VAR_NAME(regn_req->bt_mac, "->"), "");
	} else {
		CORE_LOGE(TAG, "Could not make json key: %s", GET_VAR_NAME(regn_req->bt_mac, "->"));
	}
#endif
	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	CORE_LOGD(TAG, "SOM Registration Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		CORE_LOGE(TAG, "make_som_request_json malloc failed!");
		return CORE_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return CORE_OK;
}

//#define RESP_JSON "{\"reg_stat\": true,\"error\": {\"err_code\": 123,\"err_msg\": \"This is an error!\"},\"reg_time\": 123456789,\"wifi_mac\": \"123456\",\"reg_location\": \"kolkata\",\"bt_passkey\": \"scvsdcbhaskdvbsaydkvbyr78362g5ybq\"}"

core_err parse_som_registration_response_json(char *json_buff, SomRegistrationResponse *regn_resp) {
//#define JSON_KEY_REGSTAT		"reg_stat"
//#define JSON_KEY_ERROR			"error"
//#define JSON_KEY_ERROR_CODE		"err_code"
//#define JSON_KEY_ERROR_MSG		"err_msg"
//#define JSON_KEY_REG_TIME		"reg_time"
//#define JSON_KEY_REG_LOC		"reg_location"
//#define JSON_KEY_BT_PASS		"bt_passkey"

	core_err ret = CORE_FAIL;

	JSON_Value* root_value = NULL;
	JSON_Object * rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "JSON Value type not matched");
			ret = CORE_FAIL;
			goto free_memory;
		} else {
			rootObj = json_value_get_object(root_value);

			/* Parse the common info: stat and error */
			ret = inproc_parse_json_common_info(json_buff, &regn_resp->status, &regn_resp->error);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
				ret = CORE_FAIL;
				goto free_memory;
			}
			/* Check if SOM registration was successful */
			if (regn_resp->status) {
				/* Get the som registration time */
				long long time_ms = json_object_get_number(rootObj, GET_VAR_NAME(regn_resp->reg_time, "->"));
				time_t reg_time_sec = time_ms / 1000;
#if CONFIG_PLATFORM_ESP_IDF
				gmtime_r(&reg_time_sec, &regn_resp->reg_time);
#endif

				/* Get the registration location */
				ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(regn_resp->reg_location, "->"), regn_resp->reg_location);
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, proceeding further anyways!",
							GET_VAR_NAME(regn_resp->reg_location, "->"));
				}

				/* Get the BLUETOOTH PASSKEY */
				ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(regn_resp->bt_passkey, "->"), regn_resp->bt_passkey);
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(regn_resp->bt_passkey, "->"));
					ret = CORE_FAIL;
					goto free_memory;
				}
			} else {
				CORE_LOGE(TAG, "The server returned status as not successful! stat = %d", regn_resp->status);
				ret = CORE_FAIL;
			}
		}
free_memory:
		/* clear root_value */
		json_value_free(root_value);
	} else {
		CORE_LOGE(TAG, "Could not create JSON root object");
		ret = CORE_FAIL;
	}

	return ret;
}

void print_som_registration_response(SomRegistrationResponse *regn_resp) {
	CORE_LOGI(TAG, "Parsing SOM registration response completed... The following data was received:");
	CORE_LOGI(TAG, "%s : %d", GET_VAR_NAME(regn_resp->status, "->"), regn_resp->status);
	CORE_LOGI(TAG, "%s : %d", GET_VAR_NAME(regn_resp->error.err_code, "->"), regn_resp->error.err_code);
	CORE_LOGI(TAG, "%s : %s", GET_VAR_NAME(regn_resp->error.err_msg, "->"), regn_resp->error.err_msg);
	char strftime_buf[64];
	//asctime_r(&regn_resp.reg_time, strftime_buf);
	strftime(strftime_buf, sizeof(strftime_buf), "%c", &regn_resp->reg_time);
	CORE_LOGI(TAG, "%s : %s", GET_VAR_NAME(regn_resp->reg_time, "->"), strftime_buf);
	CORE_LOGI(TAG, "%s : %s", GET_VAR_NAME(regn_resp->reg_location, "->"), regn_resp->reg_location);
	CORE_LOGI(TAG, "%s : %s", GET_VAR_NAME(regn_resp->bt_passkey, "->"), regn_resp->bt_passkey);
}
