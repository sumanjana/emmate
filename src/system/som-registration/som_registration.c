/*
 * File Name: som_registration.c
 * File Path: /emmate/src/system/som-registration/som_registration.c
 * Description:
 *
 *  Created on: 24-May-2019
 *      Author: Rohan Dey
 */

#include "som_registration.h"
#include "som_registration_helper.h"
#include "som_registration_parser.h"
#include "http_client_core.h"
#include "http_constant.h"
#include "system.h"
#include "system_urls.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#include "core_system_info.h"
#include <string.h>

#define TAG	LTAG_SOM_REG

static core_err make_som_registration_data(SomRegistrationRequest *regn_req, char **json_buf, int *json_len) {
	core_err ret = CORE_FAIL;

	ret = get_somthing_id(regn_req->som_id);

	regn_req->chip_info = get_hardware_info();

	strcpy(regn_req->core_version, CORE_DEV_VERSION_NUMBER);
	strcpy(regn_req->app_version, "");
#if CONFIG_USE_WIFI
	get_board_wifi_sta_mac(regn_req->wifi_mac);
#endif
#if CONFIG_USE_ETH
#endif
#if CONFIG_USE_GSM
#endif
#if CONFIG_USE_BLE
	get_board_bt_mac(regn_req->bt_mac);
#endif

	ret = make_som_regn_request_json(json_buf, json_len, regn_req);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "make_som_regn_request_json failed!");
	}

	return ret;
}

#if CONFIG_USE_HTTP

#define SOM_REGN_HTTP_RESPONSE_SIZE	1024
static char *http_resp_data = NULL;
static int http_resp_len = 0;
static uint8_t event_stat = false;

static void som_registration_response_handler(uint8_t http_event_stat, char* data, size_t data_len) {
	event_stat = http_event_stat;
	switch (http_event_stat) {
	case HTTP_CLIENT_EVENT_ON_DATA: {
		//CORE_LOGI(TAG, "len = %d\n%s", data_len, data);
		memcpy((http_resp_data + http_resp_len), data, data_len);
		http_resp_len += data_len;
		break;
	}
	case HTTP_CLIENT_EVENT_ON_FINISH: {
		break;
	}
	case HTTP_CLIENT_EVENT_DISCONNECTED: {
		break;
	}
	}
}

static core_err validate_som_registration_response(SomRegistrationResponse *regn_resp) {
	core_err ret = CORE_FAIL;

	if (regn_resp->status) {
		ret = CORE_OK;
	}

	return ret;
}

static core_err save_som_registration_response(SomRegistrationResponse *regn_resp) {
	core_err ret = CORE_FAIL;

	save_som_registration_time(&regn_resp->reg_time);
	save_som_registration_location(regn_resp->reg_location);
	save_som_registration_btpasskey(regn_resp->bt_passkey);

	return ret;
}

/*
 * Register a SOM with the server
 */
core_err register_som() {
	core_err ret = CORE_FAIL;

	CORE_LOGD(TAG, "Starting SOM Registration ...");

	/* Make the SOM registration request data to be posted to server */
	char *json_buf;
	int json_len;
	SomRegistrationRequest regn_req;
	memset(&regn_req, 0, sizeof(SomRegistrationRequest));
	ret = make_som_registration_data(&regn_req, &json_buf, &json_len);

	CORE_LOGI(TAG, "SOM Registration JSON:");
	CORE_LOGI(TAG, "Len = %d", json_len);
	CORE_LOGI(TAG, "JSON:");
	CORE_LOGI(TAG, "%.*s", json_len, json_buf);

	/* Initialize a SOM registration response data to be populated after receiving the response from server */
	SomRegistrationResponse regn_resp;
	memset(&regn_resp, 0, sizeof(SomRegistrationResponse));

	/* Allocate memory for the http response */
	http_resp_data = (char*) calloc(SOM_REGN_HTTP_RESPONSE_SIZE, sizeof(char));
	if (http_resp_data == NULL) {
		CORE_LOGE(TAG, "memory allocation for http_resp_data failed");
		return CORE_ERR_NO_MEM;
	}
	memset(http_resp_data, 0, SOM_REGN_HTTP_RESPONSE_SIZE);
	http_resp_len = 0;

	/* Initialize a http client data to be used for doing a http request */
	HttpClientData http_cli_data;
	bzero(&http_cli_data, sizeof(HttpClientData));
#if TESTING
	config_ota_tesing_http_cli_data(&http_cli_data);
#else
	http_cli_data.http_cnf_data.url = IQ_SOM_REGISTRATION_POST_URL;
	http_cli_data.http_cnf_data.cert_pem = NULL;  //IQ_HOST_ROOTCA;
	http_cli_data.http_cnf_data.host_server = IQ_SOM_REG_HOST;
	http_cli_data.http_cnf_data.port = IQ_HOST_PORT;
	http_cli_data.http_cnf_data.method = HTTP_CLIENT_METHOD_POST;
	http_cli_data.http_cnf_data.user_agent = "core-embed/1.0";
	http_cli_data.http_cnf_data.content_type = CONTENT_TYPE_APPLICATION_JSON;
	http_cli_data.http_cnf_data.post_data = json_buf;
	http_cli_data.http_cnf_data.post_data_len = json_len;
#endif
	http_cli_data.http_ctrl.http_recv_handler = som_registration_response_handler;

	CORE_LOGD(TAG, "Going to init http client with the following params:");
	CORE_LOGD(TAG, "url: %s", http_cli_data.http_cnf_data.url);
	CORE_LOGD(TAG, "cert_pem: %s", http_cli_data.http_cnf_data.cert_pem ? "cert is present" : "no cert");
	CORE_LOGD(TAG, "host_server: %s", http_cli_data.http_cnf_data.host_server);
	CORE_LOGD(TAG, "port: %d", http_cli_data.http_cnf_data.port);
	CORE_LOGD(TAG, "method: %d", http_cli_data.http_cnf_data.method);
	CORE_LOGD(TAG, "user_agent: %s", http_cli_data.http_cnf_data.user_agent);
	CORE_LOGD(TAG, "content_type: %s", http_cli_data.http_cnf_data.content_type);

	/* Initialize a http client instance */
	ret = init_http_client(&http_cli_data);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_http_client failed, SOM registration is not successful!");
		goto free_memory;
	}

	/* Perform the http process */
	CORE_LOGD(TAG, "Going to perform http client action");
	ret = perform_http_client_process(&http_cli_data);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "perform_http_client_process failed, SOM registration is not successful!");
		goto free_memory;
	}

	/* Wait for the http event to finish */
	while (event_stat != HTTP_CLIENT_EVENT_ON_FINISH) {
		CORE_LOGD(TAG, "waiting for http action to be finished");
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
	}

	/* Check the http status code */
	uint16_t http_stat = get_http_status_code(&http_cli_data);
	CORE_LOGD(TAG, "http status: %d", http_stat);
	if (http_stat == HTTP_OK_RES_CODE) {
		CORE_LOGD(TAG, "Resp Len = %d\n", http_resp_len);
		CORE_LOGD(TAG, "Data:\n");
		for (int i = 0; i < http_resp_len; i++)
			printf("%c", http_resp_data[i]);
		CORE_LOGD(TAG, "\n");

		/* Parse the http response data */
		CORE_LOGD(TAG, "Going to parse som registration response");
		ret = parse_som_registration_response_json(http_resp_data, &regn_resp);
		if (ret == CORE_OK) {
			/* Print the som registration response values received from the server */
			print_som_registration_response(&regn_resp);

			/* Check and validate the response data */
			CORE_LOGD(TAG, "Going to validate the parse som registration response");
			ret = validate_som_registration_response(&regn_resp);

			/* TODO: If response data is ok, then save the info into the persistent memory, else return error */
			if (ret == CORE_OK) {
				CORE_LOGD(TAG, "Going to save the following data into NVS");
				char strftime_buf[64];
				strftime(strftime_buf, sizeof(strftime_buf), "%c", &regn_resp.reg_time);
				CORE_LOGD(TAG, "reg_time: %s", strftime_buf);
				CORE_LOGD(TAG, "reg_location: %s", regn_resp.reg_location);
				CORE_LOGD(TAG, "bt_passkey: %s", regn_resp.bt_passkey);
				save_som_registration_response(&regn_resp);
			}

		}
	} else {
		CORE_LOGE(TAG, "HTTP failed with status code = %d", http_stat);
		ret = CORE_FAIL;
	}

	/* Our http tasks are done, now deinitialize the http client instance */
	CORE_LOGD(TAG, "Going to deinitialize the http client");
	if (deinit_http_client(&http_cli_data) != CORE_OK) {
		CORE_LOGE(TAG, "deinit_http_client failed");
	}
	//	ret = deinit_http_client(&http_cli_data);
	free_memory:
	/* Free the allocated http respose memory */
	CORE_LOGD(TAG, "Freeing allocated response memory");
	free(json_buf);
	free(http_resp_data);
	return ret;
}
#endif
