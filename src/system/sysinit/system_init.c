/*
 * system_init.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */
#include "core_config.h"
#include "system.h"
#include "system_init.h"
#include "system_constants.h"
#include "core_error.h"
#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#endif
#if CONFIG_USE_HMI
#include "hmi.h"
#include "system_hmi_led_notification.h"
#endif
#if CONFIG_USE_CONN
#include "conn.h"
#endif
#include "system_utils.h"
#if CONFIG_USE_SYSTIME
#include "systime.h"
#endif
#include "tcpip_adapter_core.h"
#if CONFIG_USE_SOM_REGISTRATION
#include "som_registration.h"
#endif
#if CONFIG_USE_SYS_HEARTBEAT
#include "sys_heartbeat.h"
#endif
#if CONFIG_USE_HW_IDENTIFY
#include "hw_identify.h"
#endif
#if CONFIG_USE_OTA
#include "fota_core.h"
#include "fota_core_verify.h"
#endif
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#include <string.h>

#define TAG	LTAG_SYSTEM_SYSINIT

/* Static Function Declarations */
static void get_opermode_name_by_value(uint16_t opermode, char *opername) {
	switch (opermode) {
	case STARTUP:
		strcpy(opername, MODE_NAME_STARTUP);
		break;
	case SYSTEM_CONFIG_MODE_FACTORY:
		strcpy(opername, MODE_NAME_SYSTEM_CONFIG_MODE_FACTORY);
		break;
	case NETWORK_CONNECTION_MODE:
		strcpy(opername, MODE_NAME_NETWORK_CONNECTION_MODE);
		break;
	case SETUP_SYSTIME_SNTP:
		strcpy(opername, MODE_NAME_SETUP_SYSTIME_SNTP);
		break;
	case SOM_REGISTRATION_MODE:
		strcpy(opername, MODE_NAME_SOM_REGISTRATION_MODE);
		break;
	case SYSTEM_HEARTBEAT:
		strcpy(opername, MODE_NAME_SYSTEM_HEARTBEAT);
		break;
	case START_APPLICATION:
		strcpy(opername, MODE_NAME_START_APPLICATION);
		break;
	case MAINTENANCE_MODE:
		strcpy(opername, MODE_NAME_MAINTENANCE_MODE);
		break;
	case OTA_UPDATE_MODE:
		strcpy(opername, MODE_NAME_OTA_UPDATE_MODE);
		break;
	case SYS_DO_NOTHING:
		strcpy(opername, MODE_NAME_SYS_DO_NOTHING);
		break;
	}
}

/*
 * Check if Startup Mode requires HMI. If yes then initialize HMI's LED module
 * Initialize persistent memory
 * Read saved operating mode from persistent memory if any, else start with device config mode
 * Select Operation Mode depending upon the configurations read
 * If Operation Mode = Device Configuration Mode, then select & initialize the required configuration peripheral (BLE/USB/UART)
 * If Operation Mode = OTA Update, then select & initialize network interface and required peripherals
 *
 * */
core_err init_system(SystemData *sys_data) {
	core_err ret = CORE_FAIL;
	EventBits evt_bits;

#if CONFIG_SHOW_THREAD_DEBUG
	// start task List print task
	start_sysTaskList_task();
#endif

/*
 * Initialize independed HMI Modules
 * LED HMI Modules
 * BUTTON HMI Modules
 */
#if CONFIG_USE_LED
	init_leds();
#endif
#if CONFIG_USE_BUTTON
	init_buttons();
#endif

/* Check if the system requires HMI */
#if CONFIG_HAVE_SYSTEM_HMI
	init_startup_hmi_module();
#endif	/* CONFIG_HAVE_SYSTEM_HMI */

	/* Initialize all available persistent memory for configuration and data storage */
#if CONFIG_USE_PERSISTENT
	ret = init_persistent_mem_for_config();
	CORE_LOGD(TAG, "init_persistent_mem_for_config");

	ret = init_persistent_mem_for_data_storage();

	ret = read_opermode_from_persistent_mem();
	if (ret == CORE_FAIL) {
		// TODO: Handle error
		CORE_LOGE(TAG, "Error: read_opermode_from_persistent_mem failed. %s, %d", (char*) __FILE__, __LINE__);
		return ret;
	} else {
		sys_data->oper_mode = ret;
		char oper_name[64] = { 0 };
		get_opermode_name_by_value(sys_data->oper_mode, oper_name);
		CORE_LOGD(TAG, "Found Operation Mode = (%s) from persistent memory", oper_name);
	}
#else
	sys_data->oper_mode = SYSTEM_CONFIG_MODE_FACTORY;
#endif

	/* Read and save the SOM ID */
	read_save_somthing_id();

	bool is_sys_init_done = false;
	bool is_device_config_reqd = false;

	do {
		/* Check the extracted operation mode and proceed accordingly */
		switch (sys_data->oper_mode) {
		case SYSTEM_CONFIG_MODE_FACTORY: {
#if CONFIG_USE_DEVCONFIG
			CORE_LOGI(TAG, "============================ SYSTEM_CONFIG_MODE_FACTORY ============================\r\n");

			CORE_LOGI(TAG, "Starting the Device Configuration Process ...");

			/* Initialize and start the device configuration via the selected device config peripheral */
			ret = init_and_start_device_cfg(&sys_data->dev_cfg);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Error: init_and_start_device_cfg failed %s, %d", (char*) __FILE__, __LINE__);
				return ret;
			}

			/* Here we wait for the device configuration data to be received and validated */
			evt_bits = event_group_wait_bits(get_system_evtgrp_hdl(), DEVCFG_DATA_VALIDATION_BIT, true, false,
					EventMaxDelay);
			if ((evt_bits & DEVCFG_DATA_VALIDATION_BIT) == DEVCFG_DATA_VALIDATION_BIT) {
				CORE_LOGI(TAG, "System configuration data is received successfully");
				/* SYSTEM_CONFIG_MODE_FACTORY is done, now we move to NETWORK_CONFIG_MODE */
				sys_data->oper_mode = NETWORK_CONNECTION_MODE;
				is_device_config_reqd = true;
			}
#else
			sys_data->oper_mode = NETWORK_CONNECTION_MODE;
#endif
			break;
		}
		case NETWORK_CONNECTION_MODE: {
#if CONFIG_USE_CONN
			CORE_LOGI(TAG, "============================== NETWORK_CONNECTION_MODE ==============================\r\n");

			/* Read the saved config from persistent memory */
			ret = read_devconfig_from_persistent_mem(&sys_data->dev_cfg);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Error: read_devconfig_from_persistent_mem failed %s, %d", (char*) __FILE__, __LINE__);
				return ret;
			}
#if 1
			/////////////////////////////////////////////////////////////////////
			WiFiCredentials *cred = get_wifi_credentials();
			if (cred != NULL) {
				CORE_LOGI(TAG, "Printing stored WiFi Credentials");
				CORE_LOGI(TAG, "NUM CFG = %d", cred->config_count);
				for (int i = 0; i < cred->config_count; i++) {
					CORE_LOGI(TAG, "[%d] SSID = %s", i, cred->cfg[i].wifi_ssid);
					CORE_LOGI(TAG, "[%d] PASS = %s", i, cred->cfg[i].wifi_pwd);
				}
			} else {
				CORE_LOGI(TAG, "No saved WiFi Credentials");
			}
			/////////////////////////////////////////////////////////////////////
#endif
			/* Validate that all configurations required in Level-1 are set */
			bool is_config_ok = false;
			VALIDATE_DEVICE_CONFIG(sys_data->dev_cfg.dev_conf_checklist, DEVCFG_VALIDATION_MASK, is_config_ok);

			/* Check if configurations are valid */
			if (is_config_ok) {
				/* Start network connection thread */
				ret = start_network_connection(&sys_data->dev_cfg);

				/* We wait only if device config has been run before coming here */
				if (is_device_config_reqd) {
					is_device_config_reqd = false;
					/* Network connection is successful. Acknowledge the user via device config peripheral */
					/* 4. Acknowledge the user via device config peripheral */

					/* Wait here until the Device Configuration is completed */
					evt_bits = event_group_wait_bits(get_system_evtgrp_hdl(), DEVCFG_COMPLETE_BIT, true, false,
							EventMaxDelay);

					if ((evt_bits & DEVCFG_COMPLETE_BIT) == DEVCFG_COMPLETE_BIT) {
						/* If event_group_wait_bits () provides the same bit, which we are looking for.
						 * then continue with remaining bit dependent process or else execute other functionality. */
						CORE_LOGI(TAG, "Device configuration is completed successfully\r\n");
					} else {
						/* If event_group_wait_bits () provides the same bit, which we are looking for.
						 * then continue with remaining bit dependent process or else execute other functionality. */
					}
				}

				/* NETWORK_CONNECTION_MODE is done */
				/* And save NETWORK_CONNECTION_MODE to persistent memory so that the device starts running network connection after rebooting */
				ret = write_opermode_to_persistent_mem(sys_data->oper_mode);
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "write_opermode_to_persistent_mem failed");
					return ret;
				}

//#if CONFIG_HAVE_SNTP_SYSTIME
//				/* Now we move to SETUP_SYSTIME_SNTP */
//				sys_data->oper_mode = SETUP_SYSTIME_SNTP;
//#elif CONFIG_USE_SOM_REGISTRATION
//				sys_data->oper_mode = SOM_REGISTRATION_MODE;
//#elif (CONFIG_USE_HW_IDENTIFY) || (CONFIG_USE_SYS_HEARTBEAT)
//				if (!is_hw_somid_present()) {
//					sys_data->oper_mode = HARDWARE_IDENTIFICATION;
//				} else {
//					sys_data->oper_mode = SYSTEM_HEARTBEAT;
//				}
//#else
//				sys_data->oper_mode = START_APPLICATION;
//#endif
				sys_data->oper_mode = VERIFY_RUNNING_FIRMWARE;
			} else {
				CORE_LOGI(TAG,
						"Device configurations are invalid! Changing operation mode to SYSTEM_CONFIG_MODE_FACTORY");
				sys_data->oper_mode = SYSTEM_CONFIG_MODE_FACTORY;
			}
#else
			sys_data->oper_mode = VERIFY_RUNNING_FIRMWARE;
#endif
			break;
		}

		case VERIFY_RUNNING_FIRMWARE: {
#if CONFIG_USE_OTA
			CORE_LOGI(TAG, "============================== VERIFY_RUNNING_FIRMWARE ==============================\r\n");
			/* OTA Image Verification Process */
			/*
			 * TODO: Please Enable app rollback support from make menuconfig
			 * for execute Application Rollback Functionality (for ESP32)
			 */
			verify_running_firmware();
#endif

#if CONFIG_HAVE_SNTP_SYSTIME
				/* Now we move to SETUP_SYSTIME_SNTP */
				sys_data->oper_mode = SETUP_SYSTIME_SNTP;
#elif CONFIG_USE_SOM_REGISTRATION
				sys_data->oper_mode = SOM_REGISTRATION_MODE;
#elif (CONFIG_USE_HW_IDENTIFY) || (CONFIG_USE_SYS_HEARTBEAT)
				if (!is_hw_somthing_id_present()) {
					sys_data->oper_mode = HARDWARE_IDENTIFICATION;
				} else {
					sys_data->oper_mode = SYSTEM_HEARTBEAT;
				}
#else
				sys_data->oper_mode = START_APPLICATION;
#endif
			break;
		}

#if CONFIG_HAVE_SNTP_SYSTIME
		case SETUP_SYSTIME_SNTP: {
			CORE_LOGI(TAG, "============================== SETUP_SYSTIME_SNTP ==============================\r\n");
			start_system_time_module();

#if CONFIG_USE_SOM_REGISTRATION
			/* now we move to SOM_REGISTRATION_MODE */
			sys_data->oper_mode = SOM_REGISTRATION_MODE;
#elif (CONFIG_USE_HW_IDENTIFY) || (CONFIG_USE_SYS_HEARTBEAT)
			if (!is_hw_somthing_id_present()) {
				CORE_LOGD(TAG, "No Hard-Wired SOMTHING ID present, starting Hardware Identification module");
				sys_data->oper_mode = HARDWARE_IDENTIFICATION;
			} else {
				CORE_LOGD(TAG, " Hard-Wired SOMTHING ID present, starting System Heartbeat module");
				sys_data->oper_mode = SYSTEM_HEARTBEAT;
			}
#else
			sys_data->oper_mode = START_APPLICATION;
//			TaskDelay(DELAY_3_SEC / TICK_RATE_TO_MS);
#endif
			break;
		}
#endif

#if CONFIG_USE_SOM_REGISTRATION
			case SOM_REGISTRATION_MODE: {
				CORE_LOGI(TAG, "============================== SOM_REGISTRATION_MODE ==============================\r\n");
				/* Check for Internet availability */
				/* wait for network to connect */
				evt_bits = event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);

				static uint8_t som_regn_retry = 1;
				/* Do the SOM registration */
				ret = register_som();

				if (ret == CORE_OK) {
					/* SOM Registration is done, now start the application
					 * When an application is using the SOM Registration module, it should not need Sys Heartbeat and OTA module
					 * */
					sys_data->oper_mode = START_APPLICATION;
				} else {
					// TODO: implement retry logic
					if (++som_regn_retry > 5) {
						CORE_LOGI(TAG, "Retrying SOM registration for %d time ...", som_regn_retry);
						/* SOM Registration failed */
						sys_data->oper_mode = SYS_DO_NOTHING;
						TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
					}
				}
				break;
			}
#endif

		case HARDWARE_IDENTIFICATION: {
			CORE_LOGI(TAG, "============================== HARDWARE_IDENTIFICATION ==============================\r\n");
#if CONFIG_USE_HW_IDENTIFY
			start_hardware_identification();
#else
			/* The sys heartbeat module should be waiting on this bit. It is actually set inside the hardware identification
			 * thread. Since the hw identification functionality is not enabled, we set the START_SYSHEARTBEAT_BIT bit here */
			event_group_set_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT);
#endif
			sys_data->oper_mode = SYSTEM_HEARTBEAT;
			break;
		}

		case SYSTEM_HEARTBEAT: {
			CORE_LOGI(TAG, "============================== SYSTEM_HEARTBEAT ==============================\r\n");
			if (is_hw_somthing_id_present()) {
				/* The sys heartbeat module should be waiting on this bit. It is actually set inside the hardware identification
				 * thread. Since the hw identification functionality is not enabled, we set the START_SYSHEARTBEAT_BIT bit here */
				event_group_set_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT);
			}
#if CONFIG_USE_SYS_HEARTBEAT
			/* Start the System Heartbeat Process */
			start_system_heartbeat();
#endif
			/* Initialization complete, now start the application */
			sys_data->oper_mode = START_APPLICATION;
			break;
		}

		case START_APPLICATION: {
			CORE_LOGI(TAG, "============================== START_APPLICATION ==============================\r\n");
			/* If control reaches here, it means everything is OK, set set the return value to CORE_OK */
			ret = CORE_OK;
			/* Start the application */
			is_sys_init_done = true;
			sys_data->oper_mode = SYS_DO_NOTHING;
			break;
		}
		case MAINTENANCE_MODE: {
			CORE_LOGI(TAG, "============================== MAINTENANCE_MODE ==============================\r\n");
			break;
		}
		case SYS_DO_NOTHING: {
			/* The control should never come here unless an fatal error occurs */
			CORE_LOGE(TAG, "\n\n**************************** SYS_DO_NOTHING ****************************\n\n");
			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			break;
		}
		default: {
			break;
		}
		} // switch (sys_data->oper_mode)
	} while (!is_sys_init_done); // Loop until the system is not completely initialized

	return ret;
}

/*
 * Perform a Power On Self Test
 *
 *
 */

core_err perform_post() {

	// TODO: write POST fucntionality

	return CORE_OK;
}
