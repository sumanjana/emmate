/*
 * File Name: board_ids.h
 * File Path: /emmate/src/boards/board_ids.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#ifndef BOARD_IDS_H_
#define BOARD_IDS_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#define NO_HW_SOM_ID	"NO-HW-SOM-ID"		/**< MACRO to denote that there is no SOM ID present on the SOM */

/*
 * @brief		Reads the SOM ID from Silicon Number Chip and stores it for future use
 * 				The SOM ID can be acquired by calling get_som_id() after calling this function
 *
 * @return
 * 				- CORE_OK: succeed
 * 				- CORE_FAIL on fail
 * */
core_err read_save_somthing_id();

/*
 * @brief		Get the SOM ID. read_save_som_id() must be called before calling this function
 *
 * @param[out]	somthing_id[SOM_ID_LEN]	SOM ID returned as an out parameter
 *
 * @return
 * 				- CORE_OK: succeed
 * 				- CORE_FAIL on fail
 * */
core_err get_somthing_id(char *somthing_id);

/*
 * @brief		Set the Virtual SOM ID. This SOM ID is received from the server.
 * 				The caller should set the virtual SOM ID only if there is no hard-wired SOM ID available on the board
 * 				After calling this function, the same SOM ID can be read by calling get_som_id()
 *
 * @param[in]	v_somthing_id	Virtual SOM ID to be set as SOM ID in the module
 *
 * @return
 * 				- CORE_OK: succeed
 * 				- CORE_FAIL on fail
 * */
core_err set_virtual_somthing_id(char *v_somthing_id);

/*
 * @brief		Checks whether a Hardwired SOM ID is present in the board
 *
 * @return
 * 				- true: if Hardwired SOM ID is present
 * 				- false: if Hardwired SOM ID is not present
 * */
bool is_hw_somthing_id_present();

/*
 * @brief		Get the Wi-Fi STA interface's MAC address
 *
 * @param[out]	wifi_sta_mac[6]	WiFi STA interface's MAC address (6 bytes) returned as an out parameter
 *
 * @return
 * 				- CORE_OK success
 * 				- CORE_FAIL on fail
 * 				- CORE_ERR_NOT_FOUND if NO_SOM_ID is found
 * */
core_err get_board_wifi_sta_mac(uint8_t *wifi_sta_mac);

/*
 * @brief		Get the Bluetooth interface's MAC address
 *
 * @param[out]	bt_mac[6]	Bluetooth interface's MAC address (6 bytes) returned as an out parameter
 *
 * @return
 * 				- CORE_OK: succeed
 * 				- CORE_FAIL on fail
 * */
core_err get_board_bt_mac(uint8_t *bt_mac);

/*
 * @brief		Get the Ethernet interface's MAC address
 *
 * @param[out]	eth_mac[6]	Ethernet interface's MAC address (6 bytes) returned as an out parameter
 *
 * @return
 * 				- CORE_OK: succeed
 * 				- CORE_FAIL on fail
 * */
core_err get_board_eth_mac(uint8_t *eth_mac);

/*
 * @brief		Get the GSM Module's IMEI number
 *
 * @param[out]	gsm_imei	GSM Module's IMEI number returned as an out parameter
 *
 * @return
 * 				- CORE_OK: succeed
 * 				- CORE_FAIL on fail
 * */
core_err get_board_gsm_imei(long *gsm_imei);

#endif /* BOARD_IDS_H_ */
