/*
 * File Name: uart_helper.c
 *
 * Description:
 *
 *  Created on: 15-Oct-2019
 *      Author: Noyel Seth
 */

#include "uart_helper_api.h"
#include "core_utils.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG 	LTAG_UART

#define DEFAULT_DATA_TRANSCTION_RATE	100

/**
 *
 */
core_err init_uart_hw_flow_driver(uart_core_port_t uart_port, int baud_rate, int tx_pin, int rx_pin, int rts_pin,
		int cts_pin, uart_core_hw_flowcontrol_t flow_ctrl, uart_data_packet_t data_packet_type,
		uart_core_transactor_cb uart_transactor_cb_handle) {

	core_err ret = CORE_FAIL;

	uint8_t word_len_bits = (data_packet_type & 0xF00) >> 8;
	uint8_t parity_bits = (data_packet_type & 0x0F0) >> 4;
	uint8_t stop_bits = (data_packet_type & 0x00F) >> 0;

	/* Do all necessary initializations here */
	UART_CONFIGURATION uart_config;
	uart_config.baud_rate = baud_rate;

	// configure UART word-length bit
	if (word_len_bits == UART_WORD_LENGTH_5_BITS) {
		CORE_LOGD(TAG, "Uart Word Length %s", GET_VAR_NAME(UART_WORD_LENGTH_5_BITS, NULL));
		uart_config.data_bits = UART_CORE_DATA_5_BITS;

	} else if (word_len_bits == UART_WORD_LENGTH_6_BITS) {
		CORE_LOGD(TAG, "UART Word Length %s", GET_VAR_NAME(UART_WORD_LENGTH_6_BITS, NULL));
		uart_config.data_bits = UART_CORE_DATA_6_BITS;

	} else if (word_len_bits == UART_WORD_LENGTH_7_BITS) {
		CORE_LOGD(TAG, "UART Word Length %s", GET_VAR_NAME(UART_WORD_LENGTH_7_BITS, NULL));
		uart_config.data_bits = UART_CORE_DATA_7_BITS;

	} else if (word_len_bits == UART_WORD_LENGTH_8_BITS) {
		CORE_LOGD(TAG, "UART Word Length %s", GET_VAR_NAME(UART_WORD_LENGTH_8_BITS, NULL));
		uart_config.data_bits = UART_CORE_DATA_8_BITS;
	}

	// configure UART Perity bit
	if (parity_bits == UART_PARITY_DISABLE_BITS) {
		CORE_LOGD(TAG, "UART Parity Length %s", GET_VAR_NAME(UART_PARITY_DISABLE_BITS, NULL));
		uart_config.parity = UART_CORE_PARITY_DISABLE;

	} else if (parity_bits == UART_PARITY_EVEN_BITS) {
		CORE_LOGD(TAG, "UART Parity Length %s", GET_VAR_NAME(UART_PARITY_EVEN_BITS, NULL));
		uart_config.parity = UART_CORE_PARITY_EVEN;

	} else if (parity_bits == UART_PARITY_ODD_BITS) {
		CORE_LOGD(TAG, "UART Parity Bit %s", GET_VAR_NAME(UART_PARITY_ODD_BITS, NULL));
		uart_config.parity = UART_CORE_PARITY_ODD;
	}

	// configure UART stop bit
	if (stop_bits == UART_1_STOP_BITS) {
		CORE_LOGD(TAG, "UART Stop Bit %s", GET_VAR_NAME(UART_1_STOP_BITS, NULL));
		uart_config.stop_bits = UART_CORE_STOP_BITS_1;

	} else if (stop_bits == UART_2_STOP_BITS) {
		CORE_LOGD(TAG, "UART Stop Bit %s", GET_VAR_NAME(UART_2_STOP_BITS, NULL));
		uart_config.stop_bits = UART_CORE_STOP_BITS_2;
	}

	// configure UART Hardware FlowControl as disable
	uart_config.flow_ctrl = flow_ctrl;

	CORE_LOGD(TAG, "hw-flow = %d", uart_config.flow_ctrl);
	CORE_LOGD(TAG, "word = %d", uart_config.data_bits);
	CORE_LOGD(TAG, "stop = %d", uart_config.stop_bits);
	CORE_LOGD(TAG, "parity = %d", uart_config.parity);

	// configure UART Pins Tx, Rx, RTS, CTS
	UART_CORE_PORT_PIN uart_pin;
	uart_pin.cts_io_num = cts_pin; //UART_CORE_PIN_NO_CHANGE;
	uart_pin.rts_io_num = rts_pin; //UART_CORE_PIN_NO_CHANGE;
	uart_pin.rx_io_num = rx_pin;
	uart_pin.tx_io_num = tx_pin;

	CORE_LOGD(TAG, "rts = %d", uart_pin.rts_io_num);
	CORE_LOGD(TAG, "cts = %d", uart_pin.cts_io_num);
	CORE_LOGD(TAG, "tx = %d", uart_pin.tx_io_num);
	CORE_LOGD(TAG, "rx = %d", uart_pin.rx_io_num);

	// configure UART Transaction data
	UART_CORE_TRANSACTION_END_WITH_DATA uart_transaction_end_with_data;
	uart_transaction_end_with_data.num_bytes = DEFAULT_DATA_TRANSCTION_RATE;

	ret = init_uart_core_driver(uart_port, &uart_config, &uart_pin, UART_READ_TRANSACT_NUM_BYTES,
			&uart_transaction_end_with_data, uart_transactor_cb_handle);

	return ret;
}

/**
 *
 */
core_err init_uart_driver(uart_core_port_t uart_port, int baud_rate, int tx_pin, int rx_pin,
		uart_data_packet_t comm_type, uart_core_transactor_cb uart_transactor_cb_handle) {

	core_err ret = CORE_FAIL;

	ret = init_uart_hw_flow_driver(uart_port, baud_rate, tx_pin, rx_pin, UART_CORE_PIN_NO_CHANGE,
			UART_CORE_PIN_NO_CHANGE, UART_CORE_HW_FLOWCTRL_DISABLE, comm_type, uart_transactor_cb_handle);

	return ret;
}

/**
 *
 */
core_err deinit_uart_driver(uart_core_port_t uart_port) {
	core_err ret = CORE_FAIL;
	ret = deinit_uart_core_driver(uart_port);
	return ret;
}

/**
 *
 */
int uart_driver_write_bytes(uart_core_port_t uart_num, const char* src, size_t size) {
	return uart_core_driver_write_bytes(uart_num, src, size);
}

/**
 *
 */
core_err uart_driver_update_transaction_bytes_size(uart_core_port_t uart_num, uint32_t num_bytes) {
	return uart_core_driver_update_transaction_bytes_size(uart_num, num_bytes);
}

