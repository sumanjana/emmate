/*
 * File Name: uart_helper.h
 *
 * Description:
 *
 *  Created on: 15-Oct-2019
 *      Author: Noyel Seth
 */

#ifndef UART_HELPER_API_H_
#define UART_HELPER_API_H_

#include "uart_core.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Uart Communicate Packet Type Data*/
typedef uint16_t uart_data_packet_t;

#define UART_WORD_LENGTH_5_BITS 0x5   /*!< word length: 5bits*/
#define UART_WORD_LENGTH_6_BITS 0x6   /*!< word length: 6bits*/
#define UART_WORD_LENGTH_7_BITS 0x7   /*!< word length: 7bits*/
#define UART_WORD_LENGTH_8_BITS 0x8   /*!< word length: 8bits*/

#define UART_1_STOP_BITS 0x1  /*!< stop bit: 1bit*/
#define UART_2_STOP_BITS 0x2  /*!< stop bit: 2bits*/

#define UART_PARITY_DISABLE_BITS  	0x0	/*!< Disable UART parity*/
#define UART_PARITY_ODD_BITS   		0x1	/*!< Enable UART odd parity*/
#define UART_PARITY_EVEN_BITS  		0x2	/*!< Enable UART even parity*/

/* UART Communicate Packet Type*/
#define UART_5N1 0x501	/*!< UART Communicate Packet Type word length: 5bits, parity bit: None, stop bit: 1bit*/
#define UART_6N1 0x601	/*!< UART Communicate Packet Type word length: 6bits, parity bit: None, stop bit: 1bit*/
#define UART_7N1 0x701	/*!< UART Communicate Packet Type word length: 7bits, parity bit: None, stop bit: 1bit*/
#define UART_8N1 0x801	/*!< UART Communicate Packet Type word length: 8bits, parity bit: None, stop bit: 1bit*/

#define UART_5N2 0x502	/*!< UART Communicate Packet Type word length: 5bits, parity bit: None, stop bit: 2bit*/
#define UART_6N2 0x602	/*!< UART Communicate Packet Type word length: 6bits, parity bit: None, stop bit: 2bit*/
#define UART_7N2 0x702	/*!< UART Communicate Packet Type word length: 7bits, parity bit: None, stop bit: 2bit*/
#define UART_8N2 0x802	/*!< UART Communicate Packet Type word length: 8bits, parity bit: None, stop bit: 2bit*/

#define UART_5E1 0x521	/*!< UART Communicate Packet Type word length: 5bits, parity bit: Even, stop bit: 1bit*/
#define UART_6E1 0x621	/*!< UART Communicate Packet Type word length: 6bits, parity bit: Even, stop bit: 1bit*/
#define UART_7E1 0x721	/*!< UART Communicate Packet Type word length: 7bits, parity bit: Even, stop bit: 1bit*/
#define UART_8E1 0x821	/*!< UART Communicate Packet Type word length: 8bits, parity bit: Even, stop bit: 1bit*/

#define UART_5E2 0x522	/*!< UART Communicate Packet Type word length: 5bits, parity bit: Even, stop bit: 2bit*/
#define UART_6E2 0x622	/*!< UART Communicate Packet Type word length: 6bits, parity bit: Even, stop bit: 2bit*/
#define UART_7E2 0x722	/*!< UART Communicate Packet Type word length: 7bits, parity bit: Even, stop bit: 2bit*/
#define UART_8E2 0x822	/*!< UART Communicate Packet Type word length: 8bits, parity bit: Even, stop bit: 2bit*/

#define UART_5O1 0x511	/*!< UART Communicate Packet Type word length: 5bits, parity bit: Odd, stop bit: 1bit*/
#define UART_6O1 0x611	/*!< UART Communicate Packet Type word length: 6bits, parity bit: Odd, stop bit: 1bit*/
#define UART_7O1 0x711	/*!< UART Communicate Packet Type word length: 7bits, parity bit: Odd, stop bit: 1bit*/
#define UART_8O1 0x811	/*!< UART Communicate Packet Type word length: 8bits, parity bit: Odd, stop bit: 1bit*/

#define UART_5O2 0x512	/*!< UART Communicate Packet Type word length: 5bits, parity bit: Odd, stop bit: 2bit*/
#define UART_6O2 0x612	/*!< UART Communicate Packet Type word length: 6bits, parity bit: Odd, stop bit: 2bit*/
#define UART_7O2 0x712	/*!< UART Communicate Packet Type word length: 7bits, parity bit: Odd, stop bit: 2bit*/
#define UART_8O2 0x812	/*!< UART Communicate Packet Type word length: 8bits, parity bit: Odd, stop bit: 2bit*/

/**
 *	@brief Initialize the UART Module with HardWare FlowControl.
 *
 *	@note
 *		By default UART read transaction bytes size 100.
 *		To change the bytes size use "uart_driver_update_transaction_bytes_size" api.
 *
 *	@param uart_port	UART number from uart_core_port_t
 *
 *	@param baud_rate	UART baud rate
 *
 *	@param tx_pin		UART TX pin GPIO number.
 *
 *	@param rx_pin		UART RX pin GPIO number.
 *
 *	@param rts_pin		UART RTS pin GPIO number.
 *
 *	@param cts_pin		UART CTS pin GPIO number.
 *
 *	@param flow_ctrl	UART HW flow control mode (cts/rts)
 *
 *	@param comm_type	UART Data communication packet type like: UART_8N1, UART_8E1, UART_5O2,
 *
 *	@param uart_transactor_cb_handle	UART received data CallBack
 *
 *	@return
 *      - CORE_OK on success
 *      - CORE_FAIL  Parameter error
 *      - CORE_ERR_NO_MEM Failed to allocate memory for Rx Buffer
 *
 */
core_err init_uart_hw_flow_driver(uart_core_port_t uart_port, int baud_rate, int tx_pin, int rx_pin,
		int rts_pin, int cts_pin, uart_core_hw_flowcontrol_t flow_ctrl, uart_data_packet_t data_packet_type,
		uart_core_transactor_cb uart_transactor_cb_handle);

/**
 *	@brief Initialize the UART Module.
 *
 *	@note
 *		By default UART read transaction bytes size 100.
 *		To change the bytes size use "uart_driver_update_transaction_bytes_size" api.
 *
 *	@param uart_port	UART number from uart_core_port_t
 *
 *	@param baud_rate	UART baud rate
 *
 *	@param tx_pin		UART TX pin GPIO number.
 *
 *	@param rx_pin		UART RX pin GPIO number.
 *
 *	@param comm_type	UART Data communication packet type like: UART_8N1, UART_8E1, UART_5O2,
 *
 *	@param uart_transactor_cb_handle	UART received data CallBack
 *
 *	@return
 *      - CORE_OK on success
 *      - CORE_FAIL  Parameter error
 *      - CORE_ERR_NO_MEM Failed to allocate memory for Rx Buffer
 *
 */
core_err init_uart_driver(uart_core_port_t uart_port, int baud_rate, int tx_pin, int rx_pin,
		uart_data_packet_t data_packet_type, uart_core_transactor_cb uart_transactor_cb_handle);

/**
 *	@brief De-Initialize the UART Module.
 *
 *	@param uart_port  UART number
 *
 *	@return
 *      - CORE_OK 		on success
 *      - CORE_FAIL  	Parameter error
 *
 */
core_err deinit_uart_driver(uart_core_port_t uart_port);

/**
 * @brief Send the number of bytes data to the UART port from a given buffer
 *
 * @param uart_num UART_CORE_NUM_0, UART_CORE_NUM_1 or UART_CORE_NUM_2
 * @param src   data buffer address
 * @param size  data length to send
 *
 * @return
 *     - (-1) Parameter error
 *     - OTHERS (>=0) The number of bytes pushed via Tx
 */
int uart_driver_write_bytes(uart_core_port_t uart_num, const char* src, size_t size);

/**
 * @brief Update UART read transaction bytes size
 *
 * @note
 * 		This is a blocking function.
 * 		If the UART driver is in the process
 * 		then it will wait until the process complete & update the transaction byte length.
 *
 * @param uart_num 		UART_CORE_NUM_0, UART_CORE_NUM_1 or UART_CORE_NUM_2
 * @param num_bytes   	number of bytes to update ( 0 <  num_bytes < RD_BUF_SIZE)
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL 	Parameter error
 * 		- CORE_ERR_INVALID_ARG invalid transaction bytes size
 */
core_err uart_driver_update_transaction_bytes_size(uart_core_port_t uart_num, uint32_t num_bytes);

#ifdef __cplusplus
}
#endif

#endif /* UART_HELPER_API_H_ */
