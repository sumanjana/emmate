/*
 * File Name: uart_core.c
 * File Path: /emmate/src/peripherals/uart/uart_core.c
 * Description:
 *
 *  Created on: 06-Aug-2019
 *      Author: Noyel Seth
 */

#include "uart_core.h"
#include "module_thread_priorities.h"
#include "core_constant.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#define TAG 	LTAG_UART

#define RD_BUF_SIZE		CONFIG_UART_READ_BUFF_SIZE
#define WR_BUF_SIZE		CONFIG_UART_WRITE_BUFF_SIZE

#define UART_EVENT_QUEUE_SIZE	100

//static gll_t *uart_conn_list = NULL;

UART_CORE_CONFIGURATION_DATA uart_core_config_data[UART_CORE_NUM_MAX] = { 0 };

/************************************************************************************/

/**
 *
 */
static void uart_core_task(void *pvParameters) {
	uart_core_port_t uart_port = (uart_core_port_t) pvParameters;
	UART_CORE_CONFIGURATION_DATA *uart_core_data = &uart_core_config_data[uart_port];
	while (1) {
		char *temp_buff = NULL;

		SemaphoreTake(uart_core_data->uart_core_transact_mutex_handle, THREADING_MAX_DELAY / TICK_RATE_TO_MS);

		if (uart_core_data->uart_read_transact_type == UART_READ_TRANSACT_NUM_BYTES) {
			if (uart_core_data->uart_read_transaction_end_with_data.num_bytes != 0) {
				temp_buff = (char*) calloc(uart_core_data->uart_read_transaction_end_with_data.num_bytes + 1,
						sizeof(char));
				if (uart_core_data->uart_byte_queue_handle->size
						>= uart_core_data->uart_read_transaction_end_with_data.num_bytes) {
					//printf("### %d\n", uart_core_data->uart_byte_queue_handle->size);
					int count = 0;
					for (int i = 0; i < uart_core_data->uart_read_transaction_end_with_data.num_bytes; i++) {
						temp_buff[i] = (char*) gll_pop(uart_core_data->uart_byte_queue_handle);
						count++;
					}
					uart_core_data->uart_core_transactor_cb_handle(uart_port, temp_buff, count);
				} else if (uart_core_data->uart_byte_queue_handle->size != 0) {
					//printf("####### %d\n", uart_core_data->uart_byte_queue_handle->size);
					int count = 0;
					int queue_remain_len = uart_core_data->uart_byte_queue_handle->size;
					for (int i = 0; i < queue_remain_len; i++) {
						temp_buff[i] = (char*) gll_pop(uart_core_data->uart_byte_queue_handle);
						count++;
					}
					uart_core_data->uart_core_transactor_cb_handle(uart_port, temp_buff, count);
				}

			} else {
//				if (uart_core_data->uart_byte_queue_handle->size != 0) {
//					temp_buff = (char*) calloc(RD_BUF_SIZE + 1, sizeof(char));
//					int count = 0;
//					for (int i = 0; i < RD_BUF_SIZE; i++) {
//						temp_buff[i] = (char*) gll_pop(uart_core_data->uart_byte_queue_handle);
//						count++;
//					}
//					uart_core_data->uart_core_transactor_cb_handle(uart_port, temp_buff, count);
//				}
			}
			free(temp_buff);

		}

		SemaphoreGive(uart_core_data->uart_core_transact_mutex_handle);

		TaskDelay(DELAY_10_MSEC/TICK_RATE_TO_MS);

		if (uart_core_data->is_uart_evnt_thread_handle == true) {
			break;
		}
	}
	CORE_LOGD(TAG, "exit from uart[%d] core task", uart_port);
	uart_core_data->is_uart_task_thread_handle = false;
	TaskDelete(NULL);
}

/**
 *
 */
static void uart_event_task(void *pvParameters) {

	uart_core_port_t uart_port = (uart_core_port_t) pvParameters;
	UART_CORE_CONFIGURATION_DATA *uart_core_data = &uart_core_config_data[uart_port];
	uart_core_event_t event;
//	size_t buffered_size;
	bzero(uart_core_data->uart_read_buff, RD_BUF_SIZE);
//	char temp_buff[RD_BUF_SIZE];
	while (1) {
		//Waiting for UART event.
		if (QueueReceive(uart_core_data->uart_event_queue_handle, (void * ) &event, DELAY_1_SEC /TICK_RATE_TO_MS)) {
			bzero(uart_core_data->uart_read_buff, RD_BUF_SIZE);
			CORE_LOGD(TAG, "uart[%d] event:", uart_port);
			switch (event.type) {
			//Event of UART receving data
			/*We'd better handler data event fast, there would be much more data events than
			 other types of events. If we take too much time on data event, the queue might
			 be full.*/
			case UART_CORE_DATA: {
//				static size_t count = 0;
				CORE_LOGD(TAG, "[UART DATA]: %d", event.size);
//				count += event.size;
				uart_core_read_bytes(uart_port, uart_core_data->uart_read_buff, event.size,
						DELAY_100_MSEC /TICK_RATE_TO_MS);
				//printf("%s\n", uart_core_data->uart_read_buff);
				if (uart_core_data->uart_read_transact_type == UART_READ_TRANSACT_NUM_BYTES) {
					for (int i = 0; i < event.size; i++) {
						char *ch = (char*) uart_core_data->uart_read_buff[i];
						if (gll_pushBack(uart_core_data->uart_byte_queue_handle, ch) != C_OK) {
							CORE_LOGE(TAG, "Failed to enqueue byte");
						}
					}
				}
				//printf("count = %d\n", count);
				break;
			}

				//Event of HW FIFO overflow detected
			case UART_CORE_FIFO_OVF: {
				CORE_LOGD(TAG, "hw fifo overflow");
				// If fifo overflow happened, you should consider adding flow control for your application.
				// The ISR has already reset the rx FIFO,
				// As an example, we directly flush the rx buffer here in order to read more data.
				uart_core_flush_input(uart_port);
				QueueReset(uart_core_data->uart_event_queue_handle);
				break;
			}

				//Event of UART ring buffer full
			case UART_CORE_BUFFER_FULL: {
				CORE_LOGD(TAG, "ring buffer full");
				// If buffer full happened, you should consider encreasing your buffer size
				// As an example, we directly flush the rx buffer here in order to read more data.
				uart_core_flush_input(uart_port);
				QueueReset(uart_core_data->uart_event_queue_handle);
				break;
			}

				//Event of UART RX break detected
			case UART_CORE_BREAK: {
				CORE_LOGD(TAG, "uart rx break");
				break;
			}

				//Event of UART parity check error
			case UART_CORE_PARITY_ERR: {
				CORE_LOGD(TAG, "uart parity error");
				break;
			}

				//Event of UART frame error
			case UART_CORE_FRAME_ERR: {
				CORE_LOGD(TAG, "uart frame error");
				break;
			}

				//UART_PATTERN_DET
			case UART_CORE_PATTERN_DET: {
//				uart_get_buffered_data_len(EX_UART_NUM, &buffered_size);
//				int pos = uart_pattern_pop_pos(EX_UART_NUM);
//				ESP_LOGI(TAG, "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos, buffered_size);
//				if (pos == -1) {
//					// There used to be a UART_PATTERN_DET event, but the pattern position queue is full so that it can not
//					// record the position. We should set a larger queue size.
//					// As an example, we directly flush the rx buffer here.
//					uart_flush_input(EX_UART_NUM);
//				} else {
//					uart_read_bytes(EX_UART_NUM, dtmp, pos, 100 / portTICK_PERIOD_MS);
//					uint8_t pat[PATTERN_CHR_NUM + 1];
//					memset(pat, 0, sizeof(pat));
//					uart_read_bytes(EX_UART_NUM, pat, PATTERN_CHR_NUM, 100 / portTICK_PERIOD_MS);
//					ESP_LOGI(TAG, "read data: %s", dtmp);
//					ESP_LOGI(TAG, "read pat : %s", pat);
//				}
				break;
			}

				//Others
			default: {
				ESP_LOGI(TAG, "uart event type: %d", event.type);
				break;
			}
			}
		}

		if (uart_core_data->is_uart_evnt_thread_handle == true) {
			break;
		}
	}
	CORE_LOGD(TAG, "exit from uart[%d] event task", uart_port);
	uart_core_data->is_uart_evnt_thread_handle = false;
	vTaskDelete(NULL);
}

/******************************************************************************************************************/

/**
 *
 */
core_err init_uart_core_driver(uart_core_port_t uart_port, UART_CONFIGURATION *uart_config,
		UART_CORE_PORT_PIN *uart_pin, UART_CORE_TRANSACT_TYPE uart_read_transact_type,
		UART_CORE_TRANSACTION_END_WITH_DATA* uart_read_transaction_end_with_data,
		uart_core_transactor_cb uart_core_transactor_cb_handle) {
	core_err ret = CORE_FAIL;

	uart_core_config_t uart_config_data;
	bzero(&uart_config_data, sizeof(uart_core_config_t));

	if (uart_read_transaction_end_with_data == NULL) {
		CORE_LOGE(TAG, "uart_read_transaction_end_with_data != NULL");
		return CORE_FAIL;
	}

	if (uart_read_transact_type == UART_READ_TRANSACT_NUM_BYTES) {
		if (uart_read_transaction_end_with_data->num_bytes > RD_BUF_SIZE) {
			CORE_LOGE(TAG, "uart_read_transaction_end_with_data->num_bytes > RD_BUF_SIZE");
			return CORE_FAIL;
		} else if (uart_read_transaction_end_with_data->num_bytes <= 0) {
			CORE_LOGE(TAG, "uart_read_transaction_end_with_data->num_bytes <= 0");
			return CORE_FAIL;
		}
	}
//	else if (uart_read_transact_type == UART_READ_TRANSACT_CHAR_TERMINATOR) {
//		if (uart_read_transaction_end_with_data->terminator == 0) {
//			CORE_LOGE(TAG, "uart_read_transaction_end_with_data->terminator == 0");
//			return CORE_FAIL;
//		}
//	} else if (uart_read_transact_type == UART_READ_TRANSACT_STRING_TERMINATOR) {
//		if (uart_read_transaction_end_with_data->delimiter == NULL) {
//			CORE_LOGE(TAG, "uart_read_transaction_end_with_data->delimiter == NULL");
//			return CORE_FAIL;
//		}
//	}

	if (uart_port >= UART_CORE_NUM_MAX) {
		return ret;
	} else {
		if (uart_core_config_data[uart_port].uart_running_status == UART_NOT_INITIALIZE) {
//			uart_core_config_data[uart_port].uart_config.baud_rate = uart_config->baud_rate;
//			uart_core_config_data[uart_port].uart_config.data_bits = uart_config->data_bits;
//			uart_core_config_data[uart_port].uart_config.parity = uart_config->parity;
//			uart_core_config_data[uart_port].uart_config.stop_bits = uart_config->stop_bits;
//			uart_core_config_data[uart_port].uart_config.flow_ctrl = uart_config->flow_ctrl;

			uart_config_data.baud_rate = uart_config->baud_rate;
			uart_config_data.data_bits = uart_config->data_bits;
			uart_config_data.parity = uart_config->parity;
			uart_config_data.stop_bits = uart_config->stop_bits;
			uart_config_data.flow_ctrl = uart_config->flow_ctrl;

			if (uart_pin != NULL) {
				uart_core_config_data[uart_port].uart_pin.tx_io_num = uart_pin->tx_io_num;
				uart_core_config_data[uart_port].uart_pin.rx_io_num = uart_pin->rx_io_num;
				uart_core_config_data[uart_port].uart_pin.rts_io_num = uart_pin->rts_io_num;
				uart_core_config_data[uart_port].uart_pin.cts_io_num = uart_pin->cts_io_num;
			} else {
				uart_core_config_data[uart_port].uart_pin.tx_io_num = UART_CORE_PIN_NO_CHANGE;
				uart_core_config_data[uart_port].uart_pin.rx_io_num = UART_CORE_PIN_NO_CHANGE;
				uart_core_config_data[uart_port].uart_pin.rts_io_num = UART_CORE_PIN_NO_CHANGE;
				uart_core_config_data[uart_port].uart_pin.cts_io_num = UART_CORE_PIN_NO_CHANGE;
			}

			ret = uart_core_param_config(uart_port, &uart_config_data);
			if (ret == CORE_OK) {

				ret = uart_core_set_pin(uart_port, uart_core_config_data[uart_port].uart_pin.tx_io_num,
						uart_core_config_data[uart_port].uart_pin.rx_io_num,
						uart_core_config_data[uart_port].uart_pin.rts_io_num,
						uart_core_config_data[uart_port].uart_pin.cts_io_num);

				if (ret == CORE_OK) {
					ret = uart_core_driver_install(uart_port, RD_BUF_SIZE, WR_BUF_SIZE, UART_EVENT_QUEUE_SIZE,
							&uart_core_config_data[uart_port].uart_event_queue_handle, 0);
					if (ret == CORE_OK) {
						uart_core_config_data[uart_port].uart_read_buff = (uint8_t*) calloc(RD_BUF_SIZE + 1,
								sizeof(uint8_t));
						if (uart_core_config_data[uart_port].uart_read_buff == NULL) {
							uart_core_driver_delete(uart_port);
							CORE_LOGE(TAG, "Failed to allocate uart_read_buff memory UART_%d", uart_port);
							return ret = CORE_ERR_NO_MEM;
						} else {
							uart_core_config_data[uart_port].uart_core_transactor_cb_handle =
									uart_core_transactor_cb_handle;

							uart_core_config_data[uart_port].uart_read_transact_type = uart_read_transact_type;

							memcpy(&uart_core_config_data[uart_port].uart_read_transaction_end_with_data,
									uart_read_transaction_end_with_data, sizeof(UART_CORE_TRANSACTION_END_WITH_DATA));

							char uart_evnt_task_name[10] = { 0 };
							sprintf(uart_evnt_task_name, "uart_evn_%d", uart_port);

							char uart_task_name[10] = { 0 };
							sprintf(uart_task_name, "uart_%d_task", uart_port);

							uart_core_config_data[uart_port].uart_byte_queue_handle = gll_init();
							if (uart_core_config_data[uart_port].uart_byte_queue_handle == NULL) {
								CORE_LOGE(TAG, "Failed allocate memory for UART read byte queue");
							}

							uart_core_config_data[uart_port].uart_core_transact_mutex_handle = SemaphoreCreateMutex();
							if (uart_core_config_data[uart_port].uart_core_transact_mutex_handle == NULL) {
								CORE_LOGE(TAG, "Failed allocate memory for UART transact Mutex handle");
							}

							BaseType xReturned = TaskCreate(uart_event_task, uart_evnt_task_name, TASK_STACK_SIZE_4K,
									(void* )uart_port, THREAD_PRIORITY_UART_CORE_EVENT_TASK,
									&uart_core_config_data[uart_port].uart_evnt_thread_handle);
							if (xReturned == true) {
								BaseType xReturned = TaskCreate(uart_core_task, uart_task_name, TASK_STACK_SIZE_2K,
										(void* )uart_port, THREAD_PRIORITY_UART_CORE_TASK,
										&uart_core_config_data[uart_port].uart_task_thread_handle);
								if (xReturned == true) {

									uart_core_config_data[uart_port].uart_running_status = UART_INITIALIZE;
								}
							} else {
								CORE_LOGE(TAG, "failed to UART_%d task", uart_port);
								free(uart_core_config_data[uart_port].uart_read_buff);
								uart_core_config_data[uart_port].uart_read_buff = NULL;
								uart_core_driver_delete(uart_port);
								return ret = CORE_ERR_NO_MEM;
							}
						}
					} else {
						CORE_LOGE(TAG, "Failed to install UART_%d driver", uart_port);
					}
				} else {
					CORE_LOGE(TAG, "Failed to configure UART_%d pins", uart_port);
				}

			} else {
				CORE_LOGE(TAG, "Failed to configure UART_%d", uart_port);
			}

		}
	}
	return ret;
}

/**
 *
 */
core_err deinit_uart_core_driver(uart_core_port_t uart_port) {
	core_err ret = CORE_FAIL;

	if (uart_port >= UART_CORE_NUM_MAX) {
		return ret;
	} else {
		if (uart_core_config_data[uart_port].uart_running_status != UART_NOT_INITIALIZE) {
			ret = uart_core_driver_delete(uart_port);
			if (ret == CORE_OK) {
				if (uart_core_config_data[uart_port].uart_evnt_thread_handle != NULL) {
					uart_core_config_data[uart_port].is_uart_evnt_thread_handle = true;
				}
				if (uart_core_config_data[uart_port].uart_task_thread_handle != NULL) {
					uart_core_config_data[uart_port].is_uart_task_thread_handle = true;
				}

				// wait for threads to be closed
				while (1) {
					if (uart_core_config_data[uart_port].is_uart_evnt_thread_handle == false
							&& uart_core_config_data[uart_port].is_uart_task_thread_handle == false) {
						break;
					}
					TaskDelay(DELAY_250_MSEC / TICK_RATE_TO_MS);
				}

//				if (uart_core_config_data[uart_port].uart_event_queue_handle != NULL) {
//					CORE_LOGD(TAG, "Delete uart[%d] queue handle", uart_port);
//					QueueDelete(uart_core_config_data[uart_port].uart_event_queue_handle);
//				}

				if (uart_core_config_data[uart_port].uart_byte_queue_handle != NULL) {
					gll_destroy(uart_core_config_data[uart_port].uart_byte_queue_handle);
				}

				if (uart_core_config_data[uart_port].uart_core_transact_mutex_handle != NULL) {
					SemaphoreDelete(uart_core_config_data[uart_port].uart_core_transact_mutex_handle);
				}

				bzero(&uart_core_config_data[uart_port], sizeof(UART_CORE_CONFIGURATION_DATA));
				uart_core_config_data[uart_port].uart_running_status = UART_NOT_INITIALIZE;
			} else {
				CORE_LOGE(TAG, "Failed to delete UART_%d driver", uart_port);
			}
		}
	}

	return ret;
}

/**
 *
 */
int uart_core_driver_write_bytes(uart_core_port_t uart_num, const char* src, size_t size) {
	return uart_core_write_bytes(uart_num, src, size);
}

/**
 *
 */
core_err uart_core_driver_update_transaction_bytes_size(uart_core_port_t uart_num, uint32_t num_bytes) {
	core_err ret = CORE_FAIL;
	if (num_bytes >= RD_BUF_SIZE || num_bytes <= 0) {
		return ret = CORE_ERR_INVALID_ARG;
	}

	if (uart_num >= UART_CORE_NUM_MAX) {
		return ret = CORE_FAIL;
	}



	UART_CORE_CONFIGURATION_DATA *uart_core_data = &uart_core_config_data[uart_num];

	SemaphoreTake(uart_core_data->uart_core_transact_mutex_handle, THREADING_MAX_DELAY / TICK_RATE_TO_MS);

	uart_core_data->uart_read_transaction_end_with_data.num_bytes = (int) num_bytes;

	SemaphoreGive(uart_core_data->uart_core_transact_mutex_handle);

	return ret = CORE_OK;
}

