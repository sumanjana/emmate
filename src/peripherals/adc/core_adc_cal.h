/*
 * File Name: core_adc_cal.h
 * File Path: /emmate/src/peripherals/adc/core_adc_cal.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Noyel Seth
 */

#ifndef CORE_ADC_CAL_H_
#define CORE_ADC_CAL_H_

#include "core_config.h"

#if 1 //CONFIG_PLATFORM_ESP_IDF   // This ADC calibration file only used for ESP32

#include "adc_platform_cal.h"

/**
 * @brief Type of calibration value used in characterization
 */
typedef enum {
	CORE_ADC_CAL_VAL_EFUSE_VREF = PF_ADC_CAL_VAL_EFUSE_VREF , 	/**< Characterization based on reference voltage stored in eFuse*/
	CORE_ADC_CAL_VAL_EFUSE_TP = PF_ADC_CAL_VAL_EFUSE_TP 	, 		/**< Characterization based on Two Point values stored in eFuse*/
	CORE_ADC_CAL_VAL_DEFAULT_VREF = PF_ADC_CAL_VAL_DEFAULT_VREF, 	/**< Characterization based on default reference voltage*/
} core_adc_cal_value_t;

/**
 * @brief Structure storing characteristics of an ADC
 *
 * @note Call core_adc_cal_characterize() to initialize the structure
 */
typedef adc_cal_pf_characteristics_t	core_adc_cal_characteristics_t;

/**
 * @brief Checks if ADC calibration values are burned into eFuse
 *
 * @note <b>For ESP</b>,This function checks if ADC reference voltage or Two Point values have been
 * burned to the eFuse of the current ESP32
 *
 * @param   value_type  Type of calibration value (CORE_ADC_CAL_VAL_EFUSE_VREF or CORE_ADC_CAL_VAL_EFUSE_TP)
 *
 * @return
 *      - CORE_OK: The calibration mode is supported in eFuse
 *      - CORE_ERR_NOT_SUPPORTED: Error, eFuse values are not burned
 *      - CORE_ERR_INVALID_ARG: Error, invalid argument (CORE_ADC_CAL_VAL_DEFAULT_VREF)
 */
#define core_adc_cal_check_efuse(value_type)		pf_adc_cal_check_efuse(value_type)

/**
 * @brief Characterize an ADC at a particular attenuation
 *
 * This function will characterize the ADC at a particular attenuation and generate
 * the ADC-Voltage curve in the form of [y = coeff_a * x + coeff_b].
 * Characterization can be based on Two Point values, eFuse Vref, or default Vref
 * and the calibration values will be prioritized in that order.
 *
 * @note Two Point values and eFuse Vref can be enabled/disabled using menuconfig.
 *
 * @param[in]   adc_num         ADC to characterize (ADC_CORE_UNIT_1 or ADC_CORE_UNIT_2)
 * @param[in]   atten           Attenuation to characterize
 * @param[in]   bit_width       Bit width configuration of ADC
 * @param[in]   default_vref    Default ADC reference voltage in mV (used if eFuse values is not available)
 * @param[out]  chars           Pointer to empty structure used to store ADC characteristics
 *
 * @return
 *      - CORE_ADC_CAL_VAL_EFUSE_VREF: eFuse Vref used for characterization
 *      - CORE_ADC_CAL_VAL_EFUSE_TP: Two Point value used for characterization (only in Linear Mode)
 *      - CORE_ADC_CAL_VAL_DEFAULT_VREF: Default Vref used for characterization
 */
#define core_adc_cal_characterize(adc_num, atten, bit_width, default_vref, chars)			pf_adc_cal_characterize(adc_num, atten, bit_width, default_vref, chars)


/**
 * @brief   Convert an ADC reading to voltage in mV
 *
 * This function converts an ADC reading to a voltage in mV based on the ADC's
 * characteristics.
 *
 * @note    Characteristics structure must be initialized before this function
 *          is called (call core_adc_cal_characterize())
 *
 * @param[in]   adc_reading     ADC reading
 * @param[in]   chars           Pointer to initialized structure containing ADC characteristics
 *
 * @return      Voltage in mV
 */
#define core_adc_cal_raw_to_voltage(adc_reading, chars)		pf_adc_cal_raw_to_voltage(adc_reading, chars)

/**
 * @brief   Reads an ADC and converts the reading to a voltage in mV
 *
 * This function reads an ADC then converts the raw reading to a voltage in mV
 * based on the characteristics provided. The ADC that is read is also
 * determined by the characteristics.
 *
 * @note    The Characteristics structure must be initialized before this
 *          function is called (call core_adc_cal_characterize())
 *
 * @param[in]   channel     ADC Channel to read
 * @param[in]   chars       Pointer to initialized ADC characteristics structure
 * @param[out]  voltage     Pointer to store converted voltage
 *
 * @return
 *      - CORE_OK: ADC read and converted to mV
 *      - CORE_ERR_TIMEOUT: Error, timed out attempting to read ADC
 *      - CORE_ERR_INVALID_ARG: Error due to invalid arguments
 */
#define core_adc_cal_get_voltage(channel, chars, voltage)				pf_adc_cal_get_voltage(channel, chars, voltage)

/* -------------------------- Deprecated API ------------------------------- */

/** @cond */    //Doxygen command to hide deprecated function from API Reference
/**
 * @deprecated  ADC1 characterization function. Deprecated in order to accommodate
 *              ADC2 and eFuse functionality. Use core_adc_cal_characterize() instead
 */
#define core_adc_cal_get_characteristics(vref, atten, bit_width, chars)		pf_adc_cal_get_characteristics(vref, atten, bit_width, chars)

/*
 * @deprecated  This function reads ADC1 and returns the corrected voltage. This
 *              has been deprecated in order to accommodate ADC2 support. Use the
 *              new function core_adc_cal_get_voltage() instead.
 */
#define adc_core_adc1_to_voltage(channel, chars)					adc_pf_adc1_to_voltage(channel, chars)

/** @endcond */


#else

#endif

#endif /* CORE_ADC_CAL_H_ */
