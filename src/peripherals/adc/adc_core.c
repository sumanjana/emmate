/*
 * File Name: adc_core.c
 * File Path: /emmate/src/adc/adc_core.c
 * Description:
 *
 *  Created on: 28-May-2019
 *      Author: Noyel Seth
 */

#include "adc_core.h"

#include "adc_core_channel.h"
#include "core_adc_cal.h"
#include "threading.h"
#include "core_constant.h"
#include "module_thread_priorities.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>
#include "gll.h"

#define TAG 	LTAG_ADC

static gll_t *adc_conn_list = NULL;

#define DEFAULT_VREF    1100        // Default Nominal vRef
#define NO_OF_SAMPLES   64          //Multisampling

typedef struct {
	// In params
	uint8_t adc_channel_gpio;
	adc_core_channel_t adc_channel_t;
	adc_core_unit_t adc_unit_t;
	adc_core_bits_width_t bits_width_t;
	adc_core_atten_t atten_t;
	uint16_t adc_read_interval_t;

	// Out Params
	int adc_raw_data;
	uint32_t adc_mV_data;

	// internal use
	core_adc_cal_characteristics_t *adc_chars;
	ThreadHandle adc_thread_handle;
} ADC_CONN_DATA;

static ADC_CONN_DATA * find_running_adc(uint8_t adc_channel_gpio, size_t *node_pos) {
	ADC_CONN_DATA * temp_adc_conn_data = NULL;
	if (adc_conn_list != NULL) {
		size_t list_size = adc_conn_list->size;
		CORE_LOGD(TAG, "adc_conn_list size: %d\r\n", list_size);
		size_t count = 0;
		for (count = 0; count < list_size; count++) {
			temp_adc_conn_data = (ADC_CONN_DATA*) gll_get(adc_conn_list, count);
			if (temp_adc_conn_data->adc_channel_gpio == adc_channel_gpio) {
				*node_pos = count;
				return temp_adc_conn_data;
			} else {
				//CORE_LOGW(TAG, "ADC channel_gpio not yet started.\r\n");
			}
		}
	}
	*node_pos = -1;
	return NULL;
}

static void check_efuse() {
	//Check TP is burned into eFuse
	if (core_adc_cal_check_efuse(CORE_ADC_CAL_VAL_EFUSE_TP) == CORE_OK) {
		CORE_LOGW(TAG, "eFuse Two Point: Supported\n");
	} else {
		CORE_LOGD(TAG, "eFuse Two Point: NOT supported\n");
	}

	//Check Vref is burned into eFuse
	if (core_adc_cal_check_efuse(CORE_ADC_CAL_VAL_EFUSE_VREF) == CORE_OK) {
		CORE_LOGW(TAG, "eFuse Vref: Supported\n");
	} else {
		CORE_LOGD(TAG, "eFuse Vref: NOT supported\n");
	}

}

static void print_char_val_type(core_adc_cal_value_t val_type) {
	if (val_type == CORE_ADC_CAL_VAL_EFUSE_TP) {
		CORE_LOGW(TAG, "Characterized using Two Point Value\n");
	} else if (val_type == CORE_ADC_CAL_VAL_EFUSE_VREF) {
		CORE_LOGW(TAG, "Characterized using eFuse Vref\n");
	} else {
		CORE_LOGW(TAG, "Characterized using Default Vref\n");
	}
}

static void adc_opt_task(void * pvParameters) {

	ADC_CONN_DATA *adc_conn_data = NULL;
	adc_conn_data = (ADC_CONN_DATA *) pvParameters;

	CORE_LOGD(TAG, "ADC GPIO %d, Interval %d, Adc bit-width %d, ADC data atten %d\r\n", adc_conn_data->adc_channel_gpio,
			adc_conn_data->adc_read_interval_t, adc_conn_data->bits_width_t, adc_conn_data->atten_t);

	check_efuse();

	//Configure ADC
	if (adc_conn_data->adc_unit_t == ADC_CORE_UNIT_1) {
		adc_core_adc1_config_width(adc_conn_data->bits_width_t);
		adc_core_adc1_config_channel_atten(adc_conn_data->adc_channel_t, adc_conn_data->atten_t);
	} else {
		adc_core_adc2_config_channel_atten((adc2_core_channel_t ) adc_conn_data->adc_channel_t, adc_conn_data->atten_t);
	}

	//Characterize ADC
	adc_conn_data->adc_chars = calloc(1, sizeof(core_adc_cal_characteristics_t));
	if(adc_conn_data->adc_chars != NULL){
		core_adc_cal_value_t val_type = core_adc_cal_characterize(adc_conn_data->adc_unit_t, adc_conn_data->atten_t,
			adc_conn_data->bits_width_t, DEFAULT_VREF, adc_conn_data->adc_chars);
		print_char_val_type(val_type);
	}else{

		CORE_LOGE(TAG,"Failed to allocate memory for core_adc_cal_characteristics_t for ADC-GPIO %d\r\n", adc_conn_data->adc_channel_gpio);
	}

	int n = 1;
	while (1) {
		uint32_t adc_reading = 0;
		//Multisampling
		for (int i = 0; i < NO_OF_SAMPLES; i++) {
			if (adc_conn_data->adc_unit_t == ADC_CORE_UNIT_1) {
				adc_reading += adc_core_adc1_get_raw((adc1_core_channel_t ) adc_conn_data->adc_channel_t)
				;
			} else {
				int raw;
				adc2_get_raw((adc2_core_channel_t) adc_conn_data->adc_channel_t, adc_conn_data->bits_width_t, &raw);
				adc_reading += raw;
			}
		}
		adc_reading /= NO_OF_SAMPLES;
		adc_conn_data->adc_raw_data = adc_reading;
		//Convert adc_reading to voltage in mV
		uint32_t voltage = core_adc_cal_raw_to_voltage(adc_reading, adc_conn_data->adc_chars);
		adc_conn_data->adc_mV_data = voltage;
		//printf("Raw: %d\tVoltage: %dmV\n", adc_reading, voltage);
		CORE_LOGD(TAG, "ADC GPIO %d, Raw: %d, Voltage: %d\n",adc_conn_data->adc_channel_gpio, adc_reading, voltage);

		TaskDelay( adc_conn_data->adc_read_interval_t / TICK_RATE_TO_MS );
	}
}

/*****************************************************************************************************************/


/*
 *
 */
core_err init_adc_peripheral(uint8_t adc_channel_gpio, uint16_t adc_read_interval_t, adc_core_bits_width_t bits_width_t,
		adc_core_atten_t atten_t) {
	core_err ret = CORE_FAIL;

	ADC_CONN_DATA *adc_conn_data = NULL;

	if (adc_conn_list == NULL) {
		adc_conn_list = gll_init();
	}
	size_t node_pos = 0;

	if (validate_adc_channel(adc_channel_gpio) == true) {
		ADC_CONN_DATA * temp_adc_conn_data = find_running_adc(adc_channel_gpio, &node_pos);
		if (temp_adc_conn_data != NULL) {
			ret = CORE_ON_PROCESS;
		} else {
			adc_conn_data = (ADC_CONN_DATA*) calloc(1, sizeof(ADC_CONN_DATA));
			if (adc_conn_data != NULL) {
				// set ADC channel
				adc_conn_data->adc_channel_gpio = adc_channel_gpio;

				// SET channel & ADC Unit
				select_adc_channel(adc_channel_gpio, &adc_conn_data->adc_unit_t, &adc_conn_data->adc_channel_t);

				// set ADC read Bit wide & attenuation condition
				adc_conn_data->atten_t = atten_t;
				adc_conn_data->bits_width_t = bits_width_t;

				//set ADC read interval time
				adc_conn_data->adc_read_interval_t = adc_read_interval_t;

				if (gll_add(adc_conn_list, adc_conn_data, 0) == C_OK) {
					char buff[15] = { 0 };
					sprintf(buff, "adc_opt_%d", adc_conn_data->adc_channel_gpio);
					BaseType xReturned = TaskCreate(adc_opt_task, buff, TASK_STACK_SIZE_2K, adc_conn_data,
							THREAD_PRIORITY_ADC_OPERATION_TASK, &adc_conn_data->adc_thread_handle);
					if (true != xReturned) {
						CORE_LOGE(TAG, "failed to create %s task!", buff);
						/* TODO: If code reaches here, thread must be stopped and resources must be cleaned before returning; */
						deinit_adc_peripheral(adc_channel_gpio);
						ret = CORE_FAIL;
					} else {
						TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS); // wait for 1secs to complete 1 adc read.
						ret = CORE_OK;
					}
				} else {
					free(adc_conn_data);
				}
			} else {
				return CORE_ERR_NO_MEM;
			}
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}

	return ret;
}



/*
 *
 */
core_err get_adc_peripheral_data(uint8_t adc_channel_gpio, int *raw_data, uint32_t *mV_data) {
	core_err ret = CORE_FAIL;
	size_t node_pos = 0;
	ADC_CONN_DATA *adc_conn_data = NULL;
	if (adc_conn_list != NULL) {
		adc_conn_data = find_running_adc(adc_channel_gpio, &node_pos);
		if (adc_conn_data != NULL && node_pos != -1) {
			*raw_data = adc_conn_data->adc_raw_data;
			if(adc_conn_data->adc_chars != NULL){
				*mV_data = core_adc_cal_raw_to_voltage(adc_conn_data->adc_raw_data, adc_conn_data->adc_chars);
			}else{
				*mV_data = 0;
			}
			ret = CORE_OK;
		} else {
			ret = CORE_ERR_INVALID_ARG;
		}
	}
	return ret;
}


/*
 *
 */
core_err pause_adc_peripheral_read(uint8_t adc_channel_gpio) {
	core_err ret = CORE_FAIL;
	size_t node_pos = 0;
	ADC_CONN_DATA *adc_conn_data = NULL;
	if (adc_conn_list != NULL) {
		adc_conn_data = find_running_adc(adc_channel_gpio, &node_pos);
		if (adc_conn_data != NULL && node_pos != -1) {
			if (adc_conn_data->adc_thread_handle != NULL) {
				CORE_LOGD(TAG, "TaskSuspend(adc_conn_data->adc_thread_handle);\r\n");
				TaskSuspend(adc_conn_data->adc_thread_handle);
				return ret = CORE_OK;
			}
		} else {
			return ret = CORE_ERR_INVALID_ARG;
		}
	}
	return ret;
}


/*
 *
 */
core_err resume_adc_peripheral_read(uint8_t adc_channel_gpio) {
	core_err ret = CORE_FAIL;
	size_t node_pos = 0;
	ADC_CONN_DATA *adc_conn_data = NULL;
	if (adc_conn_list != NULL) {
		adc_conn_data = find_running_adc(adc_channel_gpio, &node_pos);
		if (adc_conn_data != NULL && node_pos != -1) {
			if (adc_conn_data->adc_thread_handle != NULL) {
				CORE_LOGD(TAG, "TaskSuspend(adc_conn_data->adc_thread_handle);\r\n");
				TaskResume(adc_conn_data->adc_thread_handle);
				return ret = CORE_OK;
			}
		} else {
			return ret = CORE_ERR_INVALID_ARG;
		}
	}
	return ret;
}



/*
 *
 */
core_err deinit_adc_peripheral(uint8_t adc_channel_gpio) {
	core_err ret = CORE_FAIL;
	size_t node_pos = 0;
	ADC_CONN_DATA *adc_conn_data = NULL;
	if (adc_conn_list != NULL) {
		adc_conn_data = find_running_adc(adc_channel_gpio, &node_pos);
		if (adc_conn_data != NULL && node_pos != -1) {
			adc_conn_data = (ADC_CONN_DATA *) gll_remove(adc_conn_list, node_pos);
			free(adc_conn_data->adc_chars);
			if (adc_conn_data->adc_thread_handle != NULL) {
				CORE_LOGD(TAG, "TaskDelete(adc_conn_data->adc_thread_handle);\r\n");
				TaskDelete(adc_conn_data->adc_thread_handle);
			}
			free(adc_conn_data);
			return ret = CORE_OK;
		} else {
			return ret = CORE_ERR_INVALID_ARG;
		}
	}
	return ret;
}
