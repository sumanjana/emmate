/*
 * File Name: core_utils.c
 * File Path: /emmate/src/utils/core_utils.c
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#define TAG LTAG_UTILS

void set_all_log_levels_at_init() {
	set_core_logger_level(LTAG_ALL_MODULE, CORE_LOG_INFO);
	set_core_logger_level(LTAG_CONN_BLEGATT, CORE_LOG_NONE);
//	set_core_logger_level(LTAG_SOM_REG, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_CONNPROTO_HTTPCLI, CORE_LOG_DEBUG);
//	set_core_logger_level("HTTP_CLIENT", CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_CONN_WIFIPLAT, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_OTA, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_SYSHB, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_OTA, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_HMI, CORE_LOG_NONE);
//	set_core_logger_level(LTAG_HMI_LED, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_BTN_HELPER, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_SYSTEM_SYSINIT, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_CONN, CORE_LOG_DEBUG);
//	set_core_logger_level(LTAG_CONNPROTO_HTTPCLIAPI, CORE_LOG_DEBUG);
}

/*
 *
 */
char* extract_variable_name(char *var, char* delim) {
	CORE_LOGD(TAG, "var = %s, delim=%s\r\n", var, delim);
	char* lim = delim;
	char* temptok = NULL;
//when we use macro then replace all if with ternary operator.
	if (lim != NULL) {
		char* token = strstr(var, delim);
		if (token == NULL) {
			temptok = NULL;
		}
		while (token != NULL) {
			CORE_LOGD(TAG, "tok = %s\r\n", token);
			if (strcmp(delim, "->") == 0) {
				temptok = token + 2;
			} else if (strcmp(delim, ".") == 0) {
				temptok = token + 1;
			}
			token = strstr(temptok, delim);
		}
	} else {
		return var;
	}
	return temptok;
}

int format_mac(uint8_t *mac, char *fmt_mac) {
	int ret = 0;
	char buf[50] = { 0 };
	if ((mac != NULL) || (fmt_mac != NULL)) {
		if ((mac[0]==0) && (mac[1]==0) && (mac[2]==0) && (mac[3]==0) && (mac[4]==0) && (mac[5]==0)) {
			return 1;
		}
		sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		CORE_LOGD(TAG, "Formatted MAC:");
		CORE_LOGD(TAG, "MAC Len: %d", strlen(buf));
		CORE_LOGD(TAG, "MAC Addr: %s", buf);
		memcpy(fmt_mac, buf, strlen(buf));
		ret = 0;
	} else {
		ret = -1;
	}

	return ret;
}

core_err hexbytearray_to_str(uint8_t *in_hex_arr, size_t num_bytes, char *out_hex_str/*, size_t str_size*/) {
//	CORE_LOGE(TAG, "STR_SIZE = %lu\n", str_size);

//	if ((num_bytes != 0) && (str_size != 0)) {
//		if (((num_bytes * 2) + 1) != str_size) {
//			CORE_LOGE(TAG, "Incorrect hex string length\nNote: str_size must be greater than or equal to ((num_bytes*2)+1)\n");
//			return CORE_ERR_INVALID_ARG;
//		}
//	} else {
//		CORE_LOGE(TAG, "array size is 0\n");
//		return CORE_ERR_INVALID_ARG;
//	}

	if (num_bytes <= 0) {
		CORE_LOGE(TAG, "num_bytes is <= 0");
		return CORE_ERR_INVALID_ARG;
	}

	if (in_hex_arr == NULL) {
		CORE_LOGE(TAG, "hex_arr is NULL");
		return CORE_ERR_INVALID_ARG;
	}

	if (out_hex_str == NULL) {
		CORE_LOGE(TAG, "hex_str is NULL");
		return CORE_ERR_INVALID_ARG;
	}

	memset(out_hex_str, 0, ((num_bytes*2)+1));

	int pos = 0;
	for (int i = 0; i < num_bytes; i++) {
		sprintf((out_hex_str + pos), "%02x", in_hex_arr[i]);
		pos += 2;
	}

	CORE_LOGD(TAG, "OUT_HEX_STR = %s\n", out_hex_str);
	return CORE_OK;
}

core_err str_to_hexbytearray(char *in_hex_str, /*size_t str_size,*/ uint8_t *out_hex_arr, size_t num_bytes) {
//	CORE_LOGE(TAG, "STR_SIZE = %lu\n", str_size);

//	if ((arr_size != 0) && (str_size != 0)) {
//		if (((arr_size * 2) + 1) != str_size) {
//			CORE_LOGE(TAG, "Incorrect hex string length\nNote: str_size must be equal to ((arr_size*2)+1)\n");
//			return CORE_ERR_INVALID_ARG;
//		}
//	} else {
//		CORE_LOGE(TAG, "array size is 0\n");
//		return CORE_ERR_INVALID_ARG;
//	}

	if (num_bytes <= 0) {
		CORE_LOGE(TAG, "num_bytes is <= 0");
		return CORE_ERR_INVALID_ARG;
	}

	if (out_hex_arr == NULL) {
		CORE_LOGE(TAG, "hex_arr is NULL\n");
		return CORE_ERR_INVALID_ARG;
	}

	if (in_hex_str == NULL) {
		CORE_LOGE(TAG, "hex_str is NULL\n");
		return CORE_ERR_INVALID_ARG;
	}

	const char* pos = in_hex_str;
	size_t count = 0;
	for (count = 0; count < num_bytes; count++) {
		sscanf(pos, "%2hhx", &out_hex_arr[count]);
		pos += 2 * sizeof(char);
	}

	CORE_LOGD(TAG, "OUT_HEX_ARR:");
	for (count = 0; count < num_bytes; count++) {
		CORE_LOGD(TAG, "%02x", out_hex_arr[count]);
	}

	return CORE_OK;
}
