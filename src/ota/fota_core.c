/*
 * File Name: ota_core.c
 * File Path: /emmate/src/ota/ota_core.c
 * Description:
 *
 *  Created on: 25-Apr-2019
 *      Author: Rohan Dey
 */

#include "fota_core.h"
#include "fota_core_verify.h"
#include "fota_core_helper.h"
#include "http_client_core.h"
#include "http_constant.h"
#include "system.h"
#include "system_urls.h"
#include "system_utils.h"
#include "input_processor.h"
#include "inpproc_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#endif
#include "threading.h"
#include "module_thread_priorities.h"
#include "event_group_core.h"
#include "conn.h"

#if TESTING
#include "ota_testing_codes.h"
#endif

#include <string.h>
#include <stdlib.h>

#define TAG LTAG_OTA

typedef struct {
//	QueueHandle fota_sch_q;
//	QueueHandle fota_task_q;
	ThreadHandle fota_thread;
//	bool fota_running;
//	bool fota_manager_init_stat;
} FotaHelperData;

static FotaHelperData m_fhd = { .fota_thread = NULL};//, .fota_running = false, .fota_manager_init_stat = false };

static core_err download_new_firmware(HttpClientConfig *config) {
	core_err ret = CORE_FAIL, ota_finish_ret = CORE_FAIL;

	ota_https_ota_config_t ota_config = { .http_config = config, };
	ota_https_ota_handle_t https_ota_handle = NULL;

	ret = ota_https_ota_begin(&ota_config, &https_ota_handle);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "HTTPS OTA Begin failed!");
		return CORE_FAIL;
	}

	while (1) {
		ret = ota_https_ota_perform(https_ota_handle);
		if (ret != OTA_ERR_HTTPS_OTA_IN_PROGRESS) {
			break;
		}
		// ota_https_ota_perform returns after every read operation which gives user the ability to
		// monitor the status of OTA upgrade by calling ota_https_ota_get_image_len_read, which gives length of image
		// data read so far.
		CORE_LOGW(TAG, "Image bytes read: %d", ota_https_ota_get_image_len_read(https_ota_handle));
	}

	ota_finish_ret = ota_https_ota_finish(https_ota_handle);
	if ((ret == CORE_OK) && (ota_finish_ret == CORE_OK)) {
		ESP_LOGI(TAG, "HTTPS OTA download successful");
		ret = CORE_OK;
	} else {
		ESP_LOGE(TAG, "HTTPS OTA download failed!");
		ret = CORE_FAIL;
	}
	return ret;
}

void fota_task(void * pvParameter) {
	CORE_LOGI(TAG, "Starting FOTA process...");

	FotaInfo *fota_data = (FotaInfo*) pvParameter;

	CORE_LOGD(TAG, "Going to do FOTA with the following information:");
	CORE_LOGD(TAG, "id = %s", fota_data->id ? fota_data->id : "no id");
//	CORE_LOGD(TAG, "stat = %d", fota_data->stat);
	CORE_LOGD(TAG, "url_len = %d", (int )fota_data->url_len);
	CORE_LOGD(TAG, "url = %s", fota_data->url ? fota_data->url : "no url");
#if 0
	CORE_LOGD(TAG, "cert_len = %d", (int )fota_data->cert_len);
	CORE_LOGD(TAG, "cert : %s", fota_data->cert ? "cert is present" : "no cert");
#endif
	CORE_LOGD(TAG, "ver = %s", fota_data->ver);
	CORE_LOGD(TAG, "fname = %.*s", CONFIG_FIRMWARE_FILENAME_MAX_LEN, fota_data->fname);
	CORE_LOGD(TAG, "fsize = %d", (int )fota_data->fsize);

	// TODO: Save the scheduled fota time in the NV memory so that we can check after reboot
	char strftime_buf[64];
	strftime(strftime_buf, sizeof(strftime_buf), "%c", &fota_data->sch);
	CORE_LOGD(TAG, "sch = %s", strftime_buf);

	int dnl_try_count = 1;
	do {
		_send_fota_status_to_server(fota_data->id, FOTA_STATUS_DOWNLOADING, dnl_try_count); /* Send DOWNLOADING status to server */

		HttpClientConfig config = { .url = fota_data->url, .cert_pem = (char *) /*fota_data->cert*/ IQ_HOST_ROOTCA,
				/*.event_handler = fota_http_event_handler,*/};
//		core_err ret = ota_perform_https_ota(&config);
		core_err ret = download_new_firmware(&config);

		if (ret == CORE_OK) {
			CORE_LOGI(TAG, "FOTA is Successful, acknowledging the server");
			_send_fota_status_to_server(fota_data->id, FOTA_STATUS_DOWNLOADED, 0); /* Send DOWNLOADED status to server */

			// TODO: If scheduled time not received from server, do fota
			CORE_LOGI(TAG, "The next reboot will start the new firmware");
			CORE_LOGI(TAG, "Scheduled time for starting the new firmware is: %s", strftime_buf);

			while (1) {
				double remaining_secs = diff_time_with_now(&fota_data->sch);
				if (remaining_secs == (-DBL_MAX)) {
					CORE_LOGD(TAG, "diffence in time returned -DBL_MAX, hence system time is not set");
					remaining_secs = -1;
				}

				CORE_LOGI(TAG, "Remaining time for booting the new firmware is: %lf secs", remaining_secs);
				if (remaining_secs <= 0) {
					/* call system's event dispatcher to notify other threads about a system reboot */
					/* TODO: To change the reboot event dispatcher to multiple modules */
					CORE_LOGI(TAG, "Dispatching reboot event ...");
					notify_system_reboot_event();
//					CORE_LOGI(TAG, "Waiting for all modules to complete their tasks");

					/* Save the fota id. Required for image verify after reboot */
					_save_fota_identifier(fota_data->id);
					_send_fota_status_to_server(fota_data->id, FOTA_STATUS_UPDATING, 0); /* Send UPDATING status to server */

					// REBOOTING ...
					core_system_restart();
				} else {
					TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
				}
			}
		} else {
			CORE_LOGE(TAG, "Failed to download the new firmware. Retrying ...");
			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
		}
	} while (++dnl_try_count < CONFIG_FOTA_MAX_RETRY_COUNT);

	CORE_LOGE(TAG, "================================ FOTA FAILED ================================");
	_send_fota_status_to_server(fota_data->id, FOTA_STATUS_DOWNLOAD_FAILED, 0); /* Send DOWNLOAD_FAILED status to server */

//	CORE_LOGI(TAG, "Stack remaining for task '%s' is %d bytes", TaskGetTaskName(NULL),
//			TaskPfGetStackHighWaterMark(NULL));
	m_fhd.fota_thread = NULL;
	TaskDelete(NULL);
}

/*
 * Start A task for completing the OTA Process
 */
core_err start_fota_task(FotaInfo *recv_fota_info) {

	if (m_fhd.fota_thread != NULL) {
		CORE_LOGI(TAG, "A fota download is already running. Not starting another one.");
		return CORE_FAIL;
	}

	static FotaInfo fota_info;
	memcpy(&fota_info, recv_fota_info, sizeof(FotaInfo));

	BaseType xReturned = TaskCreate(&fota_task, "fota-task", TASK_STACK_SIZE_8K, &fota_info, THREAD_PRIORITY_FOTA, &m_fhd.fota_thread);
	if (true == xReturned) {
		CORE_LOGD(TAG, "Ota task started");
		return CORE_OK;
	}
	return CORE_FAIL;
}

#if 0
static void fota_manager(void * pvParameter) {
	CORE_LOGI(TAG, "fota_manager task started");
	FotaInfo fota_info;
	ThreadHandle fota_task_handle;

	while (1) {
		/* Check queue, if no items then continue loop */
		if (QueueMessagesWaiting(m_fhd.fota_sch_q) <= 0) {
			TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
			continue;
		}
		/* Get Fota Info from queue */
		QueueReceive(m_fhd.fota_sch_q, &fota_info, THREADING_MAX_DELAY);

		BaseType xReturned = TaskCreate(&fota_task, "fota-task", TASK_STACK_SIZE_8K, &fota_info, THREAD_PRIORITY_5,
				&m_fhd.fota_thread);
		if (thread_stat == false) {
			CORE_LOGE(TAG, "Failed to create fota task thread");
//			return CORE_FAIL;
		}

		/* Get Fota Task completion status from queue, wait indefinitely */
		QueueReceive(m_fhd.fota_task_q, &fota_task_handle, THREADING_MAX_DELAY);
		if (fota_task_handle == m_fhd.fota_thread) {
			TaskDelete(m_fhd.fota_thread);
		}
	}
}

core_err start_fota_manager() {
	if (m_fhd.fota_manager_init_stat) {
		return CORE_OK;
	}

	/* Create a FOTA Scheduling Queue to receive new FOTA notification from sys-heartbeat */
	m_fhd.fota_sch_q = QueueCreate(MAX_FOTA_REQUEST_COUNT, sizeof(FotaInfo));
	if (m_fhd.fota_sch_q == 0) {
		CORE_LOGE(TAG, "Failed to create FOTA Scheduling Queue");
		return CORE_FAIL;
	}

	/* Create a queue to get the Thread ID of the FOTA task which has completed. This has to be deleted */
	m_fhd.fota_task_q = QueueCreate(1, sizeof(ThreadHandle));
	if (m_fhd.fota_task_q == 0) {
		CORE_LOGE(TAG, "Failed to create Thread ID queue");
		return CORE_FAIL;
	}

	/* Create the FOTA manager thread */
	BaseType xReturned = TaskCreate(&fota_manager, "fota-manager", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_8, NULL);
	if (thread_stat == false) {
		CORE_LOGE(TAG, "Failed to create fota manager thread");
		return CORE_FAIL;
	}

	m_fhd.fota_manager_init_stat = true;

	return CORE_OK;
}
#endif
