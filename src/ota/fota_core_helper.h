/*
 * fota_core_helper.h
 *
 *  Created on: 07-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef FOTA_CORE_HELPER_H_
#define FOTA_CORE_HELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "fota_core.h"

/* Helper function to notify FOTA statuses to the server */
core_err _send_fota_status_to_server(char *fota_id, FOTA_STATUSES fota_stat, int fota_try);
core_err _save_fota_identifier(char *fota_id);

#ifdef __cplusplus
}
#endif

#endif /* FOTA_CORE_HELPER_H_ */
