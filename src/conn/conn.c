/*
 * File Name: conn.c
 * File Path: /emmate/src/conn/conn.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#include "conn.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"
#include "system.h"
#include "system_utils.h"
#include "event_group_core.h"
#include "system_hmi_led_notification.h"
#include "tcpip_adapter_core.h"
#include "module_thread_priorities.h"

#define TAG	LTAG_CONN

static ConnData _conndata;

core_err init_network(DeviceConfig *dev_cfg) {
	core_err ret = CORE_FAIL;

#if CONFIG_USE_WIFI
	ret = init_wifi_sta_mode(&dev_cfg->wifi_cfg);
#endif

	return ret;
}

core_err connect_to_network(DeviceConfig *dev_cfg) {
	core_err ret = CORE_FAIL;
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
	ret = connect_wifi();
#endif

#if IFACE_NBIOT
#endif

#if IFACE_GSM
#endif
	return ret;
}

NETWORK_CONN_STATUS get_network_conn_status() {
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
	WIFI_CONNECTION_STATUS stat = get_wifi_connection_status();
	switch (stat) {
	case WIFI_CONNECTING:
		_conndata.netstat = NETCONNSTAT_CONNECTING;
		break;
	case WIFI_CONNECTED:
		_conndata.netstat = NETCONNSTAT_CONNECTED;
		break;
	case WIFI_NOT_CONNECTED:
		_conndata.netstat = NETCONNSTAT_DISCONNECTED;
		break;
	}
#endif

#if IFACE_NBIOT
#endif

#if IFACE_GSM
#endif
	return _conndata.netstat;
}

static void network_connection_thread(void * params) {
	DeviceConfig *dev_cfg = (DeviceConfig*) params;

	/* 1. Initialize the network */
	core_err ret = init_network(dev_cfg);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize the network");
		goto kill_task;
	}

	/* 2. Connect to network using configured network interface */
	ret = connect_to_network(dev_cfg);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to connect to network");
		goto kill_task;
	}

	show_network_connecting_notification();

	EventBits ebits_in = (CONN_CONNECTING_BIT | CONN_GOT_IP_BIT | CONN_GOT_IP_FAILED);
	EventBits ebits_out;

	/* 3. Wait until we get an IP */
	ebits_out = event_group_wait_bits(get_system_evtgrp_hdl(), ebits_in, false, false, EventMaxDelay);

	/* Either we will get an IP or fail to associate with network, so we check for both conditions */
	if ((ebits_out & ebits_in) == CONN_CONNECTING_BIT) {
		CORE_LOGD(TAG, "1. Network is connecting");
		event_group_clear_bits(get_system_evtgrp_hdl(), CONN_CONNECTING_BIT);
		show_network_connecting_notification();
	} else if ((ebits_out & ebits_in) == CONN_GOT_IP_BIT) {
		CORE_LOGD(TAG, "show_network_connected_notification");
		show_network_connected_notification();

		/* Get the IP from the active interface and print the IP address */
		TcpipAdapterIpInfo_Core ip_info;
		if (is_tcpip_core_netif_up(CORE_TCPIP_ADAPTER_IF_STA)) {
			ret = get_tcpip_ip_info(CORE_TCPIP_ADAPTER_IF_STA, &ip_info);
			if (ret == CORE_OK)
				CORE_LOGI(TAG, "1. WiFi connected, IP: %s", ip4addr_ntoa(&ip_info.ip));
		} else if (is_tcpip_core_netif_up(CORE_TCPIP_ADAPTER_IF_ETH)) {
			ret = get_tcpip_ip_info(CORE_TCPIP_ADAPTER_IF_ETH, &ip_info);
			if (ret == CORE_OK)
				CORE_LOGI(TAG, "Eth connected, IP: %s", ip4addr_ntoa(&ip_info.ip));
		}

	} else if ((ebits_out & ebits_in) == CONN_GOT_IP_FAILED) {
		CORE_LOGD(TAG, "show_network_notconnected_notification\r\n");
		show_network_connecting_notification();
		/* Network could not connect to specified configurations, so we need to do CONFIG_MODE_L1 again */
		CORE_LOGI(TAG, "1. Could not get IP from network!\r\n");
	}

	while (1) {
		ebits_out = event_group_wait_bits(get_system_evtgrp_hdl(), ebits_in, false, false, EventMaxDelay);

		/* Either we will get an IP or fail to associate with network, so we check for both conditions */
		if ((ebits_out & ebits_in) == CONN_CONNECTING_BIT) {
			CORE_LOGD(TAG, "2. Network is connecting");
		} else if ((ebits_out & ebits_in) == CONN_GOT_IP_BIT) {
			CORE_LOGD(TAG, "2. Network is connected");
//			show_network_connected_notification();
		} else if ((ebits_out & ebits_in) == CONN_GOT_IP_FAILED) {
			CORE_LOGD(TAG, "2. Network connection failed!");
//			show_network_notconnected_notification();
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
			/* Try to reconnect the wifi */
			reconnect_wifi();
#endif

#if IFACE_NBIOT
#endif

#if IFACE_GSM
#endif
		}
		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	}

	kill_task:
//	if (ret == CORE_OK)
//		show_network_connected_notification();
//	else
//		show_network_notconnected_notification();

	/* Kill the task */
	TaskDelete(NULL);
}

core_err start_network_connection(DeviceConfig *dev_cfg) {
	core_err ret = CORE_FAIL;

	_conndata.netstat = NETCONNSTAT_DISCONNECTED;

	/* Create a thread to perform hardware identification */
	BaseType thread_stat;
	thread_stat = TaskCreate(network_connection_thread, "net-conn", TASK_STACK_SIZE_3K, dev_cfg, THREAD_PRIORITY_CONN,
			NULL);
	if (thread_stat == false) {
		CORE_LOGE(TAG, "Failed to create thread!");
		ret = CORE_FAIL;
	} else {
		ret = CORE_OK;
	}

	return ret;
}
