/*
 * http_client_api.h
 *
 *  Created on: 31-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef HTTP_CLIENT_API_H_
#define HTTP_CLIENT_API_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#include "http_client_core.h"
#include "http_constant.h"

/**
 * @brief      	This is a single point function for HTTP GET or POST operation. This is a blocking function and will not
 * 				return until the requested HTTP operation has been completed. The application needs to pass all the params
 * 				and wait for the function to return. This function will take care of sending the HTTP GET/POST request and
 * 				getting the response data and then returning.
 *
 * @note		Please check all parameters carefully
 *
 * @param[in]  url				HTTP URL
 * @param[in]  port				HTTP Port number
 * @param[in]  cert_pem			Required for HTTPS connections. If doing HTTP then pass NULL
 * @param[in]  method			HTTP_CLIENT_METHOD_GET or HTTP_CLIENT_METHOD_POST
 * @param[in]  host_server		HTTP Host name
 * @param[in]  user_agent		HTTP User agent
 * @param[in]  content_type		HTTP Content type
 * @param[in]  post_data		Data to be posted
 * @param[in]  post_data_len	Length of data to be posted
 * @param[out] response_data	Pointer to the response data (if any). Memory must be allocated by caller.
 * 								Pass NULL if response is not needed
 * @param[out] response_len		Pointer to the response data length. Pass NULL if response is not needed
 * @param[in]  max_response_len	Maximum number of bytes expected in the response.
 * 								This value must be greater than or equal to the size of allocated memory for response_data.
 * 								Pass 0 if response is not needed.
 * @param[out] http_stat		HTTP status code
 *
 * @return
 *     - CORE_OK		If the complete operation was successful
 *     - CORE_FAIL		If any failure occured in between
 */

core_err do_http_operation(char* url, int port, char *cert_pem, HTTP_CLIENT_METHOD method, char* host_server,
		char* user_agent, char* content_type, char* post_data, size_t post_data_len, char* response_data,
		size_t *response_len, size_t max_response_len, uint16_t *http_stat);

#ifdef __cplusplus
}
#endif

#endif /* HTTP_CLIENT_API_H_ */
