/*
 * ble_config.c
 *
 *  Created on: 15-Apr-2019
 *      Author: iqubuntu02
 */

#include "ble.h"
#include "ble_config.h"

#include "core_logger.h"
#include "ble_constant.h"
#include "core_constant.h"
#include "hmi.h"
#include "module_thread_priorities.h"
#include "system_hmi_led_notification.h"
#include "logger_constants.h"

#define TAG LTAG_CONN_BLECONFIG

static TickType ble_curr_tick = 0;

static char* ble_auth_id = NULL;

static BLE_PROCESS_DATA ble_process_data = {
		.ble_auth_status = BLE_AUTH_NOT_DONE,
		.ble_config_process_status = BLE_CONFIGURATION_ON_IDLE_STATE,
		.ble_data_process_stat = BLE_DATA_PROCEESS_NOT_DONE,
		.is_anonymous_ble_app = false };

ThreadHandle ble_task_xHandle = NULL;
static bool kill_itself = false;

/**
 * @brief
 *
 * @return
 *
 **/
static void ble_config_task(void * pvParameters) {
	core_err err = CORE_FAIL;

	while (1) {
		if (kill_itself == true) { // TODO: to be changed to event group
			goto KILL_ITSELF;
		}
		if (isbleconnected() == BLE_CONNECTED) {
			if (BLE_AUTH_NOT_DONE == ble_process_data.ble_auth_status) {
				if (TaskGetTickCount() - ble_curr_tick >= (BLE_TIMEOUT_DURATION / TICK_RATE_TO_MS)) {
					CORE_LOGW(TAG, "anonymous BLE user");
					ble_process_data.is_anonymous_ble_app = true;
					err = ble_send_response(BLE_TIMEOUT_MSG);
					if (disable_ble() == CORE_OK) {
						TaskDelay(DELAY_500_MSEC/TICK_RATE_TO_MS);
						if (enable_ble() == CORE_OK) {

						} else {
							CORE_LOGE(TAG, "Failed Enable BLE");
							// TODO: don't know yet what is to be done
						}
					}
				} else {
					if (ble_process_data.ble_data_process_stat == BLE_METADATA_RECV_DONE) {

					}
				}
			}
			if (BLE_AUTH_FAILED == ble_process_data.ble_auth_status) {
				CORE_LOGI(TAG, "Ble Authentication Failed");
				err = ble_send_response(BLE_AUTHENTICATION_FAILED);
				//printf("err..__ = %d, %s\r\n", err, (err == CORE_OK) ? "CORE_OK" : "!CORE_OK");
				ble_process_data.ble_auth_status = BLE_AUTH_NOT_DONE;
				if (disable_ble() == CORE_OK) {
					TaskDelay(DELAY_500_MSEC/TICK_RATE_TO_MS);
					if (enable_ble() == CORE_OK) {
					} else {
						CORE_LOGE(TAG, "Failed Enable BLE");
						// TODO: don't know yet what is to be done
					}
				}

			} else if (BLE_AUTH_SUCCESS == ble_process_data.ble_auth_status) {
				// TODO: send Message to BLE App for Authentication Success
				CORE_LOGI(TAG, "Ble Authentication Success");
				ble_process_data.ble_auth_status = BLE_AUTHENTICATED;
				err = ble_send_response(BLE_METADATA_RECV_SUCCESS);
			} else if (BLE_AUTHENTICATED == ble_process_data.ble_auth_status) {
				if (ble_process_data.ble_data_process_stat == BLE_BYTEDATA_RECV_DONE) {
					err = ble_send_response(BLE_DATA_RECV_SUCCESS);
					ble_process_data.ble_data_process_stat = BLE_DATA_PROCEESS_NOT_DONE;

				} else if (ble_process_data.ble_data_process_stat == BLE_BYTEDATA_RECV_FAILED) {
					err = ble_send_response(BLE_DATA_RECV_FAILED);
					ble_process_data.ble_auth_status = BLE_AUTH_NOT_DONE;
					ble_process_data.ble_data_process_stat = BLE_DATA_PROCEESS_NOT_DONE;
					if (disable_ble() == CORE_OK) {
						TaskDelay(DELAY_500_MSEC/TICK_RATE_TO_MS);
						if (enable_ble() == CORE_OK) {
						} else {
							CORE_LOGE(TAG, "Failed Enable BLE");
							// TODO: restart module
						}
					}
				} else if (ble_process_data.ble_data_process_stat == BLE_SEND_RES_DEVCFG_DONE) {
					/* Send message the configuration is successfully done */
					err = ble_send_response(NET_CONNECTED_MSG);
					//ble_process_data.ble_data_process_stat = BLE_DATA_PROCEESS_NOT_DONE;
					goto KILL_ITSELF;
				} else if (ble_process_data.ble_data_process_stat == BLE_SEND_RES_DEVCFG_FAILED) {
					/* Send message the configuration has failed */
					err = ble_send_response(NET_CONNECTION_FAIL_MSG);
					//ble_process_data.ble_data_process_stat = BLE_DATA_PROCEESS_NOT_DONE;
					goto KILL_ITSELF;
				}
			}
		} else {
			set_ble_ongoing_process_stat(BLE_CONFIGURATION_ON_IDLE_STATE);
			ble_curr_tick = TaskGetTickCount();
			ble_process_data.ble_auth_status = BLE_AUTH_NOT_DONE;
			ble_process_data.ble_data_process_stat = BLE_DATA_PROCEESS_NOT_DONE;
		}

		TaskDelay(DELAY_100_MSEC/TICK_RATE_TO_MS);
	}
	KILL_ITSELF: {
		kill_itself = false;
		ble_task_xHandle = NULL;

		/* BLE configuration process is done. Now, disable & deinit the BLE device and kill the ble_config thread */
		if (disable_ble() == CORE_OK) {

			if(ble_process_data.ble_data_process_stat == BLE_SEND_RES_DEVCFG_DONE){
				show_network_connected_notification();
			}else if(ble_process_data.ble_data_process_stat == BLE_SEND_RES_DEVCFG_FAILED){
				show_network_connecting_notification();
			}
			//reset the BLE data process status
			ble_process_data.ble_data_process_stat = BLE_DATA_PROCEESS_NOT_DONE;
			TaskDelay(DELAY_500_MSEC/TICK_RATE_TO_MS);
			if (deinit_ble() != CORE_OK) {
				CORE_LOGE(TAG, "Failed to deinit the BLE");
			}
		} else {
			CORE_LOGE(TAG, "Failed to disable the BLE");
		}
		TaskDelete(NULL);
	}
}

/***************************************************************************************************/

/**
 * @brief
 *
 * @return
 *
 **/
core_err init_ble_cfg(QueueHandle* recv_queue, char* ble_adv_name, char* ble_authid) {
	core_err err = CORE_FAIL;
	// TODO: init and enable ble

	err = set_ble_adv_name(ble_adv_name);
	if (CORE_OK == err) {
		err = init_ble();
	} else {
		CORE_LOGE(TAG, "Failed set BLE Advertisement name");
	}

	if (CORE_OK == err) {
		err = enable_ble();
	} else {
		CORE_LOGE(TAG, "Failed configure the BLE");
	}

	if (CORE_OK == err) {
		ble_platform_set_ble_dt_recv_q(recv_queue);
	} else {
		CORE_LOGE(TAG, "Failed enable the BLE");
	}

	if (NULL != ble_authid) {
		ble_auth_id = ble_authid;
	} else {
		err = CORE_FAIL;
	}

	return err;
}

/**
 * @brief
 *
 * @return
 *
 **/
core_err start_ble_cfg() {
	core_err err = CORE_FAIL;
	BaseType xReturned = TaskCreate(ble_config_task, "ble_cnf_task", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_BLE_CONFIG,
			&ble_task_xHandle);
	if (true != xReturned) {
		CORE_LOGE(TAG, "failed to create ble_task thread!");
	} else {
		err = CORE_OK;
	}
	return err;
}

/**
 * @brief
 *
 * @return
 *
 **/
void set_ble_auth_stat(BLE_AUTH_STATUS stat) {
	ble_process_data.ble_auth_status = stat;
}

/**
 * @brief
 *
 * @return
 *
 **/
BLE_AUTH_STATUS get_ble_auth_stat() {
	return ble_process_data.ble_auth_status;
}

/**
 * @brief
 *
 * @return
 *
 **/
void set_ble_ongoing_process_stat(BLE_CONFIGURATION_PROCESS_STATUS stat) {
	ble_process_data.ble_config_process_status = stat;
}

/**
 * @brief
 *
 * @return
 *
 **/
BLE_CONFIGURATION_PROCESS_STATUS get_ble_ongoing_process_stat() {
	return ble_process_data.ble_config_process_status;
}

/**
 * @brief
 *
 * @return
 *
 **/
void set_ble_data_process_stat(BLE_DATA_PROCESS_STATUS status) {
	ble_process_data.ble_data_process_stat = status;
}

/**
 * @brief
 *
 * @return
 *
 **/
void stop_ble_config() {
	kill_itself = true;
}
