/*
 * File Name: wifi.h
 * File Path: /emmate/src/conn/wifi/wifi.h
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef WIFI_H_
#define WIFI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_common.h"
#include "core_constant.h"

#include "wifi_constants.h"
#include "wifi_platform.h"
#if CONFIG_USE_EVENT_GROUP
#include "event_group_core.h"
#endif

#define WIFI_EVENT_WIFI_READY					WIFI_PLAT_EVENT_WIFI_READY				/**< WiFi ready */
#define WIFI_EVENT_SCAN_DONE					WIFI_PLAT_EVENT_SCAN_DONE				/**< WiFi finish scanning AP */
#define WIFI_EVENT_STA_START					WIFI_PLAT_EVENT_STA_START				/**< WiFi station start */
#define WIFI_EVENT_STA_STOP						WIFI_PLAT_EVENT_STA_STOP				/**< WiFi station stop */
#define WIFI_EVENT_STA_CONNECTED				WIFI_PLAT_EVENT_STA_CONNECTED			/**< WiFi station connected to AP */
#define WIFI_EVENT_STA_DISCONNECTED				WIFI_PLAT_EVENT_STA_DISCONNECTED		/**< WiFi station disconnected from AP */
#define WIFI_EVENT_STA_AUTHMODECHANGE			WIFI_PLAT_EVENT_STA_AUTHMODECHANGE		/**< the auth mode of AP connected by WiFi station changed */
#define WIFI_EVENT_STA_GOT_IP					WIFI_PLAT_EVENT_STA_GOT_IP				/**< WiFi station got IP from connected AP */
#define WIFI_EVENT_STA_LOST_IP					WIFI_PLAT_EVENT_STA_LOST_IP				/**< WiFi station lost IP and the IP is reset to 0 */
#define WIFI_EVENT_STA_WPS_ER_SUCCESS			WIFI_PLAT_EVENT_STA_WPS_ER_SUCCESS		/**< WiFi station wps succeeds in enrollee mode */
#define WIFI_EVENT_STA_WPS_ER_FAILED			WIFI_PLAT_EVENT_STA_WPS_ER_FAILED		/**< WiFi station wps fails in enrollee mode */
#define WIFI_EVENT_STA_WPS_ER_TIMEOUT			WIFI_PLAT_EVENT_STA_WPS_ER_TIMEOUT		/**< WiFi station wps timeout in enrollee mode */
#define WIFI_EVENT_STA_WPS_ER_PIN				WIFI_PLAT_EVENT_STA_WPS_ER_PIN			/**< WiFi station wps pin code in enrollee mode */
#define WIFI_EVENT_AP_START						WIFI_PLAT_EVENT_AP_START				/**< WiFi soft-AP start */
#define WIFI_EVENT_AP_STOP						WIFI_PLAT_EVENT_AP_STOP					/**< WiFi soft-AP stop */
#define WIFI_EVENT_AP_STACONNECTED				WIFI_PLAT_EVENT_AP_STACONNECTED			/**< a station connected to WiFi soft-AP */
#define WIFI_EVENT_AP_STADISCONNECTED			WIFI_PLAT_EVENT_AP_STADISCONNECTED		/**< a station disconnected from WiFi soft-AP */
#define WIFI_EVENT_AP_STAIPASSIGNED				WIFI_PLAT_EVENT_AP_STAIPASSIGNED		/**< WiFi soft-AP assign an IP to a connected station */
#define WIFI_EVENT_AP_PROBEREQRECVED			WIFI_PLAT_EVENT_AP_PROBEREQRECVED		/**< Receive probe request packet in soft-AP interface */
#define WIFI_EVENT_GOT_IP6						WIFI_PLAT_EVENT_GOT_IP6					/**< WiFi station or ap or ethernet interface v6IP addr is preferred */
#define WIFI_EVENT_ETH_START					WIFI_PLAT_EVENT_ETH_START				/**< WiFi ethernet start */
#define WIFI_EVENT_ETH_STOP						WIFI_PLAT_EVENT_ETH_STOP				/**< WiFi ethernet stop */
#define WIFI_EVENT_ETH_CONNECTED				WIFI_PLAT_EVENT_ETH_CONNECTED			/**< WiFi ethernet phy link up */
#define WIFI_EVENT_ETH_DISCONNECTED				WIFI_PLAT_EVENT_ETH_DISCONNECTED		/**< WiFi ethernet phy link down */
#define WIFI_EVENT_ETH_GOT_IP					WIFI_PLAT_EVENT_ETH_GOT_IP				/**< WiFi ethernet got IP from connected AP */
#define WIFI_EVENT_MAX							WIFI_PLAT_EVENT_MAX

typedef enum {
	WIFI_CORE_MODE_NULL = WIFI_PLATFORM_MODE_NULL, /**< null mode */
	WIFI_CORE_MODE_STA = WIFI_PLATFORM_MODE_STA, /**< WiFi station mode */
	WIFI_CORE_MODE_AP = WIFI_PLATFORM_MODE_AP, /**< WiFi soft-AP mode */
	WIFI_CORE_MODE_APSTA = WIFI_PLATFORM_MODE_APSTA, /**< WiFi station + soft-AP mode */
	WIFI_CORE_MODE_MAX = WIFI_PLATFORM_MODE_MAX
} WifiCoreModes;

typedef enum {
	WIFI_CONNECTING = 101, WIFI_CONNECTED, WIFI_NOT_CONNECTED
} WIFI_CONNECTION_STATUS;

typedef struct {
	char wifi_ssid[WIFI_SSID_LEN]; /**< WiFi SSID */
	char wifi_pwd[WIFI_PWD_LEN]; /**< WiFi Password */
	uint16_t wifi_ssid_len; /**< Length of WiFi SSID */
	uint16_t wifi_pwd_len; /**< Length of WiFi PWD*/
} WifiConfig;

typedef struct {
	uint8_t config_count;
	WifiConfig cfg[CONFIG_MAX_NUM_WIFI_CONFIGS];
} WiFiCredentials;

/**
 * @brief	This function returns the WiFi SSID that the system is currently connected to.
 *
 * @return
 * 		- char*		SSID
 **/
char * get_wifi_connected_ssid();

/**
 * @brief	start and activate the WiFi station.
 *
 * @param	wifi_cfg pass the WiFi Configuration data (SSID, Password)
 *
 * @return
 * 		- CORE_OK 	on sucess
 *		- CORE_FAIL on fail
 **/
//core_err init_wifi_sta_mode(WifiConfig *wifi_cfg);
core_err init_wifi_sta_mode();

/**
 * @brief	Try to reconnect the wifi
 *
 * @return
 * 		- CORE_OK 	on sucess
 *		- CORE_FAIL on fail
 *
 **/
void reconnect_wifi();

/**
 * @brief	start to make a connection with the configured WiFi
 *
 * @return
 * 		- CORE_OK 	on sucess
 *		- CORE_FAIL on fail
 *
 **/
core_err connect_wifi();

/**
 * @brief     	Deletes all saved WiFiCredentials from memory and from storage
 *
 * @return
 * 	- CORE_OK: succeed
 * 	- CORE_FAIL on fail
 */
core_err delete_wifi_config();

/**
 * @brief     	This function adds the input WiFi configuration to the WiFiCredentials array
 * 			and then saves the WiFiCredentials in persistent memory
 *
 * @param[in]  wifi_cfg  WifiConfig object
 *
 * @return
 * 	- CORE_OK: succeed
 * 	- CORE_FAIL on fail
 */
core_err save_wifi_config(WifiConfig *wifi_cfg);

/**
 * @brief     	Reads the wifi credentials from the persistent memory.
 * 			This function is specifically used by the wifi module and the data read is kept within the module.
 * 			To get the stored wifi credentials call get_wifi_credentials()
 *
 * @return
 * 	- CORE_OK: succeed
 * 	- CORE_FAIL on fail
 */
core_err read_wifi_credentials();

/**
 * @brief     	Returns the address of WiFi module's WiFiCredentials structure.
 * 			save_wifi_config() must be called with valid configurations else NULL will be returned
 *
 * @return
 * 	- Address of WiFiCredentials structure
 * 	- NULL if there is no WifiConfig saved.
 */
WiFiCredentials * get_wifi_credentials();

/**
 * @brief	Deinitialize the Wi-Fi module
 *
 * @return
 * 		- CORE_OK 	on sucess
 *		- CORE_FAIL on fail
 *
 **/
core_err deinit_wifi();

/**
 * @brief     Get the Wi-Fi STA interface's MAC address
 *
 * @param[out]  mac  WiFi STA interface's MAC address (6 bytes) returned as an out parameter.
 *
 * @return
 * 	- CORE_OK: succeed
 * 	- CORE_FAIL on fail
 */
core_err get_wifi_sta_mac(uint8_t *mac);

/**
 * @brief     Get the Wi-Fi's Connection status
 *
 * @return
 * 	- WIFI_NOT_CONNECTED	on Wi-Fi not-connected,
 * 	- WIFI_CONNECTED		on Wi-Fi connected
 */
WIFI_CONNECTION_STATUS get_wifi_connection_status();

#ifdef __cplusplus
}
#endif

#endif /* WIFI_H_ */
