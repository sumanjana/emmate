/*
 * core_logger.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef CORE_LOGGER_H_
#define CORE_LOGGER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include "core_config.h"
#include "core_error.h"
#include "core_common.h"
#include "core_constant.h"
#include "platform_log.h"

/* Outputs where the log must go */
#define CORE_LOG_OUTPUT_CONSOLE		0b00000001	/**< Macro to set the log output to UART */
#define CORE_LOG_OUTPUT_FILE		0b00000010	/**< Macro to set the log output to a file. The file-appender module must be enabled for this option */
#define CORE_LOG_OUTPUT_NETWORK		0b00000100	/**< Macro to set the log output to the network. The udp-log-streamer module must be enabled for this option */

/**
 * @brief Log level
 *
 */
typedef enum {
    CORE_LOG_NONE 		= PF_LOG_NONE,   /**< No log output */
    CORE_LOG_ERROR		= PF_LOG_ERROR,  /**< Critical errors, software module can not recover on its own */
    CORE_LOG_WARN		= PF_LOG_WARN,   /**< Error conditions from which recovery measures have been taken */
    CORE_LOG_INFO		= PF_LOG_INFO,   /**< Information messages which describe normal flow of events */
    CORE_LOG_DEBUG		= PF_LOG_DEBUG,  /**< Extra information which is not necessary for normal use (values, pointers, sizes, etc). */
    CORE_LOG_VERBOSE	= PF_LOG_VERBOSE /**< Bigger chunks of debugging information, or frequent messages which can potentially flood the output. */
} CORE_LOG_LEVEL;


/* Platform logging MACRO wrappers. Can be called directly from other modules and application */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * @brief Log a buffer of hex bytes at specified level, separated into 16 bytes each line.
 *
 * @param  tag      description tag
 * @param  buffer   Pointer to the buffer array
 * @param  buff_len length of buffer in bytes
 * @param  level    level of the log
 *
 */
#define CORE_LOG_BUFFER_HEX_LEVEL(tag, buffer, buff_len, level)	PF_LOG_BUFFER_HEX_LEVEL(tag, buffer, buff_len, level)

/**
 * @brief Log a buffer of characters at specified level, separated into 16 bytes each line. Buffer should contain only printable characters.
 *
 * @param  tag      description tag
 * @param  buffer   Pointer to the buffer array
 * @param  buff_len length of buffer in bytes
 * @param  level    level of the log
 *
 */
#define CORE_LOG_BUFFER_CHAR_LEVEL(tag, buffer, buff_len, level)	PF_LOG_BUFFER_CHAR_LEVEL(tag, buffer, buff_len, level)

/**
 * @brief Dump a buffer to the log at specified level.
 *
 * The dump log shows just like the one below:
 *
 *      W (195) log_example: 0x3ffb4280   45 53 50 33 32 20 69 73  20 67 72 65 61 74 2c 20  |ESP32 is great, |
 *
 *      W (195) log_example: 0x3ffb4290   77 6f 72 6b 69 6e 67 20  61 6c 6f 6e 67 20 77 69  |working along wi|
 *
 *      W (205) log_example: 0x3ffb42a0   74 68 20 74 68 65 20 49  44 46 2e 00              |th the IDF..|
 *
 * It is highly recommend to use terminals with over 102 text width.
 *
 * @param tag description tag
 * @param buffer Pointer to the buffer array
 * @param buff_len length of buffer in bytes
 * @param level level of the log
 */
#define CORE_LOG_BUFFER_HEXDUMP(tag, buffer, buff_len, level)		PF_LOG_BUFFER_HEXDUMP(tag, buffer, buff_len, level)

/**
 * @brief Log a buffer of hex bytes at Info level
 *
 * @param  tag      description tag
 * @param  buffer   Pointer to the buffer array
 * @param  buff_len length of buffer in bytes
 *
 * @see ``CORE_LOG_BUFFER_HEX_LEVEL``
 *
 */
#define CORE_LOG_BUFFER_HEX(tag, buffer, buff_len)		PF_LOG_BUFFER_HEX(tag, buffer, buff_len)

/**
 * @brief Log a buffer of characters at Info level. Buffer should contain only printable characters.
 *
 * @param  tag      description tag
 * @param  buffer   Pointer to the buffer array
 * @param  buff_len length of buffer in bytes
 *
 * @see ``CORE_LOG_BUFFER_CHAR_LEVEL``
 *
 */
#define CORE_LOG_BUFFER_CHAR(tag, buffer, buff_len)		PF_LOG_BUFFER_CHAR(tag, buffer, buff_len)

/* macro to output logs in startup code, before heap allocator and syscalls have been initialized */
#define CORE_EARLY_LOGE(tag, format, ...)				PF_EARLY_LOGE( tag, format, ##__VA_ARGS__ )
#define CORE_EARLY_LOGW(tag, format, ...)				PF_EARLY_LOGW( tag, format, ##__VA_ARGS__ )
#define CORE_EARLY_LOGI(tag, format, ...)				PF_EARLY_LOGI( tag, format, ##__VA_ARGS__ )
#define CORE_EARLY_LOGD(tag, format, ...)				PF_EARLY_LOGD( tag, format, ##__VA_ARGS__ )
#define CORE_EARLY_LOGV(tag, format, ...)				PF_EARLY_LOGV( tag, format, ##__VA_ARGS__ )

/** runtime macro to output logs at a specified level.
 *
 * @param tag tag of the log, which can be used to change the log level by ``esp_log_level_set`` at runtime.
 * @param level level of the output log.
 * @param format format of the output log. see ``printf``
 * @param ... variables to be replaced into the log. see ``printf``
 *
 * @see ``printf``
 */
#define CORE_LOG_LEVEL(level, tag, format, ...)			PF_LOG_LEVEL( level, tag, format, ##__VA_ARGS__ )

/* runtime macro to output logs at a specified level. */
#define CORE_LOG_LEVEL_LOCAL(level, tag, format, ...)	PF_LOG_LEVEL_LOCAL( level, tag, format, ##__VA_ARGS__ )


/**
 * macro to output logs at CORE_LOG_ERROR level.
 *
 * @param tag tag of the log,
 *
 * @see ``printf``
 */
#define CORE_LOGE( tag, format, ... )				PF_LOGE( tag, format, ##__VA_ARGS__ )
/// macro to output logs at ``CORE_LOG_WARN`` level.  @see ``CORE_LOGE``
#define CORE_LOGW( tag, format, ... )				PF_LOGW( tag, format, ##__VA_ARGS__ )
/// macro to output logs at ``CORE_LOG_INFO`` level.  @see ``CORE_LOGE``
#define CORE_LOGI( tag, format, ... )				PF_LOGI( tag, format, ##__VA_ARGS__ )
/// macro to output logs at ``CORE_LOG_DEBUG`` level.  @see ``CORE_LOGE``
#define CORE_LOGD( tag, format, ... )				PF_LOGD( tag, format, ##__VA_ARGS__ )
/// macro to output logs at ``CORE_LOG_VERBOSE`` level.  @see ``CORE_LOGE``
#define CORE_LOGV( tag, format, ... )				PF_LOGV( tag, format, ##__VA_ARGS__ )


/* APIs function declaration */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * @brief	Set the output where the log is to be sent. Values ORed together to select multiple outputs.
 * 			Usage:
 * 			------
 * 			set_core_logger_output(CORE_LOG_OUTPUT_CONSOLE);
 * 			- OR -
 * 			set_core_logger_output(CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_FILE);
 * 			- OR -
 * 			set_core_logger_output(CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_NETWORK);
 *
 * @param[in] output	The output peripheral where the log needs to be sent.
 *
 * @return
 *		- CORE_OK       Successful.
 *  	- CORE_FAIL     Failed to init.
 * */
core_err set_core_logger_output(uint8_t output);

/**
 * @brief set_core_logger_level sets the specific platform logging module with a set of values as sent by the calling function
 *
 * @param[in] tag  		unique Tag name for  the modules
 * @param[in] level  	log level as in CORE_LOG_LEVEL enum
 *
 * @return
 *		- CORE_OK       Successful.
 *  	- CORE_FAIL     Failed to init.
 * */
void set_core_logger_level(const char *tag, CORE_LOG_LEVEL level);

/**
 * @brief enable_core_logger sets the all platform logging module with a set of values as sent into the function init_core_logger()
 *
 * */
void enable_core_logger();

/**
 * @brief disable_core_logger sets the all platform logging module with a set CORE_LOG_NONE
 *
 * */
void disable_core_logger();

/**
 * @brief init_core_logger sets the underlying platform logging module with a set of values as sent by the calling function
 *
 * @param[in] output  output peripheral like console, file network etc.
 * @param[in] level  log level as in CORE_LOG_LEVEL enum
 *
 * @return
 *		- CORE_OK       Successful.
 *  	- CORE_FAIL     Failed to init.
 * */
core_err init_core_logger(uint8_t output, CORE_LOG_LEVEL level);

#ifdef __cplusplus
}
#endif

#endif /* CORE_LOGGER_H_ */
