/*
 * persistent_mem_device_config.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#include "persistent_mem_device_config.h"
#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT

#if CONFIG_USE_DEVCONFIG

/*
 * read_devconfig_from_persistent_mem reads only the 1st level of device configurations from the persistent memory
 * The 1st level of device configurations will include the device id, and configs required for the
 * selected network interface. These configurations will be used to do the 1st level OTA.
 * */
core_err read_devconfig_from_persistent_mem(DeviceConfig *devcfg) {
	core_err ret = CORE_FAIL;

#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
//	if ((ret = persistent_read_config_by_key(WIFI_SSID_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_ssid,
//			devcfg->wifi_cfg.wifi_ssid_len)) == CORE_OK)
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
//
//	if ((ret = persistent_read_config_by_key(WIFI_PWD_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_pwd, devcfg->wifi_cfg.wifi_pwd_len))
//			== CORE_OK)
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);


	if ((ret = read_wifi_credentials()) == CORE_OK) {
		// TODO: Need to change the checklist functionality, now calling 2 times to support existing code
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
	}

#endif

#if IFACE_NBIOT
#endif

#if IFACE_GSM
#endif

//	if (ret == CORE_ERR_NVS_NOT_FOUND) {
//		ret = CORE_ERR_NOT_FOUND;
//	}
	return ret;
}

core_err write_devconfig_to_persistent_mem(DeviceConfig *devcfg) {
	core_err ret = CORE_FAIL;
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
//	if ((ret = persistent_write_config_by_key(WIFI_SSID_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_ssid,
//			devcfg->wifi_cfg.wifi_ssid_len)) == CORE_OK) {
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
//	}
//
//	if ((ret = persistent_write_config_by_key(WIFI_PWD_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_pwd,
//			devcfg->wifi_cfg.wifi_pwd_len)) == CORE_OK) {
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
//	}

	if ((ret = save_wifi_config(&devcfg->wifi_cfg)) == CORE_OK) {
		// TODO: Need to change the checklist functionality, now calling 2 times to support existing code
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
	}
#endif

#if IFACE_NBIOT
#endif

#if IFACE_GSM
#endif

#if AWS_CRED
#endif

//	if (ret == CORE_ERR_NVS_NOT_FOUND) {
//		ret = CORE_ERR_NOT_FOUND;
//	}
	return ret;
}
#endif /* CONFIG_USE_DEVCONFIG */
