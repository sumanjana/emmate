/*
 * core_nvs.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#include "core_nvs.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT_NVS

core_err nvs_initialize() {
	return init_nvs();
}

void nvs_deinitialize() {
	return deinit_nvs();
}

core_err nvs_erase() {
	return erase_nvs();
}

core_err nvs_read_by_key(char *key, void* value, size_t* bytes_read) {
	core_err ret = read_nvsdata_by_key(key, value, bytes_read);
	if (ret != CORE_OK) {
		if ((ret == PLAT_NVS_ERR_NOT_INITIALIZED) || (ret == PLAT_NVS_ERR_NOT_FOUND)) {
			// do nothing, so these error codes will be returned
		} else {
			// for ny other error, return failure
			ret = CORE_FAIL;
		}
	}
	return ret;
}

core_err nvs_write_by_key(char *key, void *value, size_t bytes_to_write) {
	return write_nvsdata_by_key(key, value, bytes_to_write);
}
