/*
 * File Name: core_nvs.h
 * File Path: /emmate/src/persistent/nvs/core_nvs.h
 * Description:
 *
 *  Created on: 21-May-2019
 *      Author: Rohan Dey
 */

#ifndef CORE_NVS_H_
#define CORE_NVS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "nvs_helper.h"
#include "nvs_helper_errors.h"

#define NVS_ERR_NOT_INITIALIZED			PLAT_NVS_ERR_NOT_INITIALIZED		/*!< The storage driver is not initialized */
#define NVS_ERR_NOT_FOUND				PLAT_NVS_ERR_NOT_FOUND				/*!< Id namespace doesn’t exist yet and mode is NVS_READONLY */
#define NVS_ERR_TYPE_MISMATCH			PLAT_NVS_ERR_TYPE_MISMATCH			/*!< The type of set or get operation doesn't match the type of value stored in NVS */
#define NVS_ERR_READ_ONLY				PLAT_NVS_ERR_READ_ONLY				/*!< Storage handle was opened as read only */
#define NVS_ERR_NOT_ENOUGH_SPACE		PLAT_NVS_ERR_NOT_ENOUGH_SPACE		/*!< There is not enough space in the underlying storage to save the value */
#define NVS_ERR_INVALID_NAME			PLAT_NVS_ERR_INVALID_NAME			/*!< Namespace name doesn’t satisfy constraints */
#define NVS_ERR_INVALID_HANDLE			PLAT_NVS_ERR_INVALID_HANDLE			/*!< Handle has been closed or is NULL */
#define NVS_ERR_REMOVE_FAILED			PLAT_NVS_ERR_REMOVE_FAILED			/*!< The value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again. */
#define NVS_ERR_KEY_TOO_LONG			PLAT_NVS_ERR_KEY_TOO_LONG			/*!< Key name is too long */
#define NVS_ERR_PAGE_FULL				PLAT_NVS_ERR_PAGE_FULL				/*!< Internal error; never returned by nvs API functions */
#define NVS_ERR_INVALID_STATE			PLAT_NVS_ERR_INVALID_STATE			/*!< NVS is in an inconsistent state due to a previous error. Call nvs_flash_init and nvs_open again, then retry. */
#define NVS_ERR_INVALID_LENGTH			PLAT_NVS_ERR_INVALID_LENGTH			/*!< String or blob length is not sufficient to store data */
#define NVS_ERR_NO_FREE_PAGES			PLAT_NVS_ERR_NO_FREE_PAGES			/*!< NVS partition doesn't contain any empty pages. This may happen if NVS partition was truncated. Erase the whole partition and call nvs_flash_init again. */
#define NVS_ERR_VALUE_TOO_LONG			PLAT_NVS_ERR_VALUE_TOO_LONG			/*!< String or blob length is longer than supported by the implementation */
#define NVS_ERR_PART_NOT_FOUND			PLAT_NVS_ERR_PART_NOT_FOUND			/*!< Partition with specified name is not found in the partition table */

#define NVS_ERR_NEW_VERSION_FOUND		PLAT_NVS_ERR_NEW_VERSION_FOUND		/*!< NVS partition contains data in new format and cannot be recognized by this version of code */
#define NVS_ERR_XTS_ENCR_FAILED			PLAT_NVS_ERR_XTS_ENCR_FAILED		/*!< XTS encryption failed while writing NVS entry */
#define NVS_ERR_XTS_DECR_FAILED			PLAT_NVS_ERR_XTS_DECR_FAILED		/*!< XTS decryption failed while reading NVS entry */
#define NVS_ERR_XTS_CFG_FAILED			PLAT_NVS_ERR_XTS_CFG_FAILED			/*!< XTS configuration setting failed */
#define NVS_ERR_XTS_CFG_NOT_FOUND		PLAT_NVS_ERR_XTS_CFG_NOT_FOUND		/*!< XTS configuration not found */
#define NVS_ERR_ENCR_NOT_SUPPORTED		PLAT_NVS_ERR_ENCR_NOT_SUPPORTED		/*!< NVS encryption is not supported in this version */
#define NVS_ERR_KEYS_NOT_INITIALIZED	PLAT_NVS_ERR_KEYS_NOT_INITIALIZED		/*!< NVS key partition is uninitialized */
#define NVS_ERR_CORRUPT_KEY_PART		PLAT_NVS_ERR_CORRUPT_KEY_PART		/*!< NVS key partition is corrupt */

#define NVS_ERR_CONTENT_DIFFERS			PLAT_NVS_ERR_CONTENT_DIFFERS		/*!< Internal error; never returned by nvs API functions.  NVS key is different in comparison */

/**
 * @brief This function initializes the underlying Non Volatile Storage/Memory (NVS)
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 **/
core_err nvs_initialize();

/**
 * @brief This function de-initializes the underlying Non Volatile Storage/Memory (NVS)
 *
 **/
void nvs_deinitialize();

/**
 * @brief This function erases the Non Volatile Storage/Memory (NVS). No saved data can be retrieved after calling this function
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 **/
core_err nvs_erase();

/**
 * @brief This function reads the data from the NVS against a particular key
 *
 * @param[in] 	key		The key against which the data has to be read
 * @param[out] 	value		The output buffer where the read data has to be stored
 * @param[out]	bytes_read	Number of bytes read from the memory
 *
 * @return
 * 		- CORE_OK					On success
 * 		- CORE_FAIL					On failure
 * 		- NVS_ERR_NOT_INITIALIZED	The NVS driver is not yet initialized
 * 		- NVS_ERR_NOT_FOUND 		If the key was not found
 *
 **/
core_err nvs_read_by_key(char *key, void* value, size_t* bytes_read);

/**
 * @brief This function writes the data to the NVS against the input key
 *
 * @param[in]	key 			An unique tag. Maximum length is 15 characters. Shouldn’t be empty.
 * @param[in]	value			Input data to write into NVS
 * @param[in]	bytes_to_write	Number of bytes to write into the NVS
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 **/
core_err nvs_write_by_key(char *key, void *value, size_t bytes_to_write);

#ifdef __cplusplus
}
#endif

#endif /* CORE_NVS_H_ */
