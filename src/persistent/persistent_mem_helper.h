/*
 * persistent_mem_helper.h
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_HELPER_H_
#define PERSISTENT_MEM_HELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"
#if CONFIG_USE_NVS
#include "core_nvs.h"
#endif
#if CONFIG_USE_SDMMC_HOST
#include "sdmmc_host_core.h"
#endif


/**
 * @brief 	This function reads the configuration against a particular key from the selected configuration storage
 * 			The selection of the configuration storage is done through the menuconfig tool before building an application
 *
 * @param[in] 	key		The key against which the configuration has to be read
 * @param[out] 	buf		The output buffer where the read configuration has to be stored
 * @param[in]	buf_len	Length of the input buffer in bytes
 *
 * @return
 * 		- CORE_OK				On success
 * 		- CORE_FAIL				On failure
 * 		- 						Any other error passed from the lower layers
 *
 **/
core_err persistent_read_config_by_key(char *key, void* buf, uint16_t buf_len);

/**
 * @brief 	This function writes the configuration against a particular key from the selected configuration storage
 * 			The selection of the configuration storage is done through the menuconfig tool before building an application
 *
 * @param[in] 	key		The key against which the configuration has to be written
 * @param[in] 	buf		The input buffer containing the data/configuration
 * @param[in]	buf_len	Number of bytes to write
 *
 * @return
 * 		- CORE_OK				On success
 * 		- CORE_FAIL				On failure
 * 		- 						Any other error passed from the lower layers
 *
 **/
core_err persistent_write_config_by_key(char *key, void* buf, size_t buf_len);

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_HELPER_H_ */
