/*
 * persistent_mem.c
 *
 *  Created on: 11-Apr-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include <stdlib.h>
#include "core_error.h"
#include "persistent_mem.h"
#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "persistent_mem_errors.h"
#if CONFIG_USE_DEVCONFIG
#include "device_config.h"
#endif
#if ((CONFIG_USE_SDMMC_HOST) && (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC))
#include "fatfs_helper.h"
#endif
#include "gpio_core.h"

#define TAG LTAG_PERSISTENT

/*
 * read_opermode_from_persistent_mem function reads only the operation mode from the persistent memory.
 * For the very first boot there will be no operating mode set in the memory. In this case this function will
 * set the current operating mode as CONFIG_MODE and write into the persistent memory.
 *
 * This function should be called right after systems boots so that the correct operating mode is selected.
 * */
uint8_t read_opermode_from_persistent_mem() {
	core_err ret = CORE_FAIL;

	uint8_t opermode;

	if ((ret = persistent_read_config_by_key(OPERMODE_NVS_KEY, (void*) &opermode, 1)) == CORE_OK) {
		return opermode;
	}
	/* If the OPERMODE_NVS_KEY is not found then set the opermode to CONFIG_MODE and write the same to the
	 * persistent memory */
	else if (ret == NVS_ERR_NOT_FOUND) {
		CORE_LOGI(TAG, "Operation mode not set. Setting current operation mode to CONFIG_MODE ");
		opermode = SYSTEM_CONFIG_MODE_FACTORY;
		if ((ret = persistent_write_config_by_key(OPERMODE_NVS_KEY, (void*) &opermode, sizeof(opermode))) == CORE_OK) {
			return opermode;
		} else {
			return CORE_FAIL;
		}
	} else {
		return CORE_FAIL;
	}
}

/*
 * write_opermode_to_persistent_mem function writes the opermode param into the persistent memory
 *
 * */
core_err write_opermode_to_persistent_mem(SYSTEM_OPERATION_MODES opermode) {
	core_err ret = CORE_FAIL;

	if ((ret = persistent_write_config_by_key(OPERMODE_NVS_KEY, (void*) &opermode, sizeof(opermode))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Error: write_opermode_to_persistent_mem failed!. %s, %d", (char*)__FILE__, __LINE__);
	}

	return ret;
}

core_err init_persistent_mem_for_config() {
	core_err ret = CORE_FAIL;

	int16_t config_mem_type = CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE;

	if (config_mem_type == CONFIG_PERSISTENT_MEMORY_NVS) {
		/* Initialize NVS */
		ret = nvs_initialize();
	} else if (config_mem_type == CONFIG_PERSISTENT_MEMORY_SDMMC) {
		// Initialize SD
	}

	return ret;
}

core_err init_persistent_mem_for_data_storage() {
	core_err ret = CORE_FAIL;

#if ((CONFIG_USE_SDMMC_HOST) && (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC))

#if CONFIG_PLATFORM_ESP_IDF	/* For ESP, SPI mode is used to save pins */

#else	/* For other platforms, SDMMC mode*/
	gpio_io_set_pull_mode(SDMMC_CMD, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D0, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D1, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D2, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D3, GPIO_IO_PULLUP_ONLY);
#endif
	// TODO: Card Detect pin has to be checked before calling this function and relevant error code has to be returned.
	ret = init_and_mount_fatfs_on_sdmmc(CONFIG_SDMMC_PARTITION_NAME, false, SDMMC_MAX_NUM_FILES_TO_OPEN);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to Initialize/Mount FAT File System on SD/MMC Card!");
		ret = CORE_FAIL;
	} else {
		CORE_LOGI(TAG, "Initialize/Mount FAT File System on SD Card succeeded");
	}
#elif ((CONFIG_USE_NVS) && (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS))
	/* NVS is already initialized when init_persistent_mem_for_config() is called */
#endif

	return ret;
}

core_err reset_persistent_mem_for_reconfig() {
	core_err ret = CORE_FAIL;
#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	ret = write_opermode_to_persistent_mem(SYSTEM_CONFIG_MODE_FACTORY);
#endif
	return ret;
}

core_err erase_configs_from_persistent_mem() {
	core_err ret = CORE_FAIL;
#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	ret = nvs_erase();
#endif
	return ret;
}
