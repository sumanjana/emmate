/*
 * persistent_mem_wifi.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */
#include "persistent_mem_wifi.h"
#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT

#if CONFIG_USE_WIFI
/*
 * Read the wifi credentials data from persistent memory
 */
core_err read_wifi_credentials_from_persistent_mem(WiFiCredentials *wifi_cred) {
	core_err ret = CORE_FAIL;

	if ((ret = persistent_read_config_by_key(WIFI_CRED_NVS_KEY, (void*) wifi_cred, sizeof(WiFiCredentials))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Reading WiFiCredentials from persistent memory failed");
	}
	return ret;
}

/*
 * Write the wifi credentials data into persistent memory
 */
core_err write_wifi_credentials_to_persistent_mem(WiFiCredentials *wifi_cred) {
	core_err ret = CORE_FAIL;

	if ((ret = persistent_write_config_by_key(WIFI_CRED_NVS_KEY, (void*) wifi_cred, sizeof(WiFiCredentials))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Writing WiFiCredentials to persistent memory failed");
	}
	return ret;
}
#endif
