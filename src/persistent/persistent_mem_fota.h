/*
 * persistent_mem_fota.h
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_FOTA_H_
#define PERSISTENT_MEM_FOTA_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"

#if CONFIG_USE_OTA
/**
 * @brief This function reads the FOTA ID from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	fota_id	Pointer to char to be populated and returned as an out parameter.
 * 						Length of somthing_id is defined by FOTA_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_fotaid_from_persistent_mem(char *fota_id);

/**
 * @brief This function writes the FOTA ID to the selected persistent memory
 *
 * @param[in]	fota_id	Pointer to char to be saved in the memory. Length of somthing_id is defined by FOTA_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_fotaid_to_persistent_mem(char *fota_id);
#endif


#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_FOTA_H_ */
