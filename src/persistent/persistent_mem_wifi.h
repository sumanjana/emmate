/*
 * persistent_mem_wifi.h
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_WIFI_H_
#define PERSISTENT_MEM_WIFI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"
#if CONFIG_USE_WIFI
#include "wifi.h"
#endif

#if CONFIG_USE_WIFI
/**
 * @brief This function reads the Wi-Fi Credentials from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	wifi_cred	Pointer to WiFiCredentials to be populated and returned as an out parameter
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_wifi_credentials_from_persistent_mem(WiFiCredentials *wifi_cred);

/**
 * @brief This function writes the Wi-Fi Credentials to the selected persistent memory
 *
 * @param[in]	wifi_cred	Pointer to WiFiCredentials to be saved in the memory
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_wifi_credentials_to_persistent_mem(WiFiCredentials *wifi_cred);
#endif

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_WIFI_H_ */
