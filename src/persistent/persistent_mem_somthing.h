/*
 * persistent_mem_somthing.h
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_SOMTHING_H_
#define PERSISTENT_MEM_SOMTHING_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"

/**
 * @brief This function reads the SOM Thing ID from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	somthing_id	Pointer to char to be populated and returned as an out parameter. Length of somthing_id is defined by SOMTHING_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_somthing_id_from_persistent_mem(char *somthing_id);

/**
 * @brief This function writes the SOM Thing ID to the selected persistent memory
 *
 * @param[in]	somthing_id	Pointer to char to be saved in the memory. Length of somthing_id is defined by SOMTHING_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_somthing_id_to_persistent_mem(char *somthing_id);

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_SOMTHING_H_ */
