/*
 * persistent_mem_api.h
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_API_H_
#define PERSISTENT_MEM_API_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"

/**
 * @brief 	Read data from persistent memory by a unique key/tag. The function persistent_write_data_by_key()
 * 			must be called with the same key to save data before calling this function
 *
 * @param[in] 	key		The key against which the stored data has to be read
 * @param[out] 	value	The output buffer where the read data has to be stored
 * @param[in]	buf_len	Length of the output buffer
 *
 * @return
 * 		- CORE_OK				On success
 * 		- CORE_FAIL				On failure
 */
core_err persistent_read_data_by_key(char *key, void* buf, size_t buf_len);

/**
 * @brief	Write data to the selected persistent memory using a unique key as an identifier. Later on the data can be retrieved
 * 			by persistent_read_data_by_key()
 *
 * @param[in]	key		An unique tag. Maximum length is 15 characters. Shouldn’t be empty.
 * @param[in]	buf		Data buffer
 * @param[in]	buf_len	Length of the input buffer
 *
 * @return
 * 		- CORE_OK				On success
 * 		- CORE_FAIL				On failure
 */
core_err persistent_write_data_by_key(char *key, void* buf, size_t buf_len);

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_API_H_ */
