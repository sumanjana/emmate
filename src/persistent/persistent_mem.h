/*
 * persistent_mem.h
 *
 *  Created on: 11-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_H_
#define PERSISTENT_MEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#if CONFIG_USE_NVS
#include "core_nvs.h"
#endif
#if CONFIG_USE_SDMMC_HOST
#include "sdmmc_host_core.h"
#endif

#include "system.h"

#include "persistent_mem_device_config.h"
#include "persistent_mem_wifi.h"
#include "persistent_mem_fota.h"
#include "persistent_mem_api.h"
#include "persistent_mem_somthing.h"

/**
 * @brief Maximum number of files that can be opened using FATFS in a SD/MMC card
 * */
#define SDMMC_MAX_NUM_FILES_TO_OPEN		CONFIG_MAX_NUM_FILES_TO_OPEN

/**
 * @brief 	Name of the partition to be created when the FATFS is mounted on a SD/MMC card
 * 			This partition will be used for the entire system. All files created will fall under
 * 			this partition
 * */
//#define CONFIG_SDMMC_PARTITION_NAME	"/emmate"

/**
 * @brief read_opermode_from_persistent_mem function reads only the operation mode from the persistent memory.
 * For the very first boot there will be no operating mode set in the memory. In this case this function will
 * set the current operating mode as CONFIG_MODE and write into the persistent memory.
 *
 * This function should be called right after systems boots so that the correct operating mode is selected.
 *
 * @return
 * 		- SYSTEM_OPERATION_MODES if value was get successfully
 *      - CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *      - CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *      - CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *      - CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 * */
uint8_t read_opermode_from_persistent_mem();

/**
 * @brief write_opermode_to_persistent_mem function writes the SYSTEM_OPERATION_MODES into the persistent memory
 *
 * @param[in]	opermode	SYSTEM_OPERATION_MODES value to set into persistent memory
 *
 * @return
 *		- CORE_OK if value was set successfully
 *      - CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *      - CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *      - CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *      - CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 * */
core_err write_opermode_to_persistent_mem(SYSTEM_OPERATION_MODES opermode);

/**
 * @brief initialize the all persistent memory modules
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 * */
core_err init_persistent_mem_for_config();

/**
 *	@brief 	Erases the current System Operation Mode and sets it to SYSTEM_CONFIG_MODE_FACTORY so that Device Configuration
 *			can be done again
 *	@return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 */
core_err reset_persistent_mem_for_reconfig();

/**
 *	@brief Erases all configurations from the persistent memory
 *	@return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 */
core_err erase_configs_from_persistent_mem();

/**
 *	@brief Initializes the selected storage to be used for storing data in the application
 *	@return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 */
core_err init_persistent_mem_for_data_storage();

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_H_ */
