# Persistent Module
## Overview
The Persistent module provides APIs to save/retrieve data into non-volatile memories like NVS/NVM, SDMMC, Flash etc.

## Features
- Data storage options - NVS, SDMMC Card (SPI/SD Card)
- Key (TAG) based data storing/retrieval to/from the Non-Volatile Flash memory (NVS/NVM)
- Standard file operations in SDMMC Cards with the help of file-system module

## How to use this module from an application

##### Header(s) to include

```
persistent_mem.h
```

#### Module Specific Configurations

The following configurations are needed for the External IC module to work. Please enable the requried configuration by refering the below image.

As an application developer, you only need to select the "Persistent Memory Type for Data Storage" option. Other configurations are internally used by the EmMate Framework and should not be altered.

![](img/persistent-config.png)


**If SD Card is selected**


![](img/persistent-config-1.png)

#### Module Specific APIs

**Important APIs to store and retrieve data**

- NVS memory - `persistent_read_data_by_key()` and `persistent_write_data_by_key()`
- SDMMC - For SD Card is selected from the configuration, by default the FAT File System is enabled. So standard file operations can be performed using C Library functions such as fopen, fread, fwrite, fclose etc.

