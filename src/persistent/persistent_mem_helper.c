/*
 * persistent_mem_helper.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#define TAG LTAG_PERSISTENT

core_err persistent_read_config_by_key(char *key, void* buf, uint16_t buf_len) {
	core_err ret = CORE_FAIL;
	size_t data_len = 0;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	memset(buf, 0x00, buf_len);
	if ((ret = nvs_read_by_key(key, buf, &data_len)) == CORE_OK) {
		// TODO:
	} else {
		CORE_LOGE(TAG, "persistent_read_config_by_key failed for [key] = %s!", key);
	}
#endif
	return ret;
}

core_err persistent_write_config_by_key(char *key, void* buf, size_t buf_len) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = nvs_write_by_key(key, buf, buf_len)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "persistent_write_config_by_key failed for [key] = %s!", key);
	}
#endif
	return ret;
}
