/*
 * persistent_mem_somthing.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */


#include "persistent_mem_somthing.h"
#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT

/*
 * Read the somthing id from persistent memory
 */
core_err read_somthing_id_from_persistent_mem(char *somthing_id) {
	core_err ret = CORE_FAIL;

	if ((ret = persistent_read_config_by_key(SOMTHING_ID_NVS_KEY, (void*) somthing_id, SOMTHING_ID_LEN)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Reading SOM-Thing ID from persistent memory failed");
	}
	return ret;
}

/*
 * Write the somthing id into persistent memory
 */
core_err write_somthing_id_to_persistent_mem(char *somthing_id) {
	core_err ret = CORE_FAIL;

	if ((ret = persistent_write_config_by_key(SOMTHING_ID_NVS_KEY, (void*) somthing_id, SOMTHING_ID_LEN)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Writing SOM-Thing ID to persistent memory failed");
	}
	return ret;
}
