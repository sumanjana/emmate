/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   single_led.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The a LED blink in different pattern using led-helper lib
 *
 *
 *
 */

#ifndef SINGLE_LED_H_
#define SINGLE_LED_H_

#include "core_config.h"
#include "core_constant.h"
#include "core_error.h"
#include "core_logger.h"
#include "threading.h"
#include "hmi.h"
#include "thing.h"


// Pattern-1 For Mono Led, Led On for 100msec, Off for 250msec and this pattern run for 1 time
#define PATTERN_1	make_led_pattern(COLOR_MONO,100,250,1)

// Pattern-2 For Mono Led, Led On for 500msec, Off for 500msec and this pattern run for 1 time
#define PATTERN_2	make_led_pattern(COLOR_MONO,500,500,1)

// Pattern-3 For Mono Led, Led On for 1000msec, Off for 1000msec and this pattern run for 3 time
#define PATTERN_3	make_led_pattern(COLOR_MONO, 1000, 500, 3)

/**
 * @brief	Init function for single_led module
 *
 */
void single_led_init();

/**
 * @brief	Execution function for single_led module
 *
 */
void single_led_loop();

#endif	/* SINGLE_LED_H_ */
