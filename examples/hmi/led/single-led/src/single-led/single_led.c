/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   single_led.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The a LED blink in different pattern using led-helper lib
 *
 *
 *
 */

#include "single_led.h"

#define BLINK_GPIO 		CUSTOM_LED_1

#define TAG "single_led"

/********************************************** Module's Static Functions **********************************************************************/

static void start_led_pattern_1() {
	core_err ret;
	int num_leds = 1;

	/* Creating and starting an LED notification consists of 4 steps. These steps must be followed
	 * in sequential manner, else behaviour of the library is undefied.
	 *
	 * Note: If you have multiple LEDs, then please follow Step 2 & 3, in the order as shown below.
	 *
	 * */

	/* Step 1: init_led_notification(num_led, period); */
	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_led_notification failed!", __func__);
		return;
	}

	/* Step 2: set_led_type_idx(led no., led_type, m_r_idx, g_idx, b_idx) */
	/* Step 3: map_patterns_to_led(led no., num_patterns, pattern1, pattern2, ...) */
	set_led_type_idx(num_leds, MONO_LED, BLINK_GPIO, 0, 0);
	map_patterns_to_led(num_leds, 1, PATTERN_1);

	/* Step 4: Start the led notification */
	start_led_notification();
}

static void start_led_pattern_2() {
	core_err ret;
	int num_leds = 1;

	/* Creating and starting an LED notification consists of 4 steps. These steps must be followed
	 * in sequential manner, else behaviour of the library is undefied.
	 *
	 * Note: If you have multiple LEDs, then please follow Step 2 & 3, in the order as shown below.
	 *
	 * */

	/* Step 1: init_led_notification(num_led, period); */
	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_led_notification failed!", __func__);
		return;
	}

	/* Step 2: set_led_type_idx(led no., led_type, m_r_idx, g_idx, b_idx) */
	/* Step 3: map_patterns_to_led(led no., num_patterns, pattern1, pattern2, ...) */
	set_led_type_idx(num_leds, MONO_LED, BLINK_GPIO, 0, 0);
	map_patterns_to_led(num_leds, 1, PATTERN_2);

	/* Step 4: Start the led notification */
	start_led_notification();
}

static void start_led_pattern_3() {
	core_err ret;
	int num_leds = 1;

	/* Creating and starting an LED notification consists of 4 steps. These steps must be followed
	 * in sequential manner, else behaviour of the library is undefied.
	 *
	 * Note: If you have multiple LEDs, then please follow Step 2 & 3, in the order as shown below.
	 *
	 * */

	/* Step 1: init_led_notification(num_led, period); */
	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_led_notification failed!", __func__);
		return;
	}

	/* Step 2: set_led_type_idx(led no., led_type, m_r_idx, g_idx, b_idx) */
	/* Step 3: map_patterns_to_led(led no., num_patterns, pattern1, pattern2, ...) */
	set_led_type_idx(num_leds, MONO_LED, BLINK_GPIO, 0, 0);
	map_patterns_to_led(num_leds, 3, PATTERN_1, PATTERN_2, PATTERN_3);

	/* Step 4: Start the led notification */
	start_led_notification();
}

/*******************************************************************************************************************/

void single_led_init() {

	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);

	/* Initialize the basic LEDs activity. (LED pin details are in thing/thing.h) */
	core_err ret = init_leds();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initilize the leds");
		return;
	}

}

void single_led_loop() {
	static uint8_t count = 1;
	switch (count) {
	case 1: {
		CORE_LOGI(TAG, "Executing LED pattern-1");
		start_led_pattern_1();
		count++;
		break;
	}
	case 2: {
		CORE_LOGI(TAG, "Executing LED pattern-2");
		start_led_pattern_2();
		count++;
		break;
	}
	case 3: {
		CORE_LOGI(TAG, "Executing LED pattern-3");
		start_led_pattern_3();
		count = 1;
		break;
	}
	default: {
		count = 1;
		break;
	}
	}

	TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
}
