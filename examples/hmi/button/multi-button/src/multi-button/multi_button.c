/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_button.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using multiple buttons
 *
 *
 *
 */

#include "multi_button.h"
#include "core_common.h"

#include "button_helper.h"
#include "gpio_helper_api.h"
#include "gpio_core.h"

#define BLINK_GPIO 		LED_4_GPIO

#define TAG "multi_button"

#define DELAY_TIME		DELAY_10_SEC

/********************************************** Module's Static Functions **********************************************************************/

void btn_interaction_handler(uint8_t io_pin, BTN_INTERACTION_TYPE intact_type) {
	if (io_pin == SWITCH_2_GPIO && intact_type == DOUBLE_PRESSED) {
		// set the GPIO pin value 1 for On
		CORE_LOGI(TAG, "%d Button pressed LED On", SWITCH_2_GPIO);
		set_gpio_value(BLINK_GPIO, HIGH);
	}
	else if (io_pin == SWITCH_3_GPIO && intact_type == CUSTOM_PRESSED) {
		// set the GPIO pin value 0 for Off
		CORE_LOGI(TAG, "%d Button pressed LED Off", SWITCH_3_GPIO);
		set_gpio_value(BLINK_GPIO, LOW);
	}
}

/*******************************************************************************************************************/

void multi_button_init() {

	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);

	core_err res = configure_gpio(BLINK_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if(res != CORE_OK){
		CORE_LOGE(TAG,"Failed to initialize BLINK_GPIO");
	}

	/*
	 * Initialize the basic Buttons activity. (button details are in thing/thing.h)
	 */
	core_err ret = init_double_press_btn_interface(SWITCH_2_GPIO,GPIO_IO_FLOATING,btn_interaction_handler);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize the SWITCH_2");
	}

	ret = init_custom_press_btn_interface(SWITCH_3_GPIO, GPIO_IO_FLOATING,DELAY_1_SEC,
			btn_interaction_handler);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize the SWITCH_3");
	}

}

void multi_button_loop() {
	CORE_LOGD(TAG, "*** Start of %s ***\r\n", __func__);

	/*
	 * In this example the Led will glow when the CUSTOM_BTN_1 button pressed
	 * And again it will off when CUSTOM_BTN_2 button pressed.
	 */

	// write down your code...
	CORE_LOGD(TAG, "*** End of %s ***\r\n", __func__);

	TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);

}
