/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   button_interaction.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using Button
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for button_interaction module
 *
 */
void button_interaction_init();

/**
 * @brief	Execution function for button_interaction module
 *
 */
void button_interaction_loop();

