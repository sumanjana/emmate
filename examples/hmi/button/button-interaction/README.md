# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/esp32_button_interaction.png" width="500">

[//]: ![ESP32-DevKit-C](fritzing/esp32_button_interaction.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `hmi/button-helper` module APIs to initialize a Button and execute a LED on/off operation using that button.

#### Example specific configurations
This example is configured ..

<img src="res/button-example-config.png" width="500">