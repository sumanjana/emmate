/**
 * This is an example c file of clcd display string  module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "clcd_display_scroll_string.h"

#define TAG	"clcd_display_scroll_string"

//Write your own string here
char *company_name = "Iquester Solution LLP";
char *platform_name = "EmMate Framework";
char *hello_string = "HELLO";
char *thanks_string = " Happy Coding & Thank You";

void clcd_display_init() {

	//Initialize clcd with max row and max column here we using 4x20 lcd display
	core_err ret = init_clcd(MAX_ROW, MAX_COL);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_clcd_lcd failed!", __func__);
		return;
	}
}

void clcd_display_string() {

	/*set string for displaying 1st row
	 *set string to  character,start row, end row, start column, end column,
	 *scroll option, scroll frequency(in ms), blink frequency(in ms),overwrite status
	 */
	core_err res = set_string_to_clcd(company_name, 0, 0, 0, 19, SCROLL_LEFT_TO_RIGHT, 250, 0,
			NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*set string for displaying 2nd row
	 * set string to  character,start row(1), end row(1), start column(0), end column(19),
	 * scroll option(right to left),scroll frequency(500 ms)
	 * rest are same of the above
	 */
	res = set_string_to_clcd(platform_name, 1, 1, 0, 19, SCROLL_RIGHT_TO_LEFT, 500, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*set string for displaying 3rd row
	 * set string to  character,start row(2), end row(2), start column(0), end column(19)
	 * scroll option(left to right),scroll frequency(1000 ms)
	 * rest are same of the above
	 */
	res = set_string_to_clcd(hello_string, 2, 2, 0, 19, SCROLL_LEFT_TO_RIGHT, 1000, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*set string for displaying 3rd row
	* set string to  character,start row(3), end row(3), start column(0), end column(19)
	* scroll option(right to left),scroll frequency(100 ms)
	* rest are same of the above
    */
	res = set_string_to_clcd(thanks_string, 3, 3, 0, 19, SCROLL_RIGHT_TO_LEFT, 100, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}
}

