/**
 * This is an example c file of clcd display string  module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "clcd_display_string.h"

#define TAG	"clcd_display_string"

//Write your own string here
char *str = "EmMate Framework";

void clcd_display_init() {

	//Initialize clcd with max row and max column here we using 16x2 lcd display
	core_err ret = init_clcd(2, 16);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_clcd_lcd failed!", __func__);
		return;
	}
}

void clcd_display_string() {

	/*set string to  character,start row, end row, start column, end column,
	 scroll option, scroll frequency(in ms), blink frequency(in ms),overwrite status*/

	core_err res = set_string_to_clcd(str, 0, 0, 0, 15, SCROLL_OFF, 0, 0,
			NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}
}

