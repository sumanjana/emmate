/**
 * This is an example c file of clcd display string  module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "clcd_display_multi_string.h"

#define TAG	"clcd_display_multiline_string"

//Write your own string here
char *company_name = "Iquester SolutionLLP";
char *platform_name = "EmMate Framework";
char *hello_string = "HELLO";
char *thanks_string = "Thanking You";

void clcd_display_init() {

	//Initialize clcd with max row and max column here we using 4x20 lcd display
	core_err ret = init_clcd(MAX_ROW, MAX_COL);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_clcd_lcd failed!", __func__);
		return;
	}
}

void clcd_display_string() {

	/*set string for displaying 1st row
	 *set string to  character,start row, end row, start column, end column,
	 *scroll option, scroll frequency(in ms), blink frequency(in ms),overwrite status
	 */

	core_err res = set_string_to_clcd(company_name, 0, 0, 0, 19, SCROLL_OFF, 0, 0,
			NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*set string for displaying 2nd row
	 * set string to  character,start row(1), end row(1), start column(2), end column(19)
	 * rest are same of the above
	 */
	res = set_string_to_clcd(platform_name, 1, 1, 2, 19, SCROLL_OFF, 0, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*set string for displaying 3rd row
	 * set string to  character,start row(2), end row(2), start column(7), end column(19)
	 * rest are same of the above
	 */
	res = set_string_to_clcd(hello_string, 2, 2, 7, 19, SCROLL_OFF, 0, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*set string for displaying 3rd row
	* set string to  character,start row(3), end row(3), start column(4), end column(19)
	* rest are same of the above
    */
	res = set_string_to_clcd(thanks_string, 3, 3, 4, 19, SCROLL_OFF, 0, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}
}

