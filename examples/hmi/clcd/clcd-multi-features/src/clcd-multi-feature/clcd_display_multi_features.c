/**
 * This is an example c file of clcd display string  module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "clcd_display_multi_features.h"

#define TAG	"clcd_display_multiline_string"

//Write your own string here
char *company_name = "Iquester SolutionLLP";
char *platform_name = "EmMate Framework";
char *hello_string = "HELLO";
char *thanks_string = "Thanking You";
char *welcome_string = "WELCOME";

void clcd_display_init() {

	//Initialize clcd with max column and max row here we using 20x4 lcd display
	core_err ret = init_clcd(MAX_ROW, MAX_COL);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_clcd_lcd failed!", __func__);
		return;
	}
}

void clcd_display_string_loop() {

	/*
	 * set string for displaying 1st row
	 * string
	 * start row, end row, start column, end column,
	 * scroll option, scroll frequency(in ms), blink frequency(in ms),
	 * overwrite status
	 *
	 */
	core_err res = set_string_to_clcd(company_name, 0, 0, 0, 19, SCROLL_OFF, 0, 0,
			OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);

	/*
	 * set string for displaying 1nd row
	 * string
	 * start row(0), end row(0), start column(2), end column(19)
	 * scroll off & scroll frequency(0ms)
	 * blink frequency(0ms)
	 * rest are same of the above
	 */
	res = set_string_to_clcd(platform_name, 0, 0, 2, 19, SCROLL_OFF, 0, 0, OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}
}
void clcd_display_string() {
	/*
	 * set string for displaying 3rd row
	 * string
	 * start row(1), end row(2), start column(7), end column(12)
	 * scroll option(left to right), scroll frequency(100 ms)
	 * rest are same of the above
	 */
	core_err res = set_string_to_clcd(hello_string, 1, 1, 7, 12, SCROLL_LEFT_TO_RIGHT, 100, 0, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*
	 * set string for displaying 3rd row
	 * string
	 * start row(2), end row(2), start column(7), end column(14)
	 * scroll option(right to left), scroll frequency(250 ms)
	 * blink frequency (250 ms)
	 * rest are same of the above
	 */
	res = set_string_to_clcd(welcome_string, 2, 2, 7, 14, SCROLL_RIGHT_TO_LEFT, 250, 250, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	/*
	 * set string for displaying 3rd row
	 * string
	 * start row(3), end row(3), start column(4), end column(19)
	 * blink frequency (500 ms)
	 * rest are same of the above
	 */
	res = set_string_to_clcd(thanks_string, 3, 3, 4, 19, SCROLL_OFF, 0, 500, NOT_OVERWRITE);
	if (res == CONFLICT_STRING) {
		CORE_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}
}

