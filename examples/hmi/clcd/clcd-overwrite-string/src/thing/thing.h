/*
 * This file will only contain comments for creating a custom thing.h
 */

#ifndef THING_H_
#define THING_H_

#include "som.h"
#include "core_config.h"

#define YOUR_THING_NAME	"CLCD Overwrite String"

#define LEDS_NUMBER		3  /*!< Number of leds used */

/*
 * The following LEDs are used by the EmMate Framework for showing notifications during startup.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing these SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_HMI_LED_MONO_RED		SOM_PIN_97   /*!< Used by the EmMate Framework as RED LED */
#define SYSTEM_HMI_LED_GREEN		SOM_PIN_79   /*!< Used by the EmMate Framework as GREEN LED */
#define SYSTEM_HMI_LED_BLUE			SOM_PIN_85   /*!< Used by the EmMate Framework as BLUE LED */

/**/
#define LEDS_ACTIVE_STATE 	0
/**/
#define LEDS_LIST { SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE }

#define BUTTONS_NUMBER	1

/*
 * The following BUTTON is used by the EmMate Framework as a Factory Reset Button for deleting all saved configurations.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing the SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_RESET_BUTTON		SOM_PIN_101

/**/
#define BUTTONS_ACTIVE_STATE	1
/**/
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }


/******************************************* CLCD GPIO Selection **********************************************/
/*
 * @Note:
 * If using CLCD-Lib, the following defines must be made else compilation error will occur
 */

// If CLCD-lib Enable for 4bit operation mode then Data pin D7-D4 use to send data to CLCD
#define LCD_D4		SOM_PIN_79
#define LCD_D5		SOM_PIN_85
#define LCD_D6		SOM_PIN_196
#define LCD_D7		SOM_PIN_27

/*
 * If CLCD 4bit Mode enable then mention below D3-D0 data pin will be deactivate
 * "CONFIG_CLCD_4BIT_MODE_ENABLE" is application config macro, which define in src/core_config.h
 */
#if !CONFIG_CLCD_4BIT_MODE_ENABLE
/*
 * If CLCD-lib Enable for 8bit operation mode then Data pin D7-D0 use to send data to CLCD.
 * So, all pin are activated
 */
#define LCD_D0		SOM_PIN_190
#define LCD_D1		SOM_PIN_192
#define LCD_D2		SOM_PIN_38
#define LCD_D3		SOM_PIN_49

#endif

// CLCD common control pins for CLCD 8bit & 4bit operation
#define LCD_RS		SOM_PIN_4
#define LCD_RW		SOM_PIN_47
#define LCD_EN		SOM_PIN_51

// Busy pin
#define LCD_BUSY		LCD_D7

#endif /* THING_H_ */
