#include "json_creation_and_post.h"

#define TAG	"core_app_main"

void core_app_main(void * param) {
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "==================================================================");

	json_creation_and_post_init();

	while (1) {
		while (get_network_conn_status() != NETCONNSTAT_CONNECTED) {
			TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
		}
		json_creation_and_post_loop();

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
