/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   json_creation_and_post.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The JSON create and  HTTP Post methods.
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "core_error.h"
#include "core_logger.h"
#include "conn.h"
#include "threading.h"
#include "thing.h"

#define EXAMPLE_URL					"http://httpbin.org/post"
#define EXAMPLE_PORT				8080
#define EXAMPLE_ROOTCA				NULL
#define EXAMPLE_HOST				"httpbin.org"
#define EXAMPLE_HTTP_USER_AGENT		"emmate/1.0"
#define EXAMPLE_CONTENT_TYPE		"application/json"
#define EXAMPLE_POST_DATA			NULL
#define EXAMPLE_RESPONSE_LEN		1024

/**
 * @brief Execution function for http_post module
 *
 */
void json_creation_and_post_loop() ;

/**
 * @brief Init function for http_post module
 *
 */
void json_creation_and_post_init();

