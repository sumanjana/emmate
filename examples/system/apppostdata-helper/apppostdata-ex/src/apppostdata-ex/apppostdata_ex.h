/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef APPPOSTDATA_EX_H_
#define APPPOSTDATA_EX_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

typedef struct{
	char msg[30];
	uint32_t post_data_count;
}APPLICATION_PUBLISH_DATA;

/**
 *
 * */
void apppostdata_ex_init();

/**
 * */
void apppostdata_ex_loop();

#endif	/* APPPOSTDATA_EX_H_ */
