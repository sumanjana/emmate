#include "time_diff.h"

#define TAG	"core_app_main"

void core_app_main(void * param) {
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "==================================================================");

	// initialize My module
	time_diff_init();

	while (1) {
		// Execute My module
		time_diff_loop();
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
