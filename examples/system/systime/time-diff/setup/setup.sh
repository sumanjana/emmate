#!/bin/bash

#setting current directory
export CURR_DIR=`pwd`

#importing Configuration File
export CONFIG_PATH=$CURR_DIR/setup.conf

source $CONFIG_PATH
export $(grep --regexp ^[A-Z] $CONFIG_PATH | cut -d= -f1)

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`

echo .....................................................................
echo Setting up your project
echo .....................................................................
echo Core distribution Location : $RELEASE_PKG_DIR
echo .....................................................................

if [ -z "$RELEASE_PKG_DIR" ]
then
	echo "Environment variable 'EMMATE_RELEASE_PATH' is not set"
	echo "You need to update your 'EMMATE_RELEASE_PATH' environment variable in the ~/.profile file."
	exit 1
fi

cd ..
export PROJ_DIR=`pwd`

echo Project Location : $PROJ_DIR
echo .....................................................................

# creating required directories
mkdir -p $PROJ_DIR/platform/core
############## HAVE TO GET ESP FROM SOMEWHERE ##############
mkdir -p $PROJ_DIR/platform/esp


#copying a temporary sample src
cd $RELEASE_PKG_DIR/setup/files

# Always copy the build folder from RELEASE_PKG_DIR/setup/files
cp -v -u -r ./build $PROJ_DIR

# Copy the src folder from RELEASE_PKG_DIR/setup/files
# Only if src is NOT PRESENT in PROJ_DIR
if [ ! -d "$PROJ_DIR/src" ]
then
	echo "Setting up new project. So copying the src to $PROJ_DIR"
	mkdir -p $PROJ_DIR/src
	cp -v -u -r ./src $PROJ_DIR
else
	# Check if the src has the version dir and conf file (required for building the examples)
	# Copy the version dir and conf file if not present
	if [ ! -d "$PROJ_DIR/src/version" ]
	then
		echo "Copying the version conf file to $PROJ_DIR/src/version"
		mkdir -p $PROJ_DIR/src/version
		cp -v -u -r ./src/version/app_version.conf $PROJ_DIR/src/version
	fi
	echo "Source directory already present. Not touching the project's source files"
fi


#copying the core src
cd $RELEASE_PKG_DIR/src

cp -v -u -r . $PROJ_DIR/platform/core


#copying the platform src
cd $RELEASE_PKG_DIR/platform

cp -v -u -r . $PROJ_DIR/platform/esp

