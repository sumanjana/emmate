/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   system_time.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * System Time
 *
 *
 *
 */
#ifndef SYSTEM_TIME_H_
#define SYSTEM_TIME_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
#include "systime.h"

#define CURRENT_TIME_MILLIS		1560259909111	// UTC Time/Date : Tue Jun 11 2019 13:31:49

/**
 * @brief set the system time and time zone using core_settimeofday()
 *  Create a structure of time value and set it.
 *  create a structure of time zone and set it.
 *
 */
void system_time_init();
/**
 * @brief Read time using get_systime()
 *     print date and time
 *
 */
void system_time_loop();

#endif	/* SYSTEM_TIME_H_ */

