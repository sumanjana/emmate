/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef APPCONFIG_EX_H_
#define APPCONFIG_EX_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

typedef struct{
	uint32_t app_config_value1;
	double app_config_value2;
}APPLICATION_CONFIGURATION;

/**
 *
 * */
void appconfig_ex_init();

/**
 * */
void appconfig_ex_loop();

#endif	/* APPCONFIG_EX_H_ */
