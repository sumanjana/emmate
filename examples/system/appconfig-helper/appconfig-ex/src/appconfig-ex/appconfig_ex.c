/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "appconfig_ex.h"
#include "appconfig_helper.h"
#include "app_json_utils.h"

#define TAG	"appconfig_ex"

APPLICATION_CONFIGURATION app_conf;

/**
 *	decleared receive app-configuration call-back function
 */
static core_err app_configuration_process(char * app_config, size_t app_config_len) {
	CORE_LOGD(TAG, "%s entry", __func__);
	core_err res = CORE_FAIL;
	CORE_LOGI(TAG, "Application Configuration %s\n", app_config);
	if (app_config != NULL) {
		res = parse_config_json(app_config, &app_conf);
		if (res != CORE_OK) {
			CORE_LOGE(TAG, "Failed to parse App Configuration");
		}else{
			CORE_LOGI(TAG,"###################### AppConfig Data");
			CORE_LOGI(TAG,"app_config_value1 = %d", app_conf.app_config_value1);
			CORE_LOGI(TAG,"app_config_value2 = %f", app_conf.app_config_value2);
			CORE_LOGI(TAG,"###################### AppConfig Data\n");
		}
	}
	CORE_LOGD(TAG, "%s exit", __func__);
	return res;
}

void appconfig_ex_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */

	// register application configuration function
	register_app_configuration_function(app_configuration_process);

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void appconfig_ex_loop() {
	CORE_LOGI(TAG, "In %s", __func__);
}
