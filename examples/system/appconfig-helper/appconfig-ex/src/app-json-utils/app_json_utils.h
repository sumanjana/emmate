/*
 * File Name: app_json_utils.h
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#ifndef APP_JSON_UTILS_H_
#define APP_JSON_UTILS_H_

#include "appconfig_ex.h"

/*
 * Parse Application Config's response Json String:
 * {
 * 	"app_config_string"	:	"some_text"
 * 	"app_config_value"	:	49.9,
 * }
 *
 */

core_err parse_config_json(char *string, APPLICATION_CONFIGURATION *resp);

#endif /* APP_JSON_UTILS_H_ */
