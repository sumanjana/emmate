/*
 * File Name: app_json_utils.c
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#include "app_json_utils.h"

#include "parson.h"
#include "core_utils.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <math.h>

#define TAG "app_json_utils"

/****************************************************************************************************************************/


/*
 * Parse Application Config's response Json String:
 * {
 * 	"app_config_value1"	:	100000
 * 	"app_config_value2"	:	49.9,
 * }
 *
 */
core_err parse_config_json(char *string, APPLICATION_CONFIGURATION *resp) {
	core_err ret = CORE_FAIL;

	if (NULL != string) {
		JSON_Value *root_value;
		JSON_Object *root_object;
		root_value = json_parse_string(string);

		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "Config Respons JSON Value type not matched\r\n");
			json_value_free(root_value);
			return ret;
		}
		root_object = json_value_get_object(root_value);

		// get app_config_value1
		uint32_t app_config_value1 = json_object_get_number(root_object, GET_VAR_NAME(resp->app_config_value1, "->"));


		// get app_config_value2
		double app_config_value2 = json_object_get_number(root_object, GET_VAR_NAME(resp->app_config_value2, "->"));


		resp->app_config_value1 = app_config_value1;
		resp->app_config_value2 = app_config_value2;

		ret = CORE_OK;
		json_value_free(root_value);

	} else {
		ret = CORE_ERR_INVALID_RESPONSE;
	}
	return ret;

}
