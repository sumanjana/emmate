/*
 * This file will only contain comments for creating a custom thing.h
 */

#ifndef THING_H_
#define THING_H_

#include "som.h"

#define YOUR_THING_NAME	"Persistent Read/Write Value"

#define LEDS_NUMBER		3  /*!< Number of leds used */

/*
 * The following LEDs are used by the EmMate Framework for showing notifications during startup.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing these SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_HMI_LED_MONO_RED		SOM_PIN_97   /*!< Used by the EmMate Framework as RED LED */
#define SYSTEM_HMI_LED_GREEN		SOM_PIN_79   /*!< Used by the EmMate Framework as GREEN LED */
#define SYSTEM_HMI_LED_BLUE			SOM_PIN_85   /*!< Used by the EmMate Framework as BLUE LED */

/**/
#define LEDS_ACTIVE_STATE 	0
/**/
#define LEDS_LIST { SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE }

#define BUTTONS_NUMBER	1

/*
 * The following BUTTON is used by the EmMate Framework as a Factory Reset Button for deleting all saved configurations.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing the SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_RESET_BUTTON		SOM_PIN_101

/**/
#define BUTTONS_ACTIVE_STATE	1
/**/
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }

/********************************************** I2C GPIO Selection *****************************************************/
/**
 * The EmMate Framework has integrated external EEPROM with Unique 32-bit Serial Number IC 24AA02UIDICs support Library
 * EmMate Framework & Application developer can use this Library directly.
 */

/**
 * The integrated external ICs support Library eeprom_24AA02UID use two GPIO for complete the eeprom_24AA02UID process
 * For eeprom_24AA02UID library, I2C control external EEPROM ICs
 * SDA & SCL GPIO need to define into Application's thing.h as:
 * #define THING_EEPROM_24AA02UID_SDA_GPIO 	SOM_PIN_x
 * #define THING_EEPROM_24AA02UID_SCL_GPIO 	SOM_PIN_x
 */
 #define THING_EEPROM_24AA02UID_SDA_GPIO 	SOM_PIN_194
 #define THING_EEPROM_24AA02UID_SCL_GPIO 	SOM_PIN_196

/******************************************* SDMMC GPIO Selection **************************************************/
/* If using SDMMC module with SPI, the following defines must be made else compilation error will occur */
#define SDMMC_CLK 	SOM_PIN_88
#define SDMMC_MISO 	SOM_PIN_90
#define SDMMC_MOSI 	SOM_PIN_92
#define SDMMC_CS 	SOM_PIN_86

#endif /* THING_H_ */
