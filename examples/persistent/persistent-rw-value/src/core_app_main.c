
#include "persistent_rw_value.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling persistent_rw_value_init() in your_module.c in your-module directory ...");
	persistent_rw_value_init();
	CORE_LOGI(TAG, "Returned from persistent_rw_value_init()");

	while(1){
		persistent_rw_value_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
