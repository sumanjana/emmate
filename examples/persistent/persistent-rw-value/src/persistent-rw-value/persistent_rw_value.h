/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef YOUR_MODULE_H_
#define YOUR_MODULE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 *
 * */
void persistent_rw_value_init();

/**
 * */
void persistent_rw_value_loop();

#endif	/* YOUR_MODULE_H_ */
