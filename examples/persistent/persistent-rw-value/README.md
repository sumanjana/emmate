# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

Here we are using the ESP32's internal storage (NVS) as the data memory, so no external hardware is necessary. See the configurations below to enable the internal NVS.

<img src="fritzing/persistent_rw_value.png" width="700">

[//]: ![image](fritzing/persistent_rw_value.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to Read/Write data from persistent memory with the help of the Persistent Module of the EmMate Framework.

#### Example specific configurations
This example is configured to use the ESP32's internal storage (NVS) as the data memory. The configurations needed to enable the NVS mempory as data storage is shown below:

![](fritzing/persistent-example-config.png)

#### Example's Information
This example shows how to read & write data from/to the persistent memory by doing the following steps:

- First, the application tries to read and print the data **"Hello! User"** from memory.
- Then the system is rebooted from the application by calling EmMate's System API
- If reading from memory was failed then the application writes the data **"Hello! User"** into the memory and again reboots the system
- The above steps are done again
