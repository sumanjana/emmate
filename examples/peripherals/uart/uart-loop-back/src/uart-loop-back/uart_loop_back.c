/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "uart_loop_back.h"
#include "uart_core.h"
#include <string.h>
#include "core_utils.h"

#define TAG	"uart_loop_back"

// Use UART Port 1
#define UART_PORT_NUM	UART_CORE_NUM_1

// Define UART's Tx, Rx (We use UART 1 without Hardware flowcontrol so CTS, RTS leave it blank)
#define UART_TX_PIN		UART_TX_GPIO
#define UART_RX_PIN		UART_RX_GPIO
#define UART_CTS_PIN
#define UART_RTS_PIN

// Define UART's Communication BaudRate
#define UART_BAUD_RATE	115200

char uart_recv_data[1024] = { 0 };
size_t uart_recv_data_len = 0;

// Define Uart Data Receive CallBack
void uart_transactor_cb(uart_core_port_t uart_port, void* data, size_t size) {
	if (UART_PORT_NUM == uart_port) {
		// Loop Back Data to terminal
		uart_core_driver_write_bytes(uart_port, data, size);
	}
}

void uart_loop_back_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	/*
	 * Populate UART configuration:
	 * baud rate
	 * Uart Data bits
	 * Uart HW Flow-control
	 * Uart Parity bits
	 * Uart Stop bits
	 *
	 */
	UART_CONFIGURATION uart_config;
	uart_config.baud_rate = UART_BAUD_RATE;
	uart_config.data_bits = UART_CORE_DATA_8_BITS;
	uart_config.flow_ctrl = UART_CORE_HW_FLOWCTRL_DISABLE;
	uart_config.parity = UART_CORE_PARITY_DISABLE;
	uart_config.stop_bits = UART_CORE_STOP_BITS_1;

	/*
	 * Populate UART Pins:
	 * Tx & Rx GPIO
	 * RTS & CTS GPIO
	 *
	 */
	UART_CORE_PORT_PIN uart_pin;
	uart_pin.cts_io_num = UART_CORE_PIN_NO_CHANGE;
	uart_pin.rts_io_num = UART_CORE_PIN_NO_CHANGE;
	uart_pin.rx_io_num = UART_RX_PIN;
	uart_pin.tx_io_num = UART_TX_PIN;

	/*
	 * Populate UART transaction's data :
	 * receive data length(in bytes)
	 *
	 */
	UART_CORE_TRANSACTION_END_WITH_DATA uart_transaction_end_with_data;
	uart_transaction_end_with_data.num_bytes = 50;

	/*
	 * initialize UART with
	 * specific Uart PORT
	 * uart config structure
	 * Uart Pin structure
	 * Uart Transaction type
	 * uart Transaction data structure
	 * Uart data receive callback
	 *
	 */
	core_err res = init_uart_core_driver(UART_PORT_NUM, &uart_config, &uart_pin, UART_READ_TRANSACT_NUM_BYTES,
			&uart_transaction_end_with_data, uart_transactor_cb);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize Uart");
	} else {

	}

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void uart_loop_back_loop() {
	CORE_LOGD(TAG, "In %s", __func__);

}
