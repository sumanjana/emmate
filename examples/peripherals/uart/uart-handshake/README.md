# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/uart_handshake.png" width="800">

[//]: ![image](fritzing/uart_handshake.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates the Handshake **UART** communication between two devices with the help of the UART peripheral of the EmMate Framework.

#### Example specific configurations
This example is configured ..

<img src="res/uart-example-config.png" width="500">

```
Note: Make sure you configure 'MASTER_BOARD' & 'SLAVE_BOARD' macros in src/uart-handshake/uart_handshake.c
	
	To Build the example for 

	<- Master -> Set 1 to the 'MASTER_BOARD' & 0 to the 'SLAVE_BOARD'
	 
	<- Slave ->  Set 0 to the 'MASTER_BOARD' & 1 to the 'SLAVE_BOARD'

```

This example does the following:

- Shows handshake comunication using the UART peripheral
- The UART's Tx & Rx pin must be configured properly in `src/thing/thing.h`
- In Master mode: 
    - Initialize the UART with the configuration
    - The Master starts the communication with **"Hello SLAVE_BOARD"** message.
    - The Master sends this message for 20 times.
    - Then it sends **"Good Bye SLAVE_BOARD"** message and waits for **"It’s been a pleasure working with you. MASTER_BOARD"** message.
    - After receiving **"It’s been a pleasure working with you. MASTER_BOARD"** message, it de-initializes the UART and remains idle.
    
- In Slave mode: 
    - Initialize the UART with the configuration
    - The Slave waits for the **"Hello SLAVE_BOARD"** message from the master.
    - After receiving **"Hello SLAVE_BOARD"** message, the Slave sends back the message **"Hi! MASTER_BOARD"** to the Master.
    - At the end when the Slave gets **"Good Bye SLAVE_BOARD"** message from the Master, then the Slave send **"It’s been a pleasure working with you. MASTER_BOARD"** message and de-initializes the UART and remains idle.
