/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "vibgyor_rainbow_colour.h"
#include "core_utils.h"
#include "led_pwm_driver.h"

#define TAG	"vibgyor_rainbow_colour"

/*
 * There are two kinds of RGB LEDs:
 * Common anode(+) LED and Common cathode(-) LED.
 * In this Example We used common cathod RGB Led. So, "COMMON_ANODE_RGB_LED" set as 0
 *
 * @note:
 * 	If in case you using a Common anode RGB Led then, set the "#define COMMON_ANODE_RGB_LED" to 1 to get the output.
 */
#define COMMON_ANODE_RGB_LED	0

#define RGB_LED_FADE_DUTAION	DELAY_2_SEC

static VIBGYOR_RAINBOW_COLOR rainbow_color = VIOLET;

core_err show_vibgyor_rainbow_color(uint8_t red_decimal_code, uint8_t green_decimal_code, uint8_t blue_decimal_code) {

	/*
	 * Set & Start LED PWM with 8bit Decimal Colour Code for RGB Led Red GPIO
	 */
	core_err res = led_pwm_driver_set_fade_time_and_start_with_decimal_code(LED_PWM_CHANNEL_0, red_decimal_code,
	RGB_LED_FADE_DUTAION);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set duty cycle to configured LED PWM Channel %d", LED_PWM_CHANNEL_0);
		return res;
	}

	/*
	 * Set & Start LED PWM with 8bit Decimal Colour Code for RGB Led Green GPIO
	 */
	res = led_pwm_driver_set_fade_time_and_start_with_decimal_code(LED_PWM_CHANNEL_1, green_decimal_code,
	RGB_LED_FADE_DUTAION);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set duty cycle to configured LED PWM Channel %d", LED_PWM_CHANNEL_1);
		return res;
	}

	/*
	 * Set & Start LED PWM with 8bit Decimal Colour Code for RGB Led Blue GPIO
	 */
	res = led_pwm_driver_set_fade_time_and_start_with_decimal_code(LED_PWM_CHANNEL_2, blue_decimal_code,
	RGB_LED_FADE_DUTAION);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set duty cycle to configured LED PWM Channel %d", LED_PWM_CHANNEL_2);
		return res;
	}
	return res;
}

void vibgyor_rainbow_colour_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	/* initializations LED PWM*/
	core_err res = led_pwm_driver_configure_timer(LED_PWM_TIMER_13_BIT, 5000, LED_PWM_TIMER_0);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Timer driver");
	}

	/*
	 * Install & activate LED PWM functionality
	 */
	res = led_pwm_driver_fade_func_install();
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM fade functionality");
	}

	/*
	 * Configure LED PWM Channels for RGB Led's Red, Green, Blue GPIOs
	 * @note:
	 * 	Set every Leds in off state.
	 * 	for Common Anode, Set 255 so LED GPIO will always on but physical LED work as off condition
	 * 	for Common Cathod, Set 0 so LED GPIO will always in off condition
	 */

	/*
	 * Configure LED PWM Channels for RGB Led Red GPIO
	 */
#if COMMON_ANODE_RGB_LED
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_0, 255, RGB_LED_RED_GPIO, LED_PWM_TIMER_0);
#else
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_0, 0, RGB_LED_RED_GPIO, LED_PWM_TIMER_0);
#endif
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel 0");
	}

	/*
	 * Configure LED PWM Channels for RGB Led Green GPIO
	 */
#if COMMON_ANODE_RGB_LED
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_1, 255, RGB_LED_GREEN_GPIO,
			LED_PWM_TIMER_0);
#else
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_1, 0, RGB_LED_GREEN_GPIO, LED_PWM_TIMER_0);
#endif
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel 1");
	}

	/*
	 * Configure LED PWM Channels for RGB Led Blue GPIO
	 */
#if COMMON_ANODE_RGB_LED
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_2, 255, RGB_LED_BLUE_GPIO,
			LED_PWM_TIMER_0);
#else
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_2, 0, RGB_LED_BLUE_GPIO, LED_PWM_TIMER_0);
#endif
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel 2");
	}

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void vibgyor_rainbow_colour_loop() {
	CORE_LOGD(TAG, "In %s", __func__);

	switch (rainbow_color) {
	case VIOLET: {
		CORE_LOGI(TAG, "Vibgyor Rainbow colour = %s", GET_VAR_NAME(VIOLET, NULL));
		/**
		 * Check this link for mode information about VIBGYOR RAINBOW colour
		 * https://www.webnots.com/vibgyor-rainbow-color-codes/
		 */
#if COMMON_ANODE_RGB_LED
		show_vibgyor_rainbow_color(107, 255, 44);
#else
		show_vibgyor_rainbow_color(148, 0, 211);
#endif
		rainbow_color++;
		break;
	}
	case INDIGO: {
		CORE_LOGI(TAG, "Vibgyor Rainbow colour = %s", GET_VAR_NAME(INDIGO, NULL));
#if COMMON_ANODE_RGB_LED
		show_vibgyor_rainbow_color(180, 255, 125);
#else
		show_vibgyor_rainbow_color(75, 0, 130);
#endif
		rainbow_color++;
		break;
	}
	case BLUE: {
		CORE_LOGI(TAG, "Vibgyor Rainbow colour = %s", GET_VAR_NAME(BLUE, NULL));
#if COMMON_ANODE_RGB_LED
		show_vibgyor_rainbow_color(255, 255, 0);
#else
		show_vibgyor_rainbow_color(0, 0, 255);
#endif
		rainbow_color++;
		break;
	}
	case GREEN: {
		CORE_LOGI(TAG, "Vibgyor Rainbow colour = %s", GET_VAR_NAME(GREEN, NULL));
#if COMMON_ANODE_RGB_LED
		show_vibgyor_rainbow_color(255, 0, 255);
#else
		show_vibgyor_rainbow_color(0, 255, 0);
#endif
		rainbow_color++;
		break;
	}
	case YELLOW: {
		CORE_LOGI(TAG, "Vibgyor Rainbow colour = %s", GET_VAR_NAME(YELLOW, NULL));
#if COMMON_ANODE_RGB_LED
		show_vibgyor_rainbow_color(0, 0, 255);
#else
		show_vibgyor_rainbow_color(255, 255, 0);
#endif
		rainbow_color++;
		break;
	}
	case ORANGE: {
		CORE_LOGI(TAG, "Vibgyor Rainbow colour = %s", GET_VAR_NAME(ORANGE, NULL));
#if COMMON_ANODE_RGB_LED
		show_vibgyor_rainbow_color(0, 128, 255);
#else
		show_vibgyor_rainbow_color(255, 127, 0);
#endif
		rainbow_color++;
		break;
	}
	case RED: {
		CORE_LOGI(TAG, "Vibgyor Rainbow colour = %s", GET_VAR_NAME(RED, NULL));
#if COMMON_ANODE_RGB_LED
		show_vibgyor_rainbow_color(0, 255, 255);
#else
		show_vibgyor_rainbow_color(255, 0, 0);
#endif
		rainbow_color = VIOLET;
		break;
	}
	}
}
