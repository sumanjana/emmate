
#include "vibgyor_rainbow_colour.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling vibgyor_rainbow_colour_init() in your_module.c in your-module directory ...");
	vibgyor_rainbow_colour_init();
	CORE_LOGI(TAG, "Returned from vibgyor_rainbow_colour_init()");

	while(1){
		vibgyor_rainbow_colour_loop();
		TaskDelay(DELAY_3_SEC / TICK_RATE_TO_MS);
	}
}
