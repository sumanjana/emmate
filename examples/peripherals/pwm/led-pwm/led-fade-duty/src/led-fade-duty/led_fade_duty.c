/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "led_fade_duty.h"
#include "led_pwm_driver.h"

#define TAG	"led-fade-duty"

static int duty_percent = 0;
bool decrement_duty = false;

void led_pwm_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* initializations LED PWM*/
	core_err res = led_pwm_driver_configure_timer(LED_PWM_TIMER_13_BIT, 5000, LED_PWM_TIMER_0);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Timer driver");
	}

	/*
	 * Install & activate LED PWM functionality
	 */
	res = led_pwm_driver_fade_func_install();
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM fade functionality");
	}

	/*
	 * Configure LED PWM Channel 0 for SYSTEM_HMI_LED_MONO_RED
	 */
	res = led_pwm_driver_configure_channel(LED_PWM_CHANNEL_0, 0, SYSTEM_HMI_LED_MONO_RED, LED_PWM_TIMER_0);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel 0");
	}

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void led_pwm_loop() {
	CORE_LOGD(TAG, "In %s", __func__);

	/*
	 * Set & Start LED PWM with PWM Duty-cycle for LED_PWM_CHANNEL_0
	 */
	core_err res = led_pwm_driver_set_duty_cycle(LED_PWM_CHANNEL_0, (uint32_t) duty_percent);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set duty cycle to configured LED PWM Channel-0");
	}

	// increment and decremet PWM duty cycle.
	if (decrement_duty == false) {
		duty_percent += 2;
	} else {
		duty_percent -= 2;
	}

	// Logic for toggle the duty-cycle increment or decremet mode.
	if (duty_percent > 100) {
		decrement_duty = true;
		duty_percent -= 2;
	} else if (duty_percent < 0) {
		decrement_duty = false;
		duty_percent += 2;
	}
}
