# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/led_fade_pwm.png" width="500">

[//]: ![image](fritzing/led_fade_pwm.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates the Led Fade In/Out with the help of the LED_PWM peripheral of the EmMate Framework.

#### Example specific configurations
This example is configured ..

<img src="res/led-pwm-example-config.png" width="500">

This example does the following:

- Shows Led fade In/Out on a single LED. For this example `SYSTEM_HMI_LED_MONO_RED` is chosen. This must be configured properly in `src/thing/thing.h`

- Demonstrates the usage of `led_pwm_driver_set_fade_time_and_start()` API.

- Here the initial duty cycle is set to 0 by `led_pwm_driver_configure_channel()`, then the `led_pwm_driver_set_fade_time_and_start()` API is used to fade in and fade out the LED from 0 to 100%. The time interval for every fade in/out is 1 second. This process is done in the function `led_fade_pwm_loop()`


