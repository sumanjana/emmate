
#include "led-fade-api-ex.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling led_fade_pwm_init() in your_module.c in your-module directory ...");
	led_fade_pwm_init();
	CORE_LOGI(TAG, "Returned from led_fade_pwm_init()");

	while(1){
		led_fade_pwm_loop();
		TaskDelay(DELAY_10_MSEC/ TICK_RATE_TO_MS);
	}
}
