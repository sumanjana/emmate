/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "led-fade-api-ex.h"

#include "led_pwm_driver.h"

#define TAG	"led_pwm"

bool decrement_duty = false;

void led_fade_pwm_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* initializations LED PWM*/
	core_err res = led_pwm_driver_configure_timer(LED_PWM_TIMER_13_BIT, 5000, LED_PWM_TIMER_0);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Timer driver");
	}

	/*
	 * Install & activate LED PWM functionality
	 */
	res = led_pwm_driver_fade_func_install();
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM fade functionality");
	}

	/*
	 * Configure LED PWM Channel 0 for SYSTEM_HMI_LED_MONO_RED
	 */
	res = led_pwm_driver_configure_channel(LED_PWM_CHANNEL_0, 0, SYSTEM_HMI_LED_MONO_RED, LED_PWM_TIMER_0);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel");
	}

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void led_fade_pwm_loop() {
	CORE_LOGD(TAG, "In %s", __func__);

	/*
	 * Set & Start LED PWM fade Out with PWM Duty-cycle for LED_PWM_CHANNEL_0 for 1sec
	 */
	core_err res = led_pwm_driver_set_fade_time_and_start(LED_PWM_CHANNEL_0, 100, DELAY_1_SEC);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set fade_time to configured LED PWM Channel-0");
	}

	// wait for 1sec
	TaskDelay(DELAY_1_SEC/ TICK_RATE_TO_MS);

	/*
	 * Set & Start LED PWM fade In with PWM Duty-cycle for LED_PWM_CHANNEL_0 for 1sec
	 */
	res = led_pwm_driver_set_fade_time_and_start(LED_PWM_CHANNEL_0, 0, DELAY_1_SEC);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set fade_time to configured LED PWM Channel-0");
	}

	// wait for 1sec
	TaskDelay(DELAY_1_SEC/ TICK_RATE_TO_MS);

}
