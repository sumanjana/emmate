# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/esp32_adc_lm35.png" width="500">

[//]: ![image](fritzing/esp32_adc_lm35.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `peripherals/adc` module's API to read an analog temperature sensor, ie. LM35. To select a different the analog pin change the macro `ADC_LM35_DETECT_AN_GPIO` in `thing/thing.h`

#### Example specific configurations
This example is configured ..

<img src="res/adc-example-config.png" width="500">

The example does the following:

- Initializes an analog pin via `init_adc_peripheral()` function
- Reads the raw value and equivalent voltage by calling `get_adc_peripheral_data()`
- Converts the voltage into temperature value 
- Prints the temperature value whenever there is a change