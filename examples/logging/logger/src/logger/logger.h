/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   logger.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Logging options and Log levels
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "core_error.h"
#include "core_logger.h"
#include "threading.h"
#include "thing.h"

/**
 * @brief	Init function for logger module
 *
 */
void logger_init();

/**
 * @brief	Execution function for logger module
 *
 */
void logger_loop();


