#!/bin/bash

#setting current directory
export CURR_DIR=`pwd`

#importing Configuration File
export BUILD_CONFIG_PATH=$CURR_DIR/build.conf

source $BUILD_CONFIG_PATH
export $(grep --regexp ^[A-Z] $BUILD_CONFIG_PATH | cut -d= -f1)

#importing Build CMD Option Helper
export BUILD_OPTION_HELPER_PATH=$CURR_DIR/build-support/build-opt-helper.sh
source $BUILD_OPTION_HELPER_PATH

#importing MenuConfiguration File
export MENUCONFIG_EXECUTION_SCRIPT=$CURR_DIR/build-support/execute-menuconfig.sh
export MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH=$CURR_DIR/menuconfig-gen-execution-status
#source $MENUCONFIG_EXECUTION_SCRIPT

export BUILD_HELP_SCRIPT=$CURR_DIR/build-support/build-helps.sh

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`

#Collect Project Path & Project Name
#go to Project Directory
cd ..
export PROJECT_DIR=$PWD
export PROJECT_NAME=$(basename $PWD)
export APP_SRC_PATH=$PROJECT_DIR/src
export BIN_DIRECTORY=$PROJECT_DIR/bin


#return to Current Directory
cd $CURR_DIR


## Script Functions ###############################################################################
make_executable() {
	local script=$1
	if [ ! -x $script ]
	then
		echo "The script file is not executable...making the script executable"
		#make is executable
		chmod a+x $script
	fi
}

execute_build_helper_menu(){
	echo "Build Helps..."
	make_executable $BUILD_HELP_SCRIPT
	$BUILD_HELP_SCRIPT
	exit 0;	
}

#define SYNCHRONIZE_CMD_EXECUTIONS_OPT array
declare -a SYNCHRONIZE_CMD_EXECUTIONS_OPT
build_opt_contains() {
    local arg=$1
    local opt=0
    for opt in "${EMMATE_BUILD_CMD_OPT[@]}"
    do
    	if [[ $arg == $opt ]]
		then
			#echo $arg == $opt
#			if [[ $arg == "-help" ]]
#			then
#				SYNCHRONIZE_CMD_EXECUTIONS_OPT[0]=$arg
#				#return 100
#			el
			if [[ $arg == "-menuconfig" ]]
			then
				SYNCHRONIZE_CMD_EXECUTIONS_OPT[0]=$arg
				#return 100
			elif [[ $arg == "-clean" ]]
			then
				SYNCHRONIZE_CMD_EXECUTIONS_OPT[1]=$arg
			elif [[ $arg == "-make" ]]
			then
				SYNCHRONIZE_CMD_EXECUTIONS_OPT[2]=$arg
			elif [[ $arg == "-erase-flash" ]]
			then
				SYNCHRONIZE_CMD_EXECUTIONS_OPT[3]=$arg
			elif [[ $arg == "-flash" ]]
			then
				SYNCHRONIZE_CMD_EXECUTIONS_OPT[4]=$arg
			elif [[ $arg == "-log" ]]
			then
				SYNCHRONIZE_CMD_EXECUTIONS_OPT[5]=$arg
			fi
			
			return 0
		fi
    done
    return 1
}


build_initiating_logging_execution(){
	#initiating logging
	export LOG_DIR="$BIN_DIRECTORY/log"
	export LOG_FILE="$LOG_DIR/build.log"

	mkdir -p $LOG_DIR


	echo Setting up the build...
	SETUP_SCRIPT=./build-support/build_setup.sh
	make_executable $SETUP_SCRIPT
	$SETUP_SCRIPT > $LOG_FILE
	echo
	echo
}



## Script Logics ###############################################################################

echo .....................................................................
echo Initiating build for $PROJECT_NAME
echo .....................................................................
echo

echo Validating the configurations ...
VALIDATE_SCRIPT=./build-support/validate_config.sh
make_executable $VALIDATE_SCRIPT
$VALIDATE_SCRIPT

# get return value of last executed script
ret_stat=$?
if [ $ret_stat -eq 0 ]
then
# Validate script is successful
	:
else
	exit 1
fi

# if BUILD_MODEis not set then set it to gui-config
if [ -z "$BUILD_MODE" ]
then
	BUILD_MODE="gui-config"
fi

echo
echo .....................................................................
echo Build Mode             : $BUILD_MODE
echo Build Date             : $CURRENT_DATE
echo Build Time             : $CURRENT_TIME
echo Build By               : $USERNAME
echo .....................................................................
echo Project Location : $PROJECT_DIR
echo .....................................................................
echo Distribution Location : $BIN_DIRECTORY
echo .....................................................................
echo


# Getting platform info
export PLATFORM_CONFIG_PATH=$CURR_DIR/build-support/platform.conf
source $PLATFORM_CONFIG_PATH

# export 'PLATFORM_TARGET' Environment variable for Menuconfig 
export PLATFORM_TARGET=$PLATFORM

#Creating BIN_DIRECTORY
mkdir -v -p $BIN_DIRECTORY


#setting Build Mode
export PROJ_BUILD_MODE=$BUILD_MODE

#echo Total-options "$@"
#echo Total-options count "$#"

if [ "$#" -ge 1 ]
then
	
	# TODO: verify cmd options
	for arg in "$@"
	do
		#echo $arg
		build_opt_contains $arg
		stat=$?
		if [[ $stat -eq 100 ]]
		then
			break;
		elif [[ $stat -eq 1 ]]
		then
			execute_build_helper_menu
		fi
	done
	#echo "${SYNCHRONIZE_CMD_EXECUTIONS_OPT[@]}"
	

	if [[ ${SYNCHRONIZE_CMD_EXECUTIONS_OPT[0]} = "-help" ]]
	then
		execute_build_helper_menu
	fi
	
	if [[ ${SYNCHRONIZE_CMD_EXECUTIONS_OPT[0]} = "-menuconfig" ]]
	then
		echo 
		echo "## Execute EmMate Menuconfig ################################"
		#run menuconf_gen
		
		# setup build Logging Files and directory
		build_initiating_logging_execution
		
		echo "Execute MenuConfig..."
		make_executable $MENUCONFIG_EXECUTION_SCRIPT
		$MENUCONFIG_EXECUTION_SCRIPT "-menuconfig" >> $LOG_FILE
		RETURN=$?
		if [ $RETURN -eq 0 ]
		then
			:
		else
			echo
			echo "## Execute EmMate Menuconfig Failed ################################"
			exit $RETURN
		fi
		echo
		echo "## Execute EmMate Menuconfig Successful ################################"
		exit 0
	fi

	
	if [[ ${SYNCHRONIZE_CMD_EXECUTIONS_OPT[1]} = "-clean" ]]
	then
		echo
		echo "## Execute EmMate Clean ################################"
		
		# .??* delete all hidden files
		rm -rf $BIN_DIRECTORY/.??*	$BIN_DIRECTORY/*
		
		echo
		echo "## Execute EmMate Clean Successful ################################"
		if [ ${#SYNCHRONIZE_CMD_EXECUTIONS_OPT[@]} -gt 1 ]
		then
			echo ${#SYNCHRONIZE_CMD_EXECUTIONS_OPT[@]}
		else
			echo ${#SYNCHRONIZE_CMD_EXECUTIONS_OPT[@]}
			exit 0
		fi
		
	fi
	
	
else
	execute_build_helper_menu
fi


# setup build Logging Files and directory
build_initiating_logging_execution

#run menuconf_gen
make_executable $MENUCONFIG_EXECUTION_SCRIPT
$MENUCONFIG_EXECUTION_SCRIPT  >> $LOG_FILE
RETURN=$?
if [ $RETURN -eq 0 ]
then
	:
else
	exit $RETURN
fi


# read menuconfig execution status
if [ -f $MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH ]
then
	source $MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH
	if [[ $MENUCONFIG_EXECUTION_STATUS = 'y' ]]
	then
		#echo "If menuconfig execute then Deleting BIN_DIRECTORY/.PROJECT_NAME if present" >> $LOG_FILE
	# delete hidden .PROJECT_NAME/* files
		rm -rf -v $BIN_DIRECTORY/.??* >> $LOG_FILE
		rm -rf -v  $MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH >> $LOG_FILE
	fi
fi



echo


#Creating Application Version
echo Updating the version ...
APP_VERSION_SCRIPT=./build-support/version.sh
make_executable $APP_VERSION_SCRIPT
$APP_VERSION_SCRIPT >> $LOG_FILE

# Getting platform info
#export PLATFORM_CONFIG_PATH=$CURR_DIR/build-support/platform.conf
#source $PLATFORM_CONFIG_PATH

if [ $PLATFORM = "esp32" ] 
then
	
	echo
	echo "Execute ESP32 Platform Build Process"
	make_executable ./build-support/$PLATFORM/platform-process.sh
	./build-support/$PLATFORM/platform-process.sh	>> $LOG_FILE
	
	echo Start Application Build....
	
	#check all cmd line argument & work on basis of those argument
	for arg in "${SYNCHRONIZE_CMD_EXECUTIONS_OPT[@]}"
	do
		make_executable ./build-support/$PLATFORM/build-esp32-project.sh
		./build-support/$PLATFORM/build-esp32-project.sh $arg #>> $LOG_FILE
		RETURN_STAT=$?
		#echo $arg $RETURN_STAT
		if [ $RETURN_STAT -eq 0 ]
		then
			# skip
			:
		else
			exit $RETURN_STAT
		fi
	done
		
elif [ $PLATFORM = "pic32" ]
then
	echo
	echo "Selected Platform is PIC32"
else
	echo
	echo "No platform selected!"
fi


