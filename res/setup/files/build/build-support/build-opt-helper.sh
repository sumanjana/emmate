#!/bin/bash

MAKE_OPT_ARR=("-make" "Build the EmMate project")
CLEAN_OPT_ARR=("-clean" "Clean the EmMate project")
FLASH_OPT_ARR=("-flash" "Flash the binaries to the EmMate Device")
ERASE_FLASH_OPT_ARR=("-erase-flash" "Erase the flashed binaries from the EmMate Device")
LOG_OPT_ARR=("-log" "View the Output Log of the EmMate Device")
MENUCONFIG_OPT_ARR=("-menuconfig" "Execute the EmMate Project Menuconfig")

export EMMATE_BUILD_CMD_OPT=($MAKE_OPT_ARR $CLEAN_OPT_ARR $FLASH_OPT_ARR $ERASE_FLASH_OPT_ARR $LOG_OPT_ARR $MENUCONFIG_OPT_ARR)

export EMMATE_BUILD_CMD_OPT_DESCRIPTON=("${MAKE_OPT_ARR[1]}" "${CLEAN_OPT_ARR[1]}" "${FLASH_OPT_ARR[1]}" "${ERASE_FLASH_OPT_ARR[1]}" "${LOG_OPT_ARR[1]}" "${MENUCONFIG_OPT_ARR[1]}")




#echo "${EMMATE_BUILD_CMD_OPT_DESCRIPTON[@]}"
#echo "${#EMMATE_BUILD_CMD_OPT_DESCRIPTON[@]}"


#for ((idx = 0; idx < ${#EMMATE_BUILD_CMD_OPT[@]}; ++idx));
#do
#	echo ${EMMATE_BUILD_CMD_OPT[$idx]} : ${EMMATE_BUILD_CMD_OPT_DESCRIPTON[$idx]}
#done