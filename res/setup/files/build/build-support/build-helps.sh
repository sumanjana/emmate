#!/bin/bash

source $BUILD_OPTION_HELPER_PATH

echo "--------------------- EmMate Build Help ---------------------"

echo "Usage: ./build.sh [options]"

echo "Option: "
#echo " -make :			Build the EmMate project"
#echo " -clean :			Clean the EmMate project"
#echo " -flash :			Flash the binaries to the EmMate Device"
#echo " -erase-flash :			Erase the flashed binaries from the EmMate Device"
#echo " -log:				View the Output Log of the EmMate Device"
#echo " -menuconfig:			Execute the EmMate Project Menuconfig"
#echo
#echo


echo " ${EMMATE_BUILD_CMD_OPT[0]} 				${EMMATE_BUILD_CMD_OPT_DESCRIPTON[0]}"
echo " ${EMMATE_BUILD_CMD_OPT[1]} 			${EMMATE_BUILD_CMD_OPT_DESCRIPTON[1]}"
echo " ${EMMATE_BUILD_CMD_OPT[2]} 			${EMMATE_BUILD_CMD_OPT_DESCRIPTON[2]}"
echo " ${EMMATE_BUILD_CMD_OPT[3]} 			${EMMATE_BUILD_CMD_OPT_DESCRIPTON[3]}"
echo " ${EMMATE_BUILD_CMD_OPT[4]} 				${EMMATE_BUILD_CMD_OPT_DESCRIPTON[4]}"
echo " ${EMMATE_BUILD_CMD_OPT[5]} 			${EMMATE_BUILD_CMD_OPT_DESCRIPTON[5]}"
echo " -help				Show Help Information of the EmMate Build CMD"
echo
echo

#for ((idx = 0; idx < ${#EMMATE_BUILD_CMD_OPT[@]}; ++idx));
#do
#	echo -e "${EMMATE_BUILD_CMD_OPT[$idx]}\t:	${EMMATE_BUILD_CMD_OPT_DESCRIPTON[$idx]}"
#done
##echo
##echo
